(function() {

    var showArea = true;
    var showProduction = true;
    var showArea5YearAverage = true;
    var showArea10YearAverage = true;
    var showProduction5YearAverage = true;
    var showProduction10YearAverage = true;

    var areaCheckbox = d3.select('.check-box-area')
        .on('click', function() {
            showArea = !showArea;
            d3.selectAll('.bar').style('opacity', showArea ? 1 : 0);
            d3.selectAll('.pulse').style('opacity', showArea ? 1 : 0);
            d3.select('.check-box-area').attr('class', 'check-box check-box-area ' + (showArea ? 'active' : ''));
        });

    var productionCheckbox = d3.select('.check-box-production')
        .on('click', function() {
            showProduction = !showProduction;
            d3.selectAll('.line').style('opacity', showProduction ? 1 : 0);
            d3.selectAll('.data-circle').style('opacity', showProduction ? 1 : 0);
            d3.select('.check-box-production').attr('class', 'check-box check-box-production ' + (showProduction ? 'active' : ''));
        });

    var area5YearAverageCheckbox = d3.select('.check-box-area-5-year-average')
        .on('click', function() {
            showArea5YearAverage = !showArea5YearAverage;
            d3.selectAll('.area-5-year-line').style('opacity', showArea5YearAverage ? 1 : 0);
            d3.select('.check-box-area-5-year-average').attr('class', 'check-box check-box-area-5-year-average ' + (showArea5YearAverage ? 'active' : ''));
        });

    var area10YearAverageCheckbox = d3.select('.check-box-area-10-year-average')
        .on('click', function() {
            showArea10YearAverage = !showArea10YearAverage;
            d3.selectAll('.area-10-year-line').style('opacity', showArea10YearAverage ? 1 : 0);
            d3.select('.check-box-area-10-year-average').attr('class', 'check-box check-box-area-10-year-average ' + (showArea10YearAverage ? 'active' : ''));
        });

    var production5YearAverageCheckbox = d3.select('.check-box-production-5-year-average')
        .on('click', function() {
            showProduction5YearAverage = !showProduction5YearAverage;
            d3.selectAll('.production-5-year-line').style('opacity', showProduction5YearAverage ? 1 : 0);
            d3.select('.check-box-production-5-year-average').attr('class', 'check-box check-box-production-5-year-average ' + (showProduction5YearAverage ? 'active' : ''));
        });

    var production10YearAverageCheckbox = d3.select('.check-box-production-10-year-average')
        .on('click', function() {
            showProduction10YearAverage = !showProduction10YearAverage;
            d3.selectAll('.production-10-year-line').style('opacity', showProduction10YearAverage ? 1 : 0);
            d3.select('.check-box-production-10-year-average').attr('class', 'check-box check-box-production-10-year-average ' + (showProduction10YearAverage ? 'active' : ''));
        });

    var data = [
        {year: "2008–09", area: 4.322, production: 6.963, yield: 1.61},
        {year: "2009–10", area: 3.983, production: 5.350, yield: 1.34},
        {year: "2010–11", area: 3.815, production: 10.488, yield: 2.75},
        {year: "2011–12", area: 3.868, production: 8.473, yield: 2.19},
        {year: "2012–13", area: 3.487, production: 7.365, yield: 2.11},
        {year: "2013–14", area: 3.269, production: 6.596, yield: 2.02},
        {year: "2014–15", area: 3.166, production: 6.654, yield: 2.10},
        {year: "2015–16", area: 2.933, production: 6.898, yield: 2.35},
        {year: "2016–17", area: 3.248, production: 9.819, yield: 3.02},
        {year: "2017–18", area: 3.100, production: 4.495, yield: 1.45}
    ];

    var area5YearAverage = (data.slice(-5).reduce(function(sum, el) {
        return sum + el.area;
    }, 0) / 5).toFixed(3);
    var area10YearAverage = (data.slice(-10).reduce(function(sum, el) {
        return sum + el.area;
    }, 0) / 10).toFixed(3);
    var production5YearAverage = (data.slice(-5).reduce(function(sum, el) {
        return sum + el.production;
    }, 0) / 5).toFixed(3);
    var production10YearAverage = (data.slice(-10).reduce(function(sum, el) {
        return sum + el.production;
    }, 0) / 10).toFixed(3);

    var areaMin  = 0;
    var areaMax = d3.max(data, function(el) {
        return el.area;
    });

    var productionMin = 0;
    var productionMax = d3.max(data, function(el) {
        return el.production;
    });

    var chartMargin = {
        left: 60,
        right: 50,
        top: 110,
        bottom: 80
    };

    var tipWidth = 200;
    var tipHeight = 100;

    function redraw() {

        var container = d3.select('.chart');

        container.html('');

        var tip = container.append("div")
            .attr("class", "d3-tip")
            .style("opacity", 0);

        var width = container.node().getBoundingClientRect().width;
        var height = 500;

        var chartHeight = height - chartMargin.top - chartMargin.bottom;
        var chartWidth = width - chartMargin.left - chartMargin.right;

        var svg = container.append('svg').attr('width', width).attr('height', height);

        var chart = svg.append('g')
            .attr('width', width - chartMargin.left)
            .attr('height', chartWidth)
            .attr("transform", "translate(" + chartMargin.left + "," + chartMargin.top+")");

        // area bar graph

        var xScale = d3.scale.ordinal()
            .domain(data.map(function(d) { return d.year; }))
            .rangeRoundBands([0 , chartWidth], .5);

        var barWidth = xScale.rangeBand();

        var areaYScale = d3.scale.linear()
            .domain([areaMin, areaMax])
            .range([chartHeight, 0]);

        var areaYAxis = d3.svg.axis()
            .scale(areaYScale)
            .tickSize(0)
            .orient("left");

        var areaYText = svg.append("g")
            .attr("class", "axis")
            .attr("transform", "translate("+(chartMargin.left )+"," + chartMargin.top+")")
            .call(areaYAxis)
            .append("text")
            .attr("transform", "rotate(-90)")
            .attr("y", -45)
            .attr("x", -40)
            .attr('class', 'highlight')
            .style("text-anchor", "end")
            .text("NSW Wheat Area Planted ('000 ha)");

        var areaXAxis = d3.svg.axis()
            .scale(xScale)
            .tickSize(1)
            .orient("bottom");

        svg.append("g")
            .attr("class", "axis x-axis")
            .attr("transform", "translate("+(chartMargin.left)+"," + ( chartHeight + chartMargin.top )  + ")")
            .call(areaXAxis)
            .append("text");

        svg.selectAll('.x-axis .tick')
            .selectAll("text")
            .style("text-anchor", "end")
            .attr("transform", "rotate(-45)");

        var barContainers = chart.selectAll("g")
            .data(data)
            .enter().append("g");

        var bars = barContainers.append('rect')
            .attr("year", function (d) { return d.year })
            .attr("v", function (d) { return d.area })
            .attr("x", function (d) { return xScale(d.year) })
            .attr("y", function (d) { return areaYScale(d.area); })
            .attr("height", function (d) { return chartHeight - areaYScale(d.area); })
            .attr("width", xScale.rangeBand())
            .attr("class", "bar")
            .style('opacity', showArea ? 1 : 0);

        bars.each(function(d, i) {
            var e =  d3.select(this);

            var barHeight = e.attr("height");
            var barLeft = parseFloat(e.attr("x"));

            container.append("div")
                .attr("class", "pulse")
                .style('height', barWidth + 'px')
                .style('width', barWidth + 'px')
                .style('opacity', showArea ? 1 : 0)
                .style("position","absolute")
                .style("top", (chartHeight - barHeight - barWidth / 2 + chartMargin.top) + "px")
                .style("left", (chartMargin.left + barLeft) + "px")
                .on('mouseover', function() {
                    tip.style('top', (chartHeight - barHeight - barWidth + chartMargin.top - tipHeight) + "px");
                    tip.style('left',  (chartMargin.left + barLeft + barWidth / 2 - tipWidth / 2) + "px");
                    tip.style("opacity", 1);
                    tip.html("<div><b>"+d.year+"</b></div><div>Area <span class='text-highlight'>"+d.area+"</span></div>" +
                        "<div>Production <span class='text-highlight'>"+d.production+"</span></div>" +
                        "<div>Yield <span class='text-highlight'>"+d.yield+"</span></div>")
                })
                .on('mouseout', function() {
                    tip.style('opacity', 0);
                })

        });

        // production linear graph

        var productionYScale = d3.scale.linear()
            .domain([productionMin, productionMax])
            .range([chartHeight, 0]);

        var productionYAxis = d3.svg.axis()
            .scale(productionYScale)
            .tickSize(0)
            .orient("right");

        svg.append("g")
            .attr("class", "axis")
            .attr("transform", "translate("+(chartMargin.left + chartWidth)+","+chartMargin.top+")")
            .call(productionYAxis)
            .append("text")
            .attr("transform", "rotate(-90)")
            .attr("y", 45)
            .attr("x", -45)
            .attr('class', 'highlight')
            .style("text-anchor", "end")
            .text("NSW Wheat Production (tonnes)");

        var lineAndDots = chart.append('g');

        var	productionLine = d3.svg.line()
            .x(function(d) { return xScale(d.year); })
            .y(function(d) { return productionYScale(d.production); });

        var productionPath = lineAndDots.append("path")
            .attr("class", "line")
            .attr("d", productionLine(data))
            .attr('transform', 'translate('+(barWidth / 2)+', 0)')
            .style('opacity', showProduction ? 1 : 0);

        var productionTotalLength = productionPath[0][0].getTotalLength();

        d3.select(productionPath[0][0])
            .attr("stroke-dasharray", productionTotalLength + " " + productionTotalLength )
            .attr("stroke-dashoffset", productionTotalLength)
            .transition()
            .duration(300)
            .ease("linear")
            .attr("stroke-dashoffset", 0);

        // linear graph circles

        lineAndDots.selectAll("line-circle")
            .data(data)
            .enter().append("circle")
            .style('opacity', showProduction ? 1 : 0)
            .attr("class", "data-circle")
            .attr("r", 5)
            .attr("cx", function(d) { return xScale(d.year); })
            .attr("cy", function(d) { return productionYScale(d.production); })
            .attr('transform', 'translate(' + (barWidth / 2) + ', 0)');

        // area 5 year average

        var	area5YearAverageLine = d3.svg.line()
            .x(function(d) { return xScale(d.year); })
            .y(function() { return areaYScale(area5YearAverage); });

        var area5yearAveragePath = chart.append("path")
            .attr("class", "area-5-year-line")
            .attr("d", area5YearAverageLine(data))
            .attr('transform', 'translate('+(barWidth / 2)+', 0)')
            .style('opacity', showArea5YearAverage ? 1 : 0);

        var area5yearAverageTotalLength = area5yearAveragePath[0][0].getTotalLength();

        d3.select(area5yearAveragePath[0][0])
            .attr("stroke-dasharray", area5yearAverageTotalLength + " " + area5yearAverageTotalLength)
            .attr("stroke-dashoffset", area5yearAverageTotalLength)
            .transition()
            .duration(300)
            .ease("linear")
            .attr("stroke-dashoffset", 0);

        // area 10 year average

        var	area10YearAverageLine = d3.svg.line()
            .x(function(d) { return xScale(d.year); })
            .y(function() { return areaYScale(area10YearAverage); });

        var area10yearAveragePath = chart.append("path")
            .attr("class", "area-10-year-line")
            .attr("d", area10YearAverageLine(data))
            .attr('transform', 'translate('+(barWidth / 2)+', 0)')
            .style('opacity', showArea10YearAverage ? 1 : 0);

        var area10yearAverageTotalLength = area10yearAveragePath[0][0].getTotalLength();

        d3.select(area10yearAveragePath[0][0])
            .attr("stroke-dasharray", area10yearAverageTotalLength + " " + area10yearAverageTotalLength)
            .attr("stroke-dashoffset", area10yearAverageTotalLength)
            .transition()
            .duration(300)
            .ease("linear")
            .attr("stroke-dashoffset", 0);

        // production 5 year average

        var	production5YearAverageLine = d3.svg.line()
            .x(function(d) { return xScale(d.year); })
            .y(function() { return productionYScale(production5YearAverage); });

        var production5yearAveragePath = chart.append("path")
            .attr("class", "production-5-year-line")
            .attr("d", production5YearAverageLine(data))
            .attr('transform', 'translate('+(barWidth / 2)+', 0)')
            .style('opacity', showProduction5YearAverage ? 1 : 0);

        var production5yearAverageTotalLength = production5yearAveragePath[0][0].getTotalLength();

        d3.select(production5yearAveragePath[0][0])
            .attr("stroke-dasharray", production5yearAverageTotalLength + " " + production5yearAverageTotalLength)
            .attr("stroke-dashoffset", production5yearAverageTotalLength)
            .transition()
            .duration(300)
            .ease("linear")
            .attr("stroke-dashoffset", 0);

        // production 10 year average

        var	production10YearAverageLine = d3.svg.line()
            .x(function(d) { return xScale(d.year); })
            .y(function() { return productionYScale(production10YearAverage); });

        chart.append("path")
            .attr("class", "production-10-year-line")
            .attr("d", production10YearAverageLine(data))
            .attr('transform', 'translate('+(barWidth / 2)+', 0)')
            .style('opacity', showProduction10YearAverage ? 1 : 0);

        var production10yearAveragePath = chart.append("path")
            .attr("class", "production-5-year-line")
            .attr("d", production5YearAverageLine(data))
            .attr('transform', 'translate('+(barWidth / 2)+', 0)')
            .style('opacity', showProduction5YearAverage ? 1 : 0);

        var production10yearAverageTotalLength = production10yearAveragePath[0][0].getTotalLength();

        d3.select(production10yearAveragePath[0][0])
            .attr("stroke-dasharray", production10yearAverageTotalLength + " " + production10yearAverageTotalLength)
            .attr("stroke-dashoffset", production10yearAverageTotalLength)
            .transition()
            .duration(300)
            .ease("linear")
            .attr("stroke-dashoffset", 0);


    }

    redraw();

    window.addEventListener("resize", redraw);

})();