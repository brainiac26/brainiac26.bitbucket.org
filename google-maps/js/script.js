(function () {
    google.maps.event.addDomListener(window, 'load', initialize);

    function initialize() {
        var map,
            drawingToolColor,
            drawingManager,
            overlays = [],
            place,
            selectedShape,
            autocomplete,

            addressSearchInput = document.getElementById('address-search'),
            tableWrapper = document.getElementById('table-wrapper'),
            submitButton = document.getElementById('submit');

        bindEvents();

        map = new google.maps.Map(document.getElementById('map'), {
            center: new google.maps.LatLng(36.2398864, -113.7644643),
            zoom: 6,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            mapTypeControl: false,
            streetViewControl: false
        });

        autocomplete = new google.maps.places.Autocomplete(addressSearchInput);

        autocomplete.addListener('place_changed', function () {

            place = autocomplete.getPlace();

            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);
            }
        });

        drawingManager = new google.maps.drawing.DrawingManager({
            drawingMode: google.maps.drawing.OverlayType.POLYGON,
            drawingControl: true,
            drawingControlOptions: {
                position: google.maps.ControlPosition.TOP_CENTER,
                drawingModes: [
                    google.maps.drawing.OverlayType.POLYGON,
                    google.maps.drawing.OverlayType.CIRCLE,
                    google.maps.drawing.OverlayType.RECTANGLE
                ]
            }
        });

        drawingManager.setMap(map);

        setDrawingOptions();

        google.maps.event.addListener(drawingManager, 'overlaycomplete', function (e) {
            var newShape = e.overlay;
            e.drawingColor = drawingToolColor;
            overlays.push(e);
            setDrawingOptions();
            google.maps.event.addListener(newShape, 'click', function () {
                setSelection(newShape);
            });
            fillTable();
            setSelection(newShape);

        });

        // Circle
        google.maps.event.addListener(drawingManager, 'circlecomplete', function (circle) {
            google.maps.event.addListener(circle, 'center_changed', function () {
                fillTable();
            });
            google.maps.event.addListener(circle, 'radius_changed', function () {
                fillTable();
            });
        });

        // Polygon
        google.maps.event.addListener(drawingManager, 'polygoncomplete', function (polygon) {
            var polygonPath = polygon.getPath();
            google.maps.event.addListener(polygon, 'dragend', function () {
                fillTable();
            });

            google.maps.event.addListener(polygonPath, 'set_at', function () {
                fillTable();
            });

            google.maps.event.addListener(polygonPath, 'insert_at', function () {
                fillTable();
            });
        });

        // Rectangle
        google.maps.event.addListener(drawingManager, 'rectanglecomplete', function (rectangle) {
            google.maps.event.addListener(rectangle, 'dragend', function () {
                fillTable();
            });
            google.maps.event.addListener(rectangle, 'bounds_changed', function () {
                fillTable();
            });
        });

        function bindEvents() {
            document.addEventListener('click', function(e) {
                if (e.target.className.indexOf('delete') !== -1) {
                    var index = e.target.getAttribute('data-index');
                    deleteShape(index);
                }
            });

            submitButton.addEventListener('click', function() {
                var params = [];
                for (var i = 0, l = overlays.length; i < l; i++) {
                    params.push(getOverlayParams(overlays[i]));
                }
                console.log({params: params});
            })
        }

        function setDrawingOptions() {
            drawingToolColor = '#'+(Math.random(10)*0xFFFFFF<<0).toString(16);
            drawingManager.setOptions({
                polygonOptions: {
                    fillColor: drawingToolColor,
                    strokeColor: drawingToolColor,
                    fillOpacity: 0.3,
                    draggable: true,
                    editable: true
                },
                rectangleOptions: {
                    fillColor: drawingToolColor,
                    strokeColor: drawingToolColor,
                    fillOpacity: 0.3,
                    draggable: true,
                    editable: true
                },
                circleOptions: {
                    fillColor: drawingToolColor,
                    strokeColor: drawingToolColor,
                    fillOpacity: 0.3,
                    draggable: true,
                    editable: true
                }
            });
        }

        function clearSelection() {
            if (selectedShape) {
                selectedShape.setEditable(false);
                selectedShape = null;
            }
        }

        function deleteShape(index) {
            var selectedShape = overlays[index].overlay;
            selectedShape.setMap(null);
            selectedShape = null;
            overlays.splice(index, 1);
            fillTable();
        }

        function findOverlayIndex(shape) {
            var index;
            for (var i = 0, l = overlays.length; i < l; i++) {
                if (shape === overlays[i].overlay) {
                    index = i;
                    break;
                }
            }
            return index;
        }

        function fillTable() {
            var innerHtml = '';
            for (var i = 0, l = overlays.length; i < l; i++) {
                var overlay = overlays[i];
                innerHtml += '<table class="table" id="shape'+i+'">';
                innerHtml += '<tr>' +
                    '<th colspan="2">' +
                    '<div class="color" style="background-color: '+overlay.drawingColor+'"></div>' +
                    'Shape '+(i+1)+'' +
                    '<a href="#" class="delete" data-index="'+i+'">Delete<i class="glyphicon glyphicon-remove"></i></a>' +
                    '</th>' +
                    '</tr>';
                innerHtml += getOverlayRows(overlay);
                innerHtml += '</table>';
            }
            tableWrapper.innerHTML = innerHtml;
        }

        function getOverlayRows(overlay) {
            switch (overlay.type) {
                case 'circle':
                    return getCircleRows(overlay.overlay);
                case 'rectangle':
                    return getRectangleRows(overlay.overlay);
                case 'polygon':
                    return getPolygonRows(overlay.overlay);
                default:
                    return '';
            }
        }

        function getOverlayParams(overlay) {
            switch (overlay.type) {
                case 'circle':
                    return getCircleParams(overlay.overlay);
                case 'rectangle':
                    return getRectangleParams(overlay.overlay);
                case 'polygon':
                    return getPolygonParams(overlay.overlay);
                default:
                    return '';
            }
        }

        function getCircleRows(overlay) {
            var rows = '';
            rows += '<tr><td>Center</td><td>'+overlay.center+'</td></tr>';
            rows += '<tr><td>Radius</td><td>'+overlay.radius+' meters</td></tr>';
            return rows;
        }

        function getCircleParams(overlay) {
            return {
                coords: overlay.center,
                radius: {lat:  overlay.radius.lat(), lng: overlay.radius.lng()}
            }
        }

        function getRectangleRows(overlay) {
            var rows = '';
            var bounds = overlay.bounds;
            var sw = bounds.getSouthWest(),
                ne = bounds.getNorthEast(),
                southWest = new google.maps.LatLng(sw.lat(), sw.lng()),
                northEast = new google.maps.LatLng(ne.lat(), ne.lng()),
                southEast = new google.maps.LatLng(sw.lat(), ne.lng()),
                northWest = new google.maps.LatLng(ne.lat(), sw.lng());
            rows += '<tr><td>South west dot</td><td>'+southWest+'</td></tr>';
            rows += '<tr><td>North east dot</td><td>'+northEast+'</td></tr>';
            rows += '<tr><td>South east dot</td><td>'+southEast+'</td></tr>';
            rows += '<tr><td>North west dot</td><td>'+northWest+'</td></tr>';
            return rows;
        }

        function getRectangleParams(overlay) {
            var bounds = overlay.bounds;
            var sw = bounds.getSouthWest(),
                ne = bounds.getNorthEast(),
                southWest = {lat: sw.lat(), lng: sw.lng()},
                northEast = {lat: ne.lat(), lng: ne.lng()},
                southEast = {lat: sw.lat(), lng: ne.lng()},
                northWest = {lat: ne.lat(), lng: sw.lng()};
            return {
                coords: [northEast, southEast, southWest, northWest]
            }
        }

        function getPolygonRows(overlay) {
            var rows = '';
            var vertices = overlay.getPath();
            for (var i = 0, l = vertices.length; i<l; i++) {
                var vertice = vertices.getAt(i);
                rows += '<tr><td>Dot ' + (i+1) + '</td><td>' +
                    new google.maps.LatLng(vertice.lat(), vertice.lng()) +
                    '</td></tr>';
            }
            return rows;
        }

        function getPolygonParams(overlay) {
            var vertices = overlay.getPath(),
                coords = [];
            for (var i = 0, l = vertices.length; i < l; i++) {
                var vertice = vertices.getAt(i);
                coords.push({lat: vertice.lat(), lng: vertice.lng()});
            }
            return {
                coords: coords
            }
        }

        function setSelection(shape) {
            clearSelection();
            selectedShape = shape;
            shape.setEditable(true);
            var table = document.getElementById('shape' + findOverlayIndex(shape));
            var selectedTable = document.getElementsByClassName('selected')[0];
            if (selectedTable) {
                selectedTable.className = selectedTable.className.slice(0, selectedTable.className.length - 'selected'.length);
            }
            table.className += ' selected';
            tableWrapper.scrollTop = table.offsetTop;
        }
    }
})();