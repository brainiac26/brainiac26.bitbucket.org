/**
 * Miami Contaminated Sites
 *
 * Created by Seven Mile Media
 * http://sevenmilemedia.com/
 */

(function($) {

    $(document).ready(function() {

        mapboxgl.accessToken = 'pk.eyJ1Ijoibmlja2hhcmIiLCJhIjoiY2pucnN4cWloMGJveTNxbjJ4dzg3dGM4eCJ9.YHcYBuehFvoDyGiJr6dBig';

        // Load basemap
        var map = new mapboxgl.Map({
            container: 'map',
            style: 'mapbox://styles/nickharb/ckg0wwgil11on19ns7qpjx3if', // stylesheet location
            center: [-80.251, 25.773], // starting position [lng, lat],
            zoom: 10 // starting zoom
        });

        // Add popups on click
        map.on('click', function(e) {
            var features = map.queryRenderedFeatures(e.point, {
                layers: ['miami-dade-county-contaminated-s'] // replace this with the name of the layer
            });

            if (!features.length) {
                return;
            }

            var feature = features[0];

            console.log(feature);

            var popup = new mapboxgl.Popup({ offset: [0, -15] })
                .setLngLat(feature.geometry.coordinates)
                .setHTML('<h3>' + feature.properties.TASK_NAME + '</h3>' + '<p>Work Group: ' + feature.properties.WORK_GROUP + '</p>' + '<p>Address: ' + feature.properties.SITE_ADDRESS + '</p>' + '<p>Description: ' + feature.properties.PHASE_DESCRIPTION + '</p>')
                .addTo(map);
        });

        // Flyover event handlers
        $('#downtown-miami').click(function() {
            map.flyTo({
                center: [-80.193, 25.766],
                zoom: 16,
                pitch: 60,
                bearing: 0,
                duration: 6000,
                curve: 2
            });
        });

        $('#south-beach').click(function() {
            map.flyTo({
                center: [-80.134, 25.772],
                zoom: 16,
                pitch: 60,
                bearing: 0,
                duration: 6000,
                curve: 2
            });
        });

        $('#mia').click(function() {
            map.flyTo({
                center: [-80.286, 25.793],
                zoom: 14,
                pitch: 60,
                bearing: 0,
                duration: 6000,
                curve: 2
            });
        });

        $('#um').click(function() {
            map.flyTo({
                center: [-80.278, 25.715],
                zoom: 16,
                pitch: 60,
                bearing: 0,
                duration: 6000,
                curve: 2
            });
        });

        $('#reset').click(function() {
            map.flyTo({
                center: [-80.251, 25.773],
                zoom: 10,
                pitch: 0,
                bearing: 0,
                duration: 6000,
                curve: 2
            });
        });


    });

})(jQuery);
