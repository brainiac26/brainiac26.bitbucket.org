(function($) {
    var tableid = "1ecIkMd9Uz35AClgBx2D8heZKRZNlxmsMAJ_9JLcL",
        key = "AIzaSyA1eu9HrGxewOvRxtAZQAeezkvCB7TanCk";

    var addresses = [],
        map,
        markers = [],
        bounds = new google.maps.LatLngBounds(),
        cluster,
        geomarker,
        infowindow = new google.maps.InfoWindow(),

        clusterStyles = [
            {
                url: './filled-circle.png',
                height: 32,
                width: 32,
                textSize: 12,
                textColor: '#fff'
            }
        ];

    $(document).ready(function() {
        map = new google.maps.Map(document.getElementById('map'), {
            center: new google.maps.LatLng(59.28192089536628, 11.299659078369132),
            zoom: 11,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            mapTypeControl: false,
            zoomControl: false,
            scaleControl: false,
            streetViewControl: false
        });

        $.get('https://www.googleapis.com/fusiontables/v2/query?sql=SELECT * FROM '+tableid+'&key=' + key)
            .then(function(data) {
                _.forEach(data.rows, function(row) {
                    addresses.push(_.object(data.columns, row));
                });

                initMarkers();
            });

        $('#location').on('click', function() {
            getLocation().then(function(position) {
                map.setCenter(position);
                map.setZoom(12);
            })
        });
    });

    function initMarkers () {
        _.forEach(addresses, function(address) {
            var position = new google.maps.LatLng(address.lat, address.lng);
            var marker = new google.maps.Marker({
                map: map,
                position: position,
                title: address['Marker hover name'],
                icon: 'https://www.sharethedignity.com.au/uploads/8/5/7/8/85785688/map-marker-2-24_orig.png',
                info: address
            });

            google.maps.event.addListener(marker, 'click', function() {
                infowindow.setContent('<div id="infowindow">' +
                    '<p>'+this.info['Location']+'</p>' +
                    '<p><a target="_blank" href="'+this.info['link']+'">View on google maps</a></p>' +
                    '</div>');
                infowindow.open(map, this);
            });

            markers.push(marker);
            bounds.extend(position);
        });
        cluster = new MarkerClusterer(map, markers, {styles: clusterStyles});
        map.fitBounds(bounds);
    };

    function getLocation() {
        var defer = $.Deferred();
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(location) {
                var position = new google.maps.LatLng(
                    location.coords.latitude,
                    location.coords.longitude
                );

                if (geomarker) {
                    geomarker.setMap(null);
                    geomarker = null;
                }

                geomarker = new google.maps.Marker({
                    position: position,
                    icon: 'location.png',
                    map: map
                });
                defer.resolve(position);
            });
        } else {
            defer.reject();
        }
        return defer.promise();
    }
})(jQuery);