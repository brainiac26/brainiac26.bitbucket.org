function CustomInfobox(latlng, map, content, color) {
  this.latlng = latlng;
  this.content = content;
  this.color = color;
  this.setMap(map);
}

CustomInfobox.prototype = new google.maps.OverlayView();

CustomInfobox.prototype.draw = function() {

  var self = this;

  var div = this.div;

  if (!div) {

    div = this.div = document.createElement('div');
    div.style.position = 'absolute';

    var content = document.createElement('div');

    content.innerHTML = this.content;

    content.className = 'marker';

    content.style.height = '24px';
    content.style.background = '#fff';
    content.style.border = "1px solid " + this.color;
    content.style.borderRight = 'none';
    content.style.lineHeight = "24px";
    content.style.padding = "0 5px 0 15px";

    div.appendChild(content);

    var bigArrow = document.createElement('div');
    bigArrow.innerHTML = '';
    bigArrow.style.width = 0;
    bigArrow.style.height = 0;
    bigArrow.style.borderStyle = 'solid';
    bigArrow.style.borderWidth = '12px 0 12px 12px';
    bigArrow.style.borderColor = 'transparent transparent transparent ' + this.color;
    bigArrow.style.position = 'absolute';
    bigArrow.style.left = '100%';
    bigArrow.style.top = '0';

    div.appendChild(bigArrow);

    var smallArrow = document.createElement('div');
    smallArrow.innerHTML = '';
    smallArrow.style.width = 0;
    smallArrow.style.height = 0;
    smallArrow.style.borderStyle = 'solid';
    smallArrow.style.borderWidth = '11px 0 11px 11px';
    smallArrow.style.borderColor = 'transparent transparent transparent #fff';
    smallArrow.style.position = 'absolute';
    smallArrow.style.left = '100%';
    smallArrow.style.top = '1px';

    div.appendChild(smallArrow);

    var panes = this.getPanes();
    panes.overlayLayer.appendChild(div);
  }

  var point = this.getProjection().fromLatLngToDivPixel(this.latlng);

  if (point) {
    div.style.left = point.x + 'px';
    div.style.top = point.y - 24 + 'px';
  }
};

CustomInfobox.prototype.remove = function() {
  if (this.div) {
    this.div.parentNode.removeChild(this.div);
    this.div = null;
    this.setMap(null);
  }
};

CustomInfobox.prototype.getPosition = function() {
  return this.latlng;
};