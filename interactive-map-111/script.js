(function($) {

  var map,
    infobox,
    markers = [];

  var styles = [
    {
      "featureType": "administrative",
      "elementType": "all",
      "stylers": [
        {
          "saturation": "-100"
        }
      ]
    },
    {
      "featureType": "administrative.province",
      "elementType": "all",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "landscape",
      "elementType": "all",
      "stylers": [
        {
          "saturation": -100
        },
        {
          "lightness": 65
        },
        {
          "visibility": "on"
        }
      ]
    },
    {
      "featureType": "poi",
      "elementType": "all",
      "stylers": [
        {
          "saturation": -100
        },
        {
          "lightness": "50"
        },
        {
          "visibility": "simplified"
        }
      ]
    },
    {
      "featureType": "road",
      "elementType": "all",
      "stylers": [
        {
          "saturation": "-100"
        }
      ]
    },
    {
      "featureType": "road.highway",
      "elementType": "all",
      "stylers": [
        {
          "visibility": "simplified"
        }
      ]
    },
    {
      "featureType": "road.arterial",
      "elementType": "all",
      "stylers": [
        {
          "lightness": "30"
        }
      ]
    },
    {
      "featureType": "road.local",
      "elementType": "all",
      "stylers": [
        {
          "lightness": "40"
        }
      ]
    },
    {
      "featureType": "transit",
      "elementType": "all",
      "stylers": [
        {
          "saturation": -100
        },
        {
          "visibility": "simplified"
        }
      ]
    },
    {
      "featureType": "water",
      "elementType": "geometry",
      "stylers": [
        {
          "hue": "#ffff00"
        },
        {
          "lightness": -25
        },
        {
          "saturation": -97
        }
      ]
    },
    {
      "featureType": "water",
      "elementType": "labels",
      "stylers": [
        {
          "lightness": -25
        },
        {
          "saturation": -100
        }
      ]
    }
  ];

  var center = new google.maps.LatLng(36.150621, -113.7864566);

  var categories;

  var $categories,
    $locations,
    $locationInfo,
    $location,
    $backButton;

  $(document).ready(function() {

    $categories = $('.categories');
    $locations = $('.locations');
    $locationInfo = $('.location-info').hide();
    $location = $locationInfo.find('.location');
    $backButton = $locationInfo.find('.back');

    init();
    bindEvents();

  });

  function bindEvents() {
    $backButton.on('click', function() {
      $locationInfo.hide();
      $locations.show();
      destroyInfobox();
      fitBounds();
      markers.forEach(function(marker) {
        marker.setIcon(getIcon(marker.data, true));
      });

      ga('send', 'event', 'Go back button', 'click');

      return false;
    });

    $locations.on('click', '.location', function() {
      var id = $(this).attr('data-id');
      selectLocation(id);
      selectMarker(id);

      ga('send', 'event', 'Headline "' + $(this).find('h3').html() + '"', 'click');

      return false;
    })
  }

  function init() {

    map = new google.maps.Map(document.getElementById('map'), {
      zoom: 10,
      center: center,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      styles: styles
    });

    $.get('categories.json').then(function(res) {
      categories = res;

      createCategories(categories);
      createLocations(categories);
      createMarkers(categories);

    })
  }

  function createInfobox(latLng, content, color) {
    if (infobox) {
      destroyInfobox();
    }
    infobox = new CustomInfobox(latLng, map, content, color);
  }

  function destroyInfobox() {
    infobox.remove();
  }

  function createCategories(categories) {
    categories.forEach(function(cat) {
      $categories.append('<div class="cat"><div class="color" style="background: '+cat.color+'"></div>'+cat.title+'</div>');
    })
  }

  function createLocations(categories) {
    categories.forEach(function(cat) {
      var locationsList = '';
      cat.locations.forEach(function(location) {
        locationsList += '<a href="#" class="location" data-id="'+location.id+'"><h3 class="location-title">'+location.headline+'</h3></a>';
      });
      $locations.append('<h2 class="cat-title" style="color: '+cat.color+'">'+cat.title+'</h2><div class="locations">'+locationsList+'</div></div>');
    })
  }

  function createMarkers(categories) {
    categories.forEach(function(cat) {
      cat.locations.forEach(function(location) {
        var marker = new google.maps.Marker({
          position: {lat: location.address.lat, lng: location.address.lng},
          optimized: false,
          zIndex: 999,
          icon: {
            url: getIcon(location, true)
          },
          map: map,
          data: location,
          color: cat.color
        });
        markers.push(marker);

        marker.dot = new google.maps.Marker({
          icon: {
            path: 'M0 0 H 15 V 15 H 0 L 0 0',
            fillColor: cat.color,
            fillOpacity: 1,
            strokeColor: '#fff',
            strokeWeight: 1,
            scale: 1,
            anchor: new google.maps.Point(15,10)
          }
        });

        marker.dot.bindTo('position', marker);
        marker.dot.bindTo('map', marker);

        google.maps.event.addListener(marker, 'click', (function(marker) {
          return function() {
            createInfobox(marker.getPosition(), marker.data.headline, marker.color);
            markers.forEach(function(marker) {
              marker.setIcon(getIcon(marker.data, true));
            });
            marker.setIcon(getIcon(marker.data));
            setCenter(marker);
            selectLocation(marker.data.id);

            ga('send', 'event', 'Marker for "' + marker.data.headline + '"', 'click');
          }
        })(marker));
      })
    });
    fitBounds();
  }

  function getIcon(location, isSmall) {
    var iconUrl = 'img/';
    if (location.video) {
      iconUrl += isSmall ? 'video_sm.png' : 'video.png';
    } else if (location.photo) {
      iconUrl += isSmall ? 'image_sm.png' : 'image.png';
    } else {
      iconUrl += isSmall ? 'article_sm.png' : 'article.png';
    }
    return iconUrl;
  }

  function selectLocation(locationId) {
    var selectedLocation;
    $locations.hide();
    $locationInfo.show();
    categories.forEach(function(cat) {
      cat.locations.forEach(function(location) {
        if (location.id === locationId) {
          selectedLocation = location;
        }
      })
    });
    createLocationInfo(selectedLocation);
  }

  function selectMarker(id) {
    var marker = markers.find(function(marker) {
      return marker.data.id === id;
    });
    markers.forEach(function(marker) {
      marker.setIcon(getIcon(marker.data, true));
    });
    marker.setIcon(getIcon(marker.data));
    setCenter(marker);
    createInfobox(marker.getPosition(), marker.data.headline, marker.color);
  }

  function createLocationInfo(location) {
    $location.html('<h3>'+location.headline+'</h3>' +
      '<p><a href="'+location.url+'" target="_blank">'+location.url+'</a></p>' +
      (location.photo ? '<img class="img-responsive" src="'+location.photo+'" />' : '') +
      '<p>'+(location.video ? '<div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" src="'+location.video+'"></iframe></div>' : '')+'</p>'+
      '<p>'+location.description+'</p>'+
      '<p>'+(location.text ? location.text : '')+'</p>');
  }

  function fitBounds() {
    var bounds = new google.maps.LatLngBounds();
    markers.forEach(function(marker) {
      bounds.extend(marker.getPosition());
    });
    map.fitBounds(bounds);
  }

  function setCenter(marker) {
    map.setCenter(marker.getPosition());
  }

})(jQuery);