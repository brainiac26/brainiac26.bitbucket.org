(function ($) {
    var spreadsheetUrl = '//docs.google.com/spreadsheets/d/1NO8FiJv7J19zN3-edRQiQ760NrqWbT3YtnQ9rsUwcOQ/pub?output=csv',
        map,
        markers = [],
        geomarker,
        infowindow,
        places = [],
        bounds;

    var clusterStyles = [
        {
            textColor: 'white',
            url: './img/clusterCircle.svg',
            height: 60,
            width: 60
        }
    ];

    $(document).ready(function() {
        initialize();
        bindEvents();
    });

    function initialize() {

        map = new google.maps.Map($('#map').get(0), {
            center: new google.maps.LatLng(-33.8474011, 150.6510813),
            zoom: 6,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            mapTypeControl: false,
            zoomControl: false,
            scaleControl: false,
            streetViewControl: false
        });

        //getLocation();

        infowindow = new google.maps.InfoWindow();

        bounds = new google.maps.LatLngBounds();

        getPlaces().then(function(data) {
            places = data;
            for (var i = 0, l = places.length; i < l; i++) {
                places[i].lat = parseFloat(places[i].lat.replace(',', '.'));
                places[i].lng = parseFloat(places[i].lng.replace(',', '.'));
            }
            addMarkers();
        });
    }

    function bindEvents() {
        $('#location').on('click', function() {
            getLocation();
        });
    }

    function getPlaces() {
        var d = $.Deferred();
        Papa.parse(spreadsheetUrl, {
            dynamicTyping: true,
            download: true,
            header: true,
            complete: function(res) {
                console.log(res);
                d.resolve(res.data);
            }
        });
        return d.promise();
    }

    function addMarkers() {
        for (var i = 0, l = places.length; i < l; i++) {
            var position = new google.maps.LatLng(places[i].lat, places[i].lng);
            var marker = new google.maps.Marker({
                position: position,
                map: map,
                info: places[i]
            });
            markers.push(marker);
            bounds.extend(position);

            google.maps.event.addListener(marker, 'click', function() {
                infowindow.setContent('<div>' +
                    '<p><b>Address: </b>'+this.info['address']+'</p>' +
                    '<p><b>Blend: </b>'+this.info['blend']+'</p>' +
                    '<p><b>Cafe name: </b>'+this.info['cafeName']+'</p>' +
                    '<p><b>Nitro: </b>'+this.info['nitro']+'</p>' +
                    '</div>');
                infowindow.open(map, this);
            });
        }

        new MarkerClusterer(map, markers, {styles: clusterStyles});
        map.fitBounds(bounds);
    }

    function getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(location) {
                var position = new google.maps.LatLng(
                    location.coords.latitude,
                    location.coords.longitude
                );
                geomarker = new google.maps.Marker({
                    position: position,
                    icon: './img/circle-24.png',
                    map: map
                });
                map.setCenter(position);
                // bounds.extend(position);
                // map.fitBounds(bounds);
            });
        }
    }
})(jQuery);