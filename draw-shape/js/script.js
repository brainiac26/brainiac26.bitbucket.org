const coords = [                            // coordinates for markers
    { lat: 34.753445, lng: -117.823869 },
    { lat: 34.743445, lng: -117.833869 },
    { lat: 34.853445, lng: -117.853869 },
    { lat: 34.953445, lng: -117.953869 },
    { lat: 34.854445, lng: -117.843869 },
];
let map;                                    // google maps map
let markers = [];                           // google maps markers
const mapOptions = {                        // options to init google maps map
    center: { lat: 34.753445, lng: -117.823869 },
    zoom: 11,
    draggableCursor: 'pointer',
    gestureHandling: 'greedy'
};
let shape;                                  // shape to be drawn
let clearMarker;                            // button to delete shape
let mapEl;                                  // map DOM element
let drawingButtonEl;                        // Start drawing button DOM element
let instructionEl;                          // instructions DOM element

document.addEventListener("DOMContentLoaded", () => {
    mapEl = document.getElementById('map');
    drawingButtonEl = document.getElementById('start-draw');
    instructionEl = document.getElementById('instruction');

    document.getElementById('map-wrapper').style.height = window.innerHeight + 'px';

    addEventListeners();

    initMap();
});

function addEventListeners() {
    mapEl.addEventListener('touchmove', (e) => { e.preventDefault(); }, { passive: false });

    drawingButtonEl.addEventListener('click', () => {
        if (shape) {
            deleteShape();
        }
        drawingButtonEl.classList.add('hide');
        instructionEl.classList.remove('hide');
        disableMap();
        map.setOptions({draggableCursor: 'url(./img/pencil.png) 0 20, auto' });

        map.addListener('mousedown', () => {
            draw();
        });
    })
}

function initMap() {
    map = new google.maps.Map(mapEl, mapOptions);  // init google maps map

    coords.forEach(coord => {                      // init markers
        const marker = new google.maps.Marker({
            position: coord,
            map: map
        });
        markers.push(marker);
    });

    google.maps.event.addListener(map.getStreetView(),'visible_changed', () => {
        if (map.getStreetView().getVisible()) {
            drawingButtonEl.classList.add('hide');
        } else {
            drawingButtonEl.classList.remove('hide');
        }
    });
}

function deleteFromMap(el) {
    el.setMap(null);
    el = null;
}

function deleteShape() {
    deleteFromMap(shape);
    deleteFromMap(clearMarker);
    markers.forEach(marker => {
        marker.setMap(map);
    });
}

function disableMap() {
    if (!map) return;
    map.setOptions({
        draggable: false,
        clickable: false,
        zoomControl: false,
        scrollwheel: false,
        disableDoubleClickZoom: false,
    });
    markers.forEach(marker => {
        marker.setOptions({clickable: false});
    });
}

function enableMap() {
    if (!map) return;
    map.setOptions({
        draggable: true,
        clickable: true,
        zoomControl: true,
        scrollwheel: true,
        disableDoubleClickZoom: true,
    });
    markers.forEach(marker => {
        marker.setOptions({clickable: true});
    });
}

function draw() {
    if (!map) return;
    shape = new google.maps.Polyline({map: map, clickable: false});

    const move = map.addListener('mousemove', (e) => {
        shape.getPath().push(e.latLng);
        e.stop();
    });

    map.addListener('mouseup', (e) => {
        google.maps.event.removeListener(move);
        const path = shape.getPath();
        shape.setMap(null);
        shape = new google.maps.Polygon({map: map, path: path});

        clearMarker =  new google.maps.Marker({
            position: shape.getPath().getArray()[0],
            icon: {
                url: './img/close-button-png-30221.png',
                scaledSize: new google.maps.Size(20, 20),
                anchor: new google.maps.Point(10, 10),
            },
            map: map
        });

        google.maps.event.addListener(clearMarker, 'click', () => {
            deleteShape();
        });

        if (markers) {
            markers.forEach(marker => {
                if (!google.maps.geometry.poly.containsLocation(marker.getPosition(), shape)) {
                    marker.setMap(null);
                }
            });
        }

        google.maps.event.clearListeners(map, 'mousedown');
        google.maps.event.clearListeners(map, 'mouseup');

        enableMap();

        map.setOptions({draggableCursor: 'pointer' });

        drawingButtonEl.classList.remove('hide');
        instructionEl.classList.add('hide');

    });
}

