class ShapeOverlay extends google.maps.OverlayView {
    constructor(bounds, coords, color, text, type, shapeId) {
        super();
        this.bounds = bounds;
        this.color = color;
        this.text = text;
        this.type = type;
        this.shapeId = shapeId;

        if (coords) {
            this.position = coords;
        }

        // if (coords && !bounds) {
        //     bounds = new google.maps.LatLngBounds();
        //     bounds.extend(new google.maps.LatLng(coords.lat(), coords.lng()));
        //     this.bounds = bounds;
        // }
    }

    onAdd() {
        this.containerDiv = document.createElement('div');
        this.containerDiv.classList.add('shape-overlay-container');
        this.containerDiv.style.textAlign = 'center';
        this.containerDiv.style.position = 'absolute';

        this.div = document.createElement('div');
        this.div.style.display = 'inline-block';
        this.div.style.padding = '4px 8px';
        this.div.style.backgroundColor = this.color;
        this.div.classList.add('shape-overlay');
        this.containerDiv.appendChild(this.div);

        const moveDiv = document.createElement('div');
        moveDiv.style.display = 'inline-block';
        const moveIcon = document.createElement('i');
        moveIcon.classList.add('fa');
        moveIcon.classList.add('fa-arrows-alt');
        moveIcon.style.fontSize = '14px';
        moveIcon.style.color = '#fff';
        moveIcon.style.cursor = 'pointer';
        moveIcon.style.marginRight = '5px';
        moveDiv.appendChild(moveIcon);

        this.div.appendChild(moveDiv);

        this.textSpan = document.createElement('span');
        this.textSpan.style.fontSize = '16px';
        this.textSpan.style.fontWeight = 'bold';
        this.textSpan.style.color = '#fff';
        this.textSpan.style.paddingRight = '10px';
        this.textSpan.innerHTML = this.text;
        this.div.appendChild(this.textSpan);

        const actionsDiv = document.createElement('div');
        actionsDiv.style.display = 'inline-block';

        const editDiv = document.createElement('div');
        editDiv.style.display = 'inline-block';
        const editIcon = document.createElement('i');
        editIcon.classList.add('fa');
        editIcon.classList.add('fa-edit');
        editIcon.style.fontSize = '14px';
        editIcon.style.color = '#fff';
        editIcon.style.cursor = 'pointer';
        editDiv.appendChild(editIcon);
        actionsDiv.appendChild(editDiv);
        editDiv.addEventListener('click', (e) => {
            e.stopPropagation();
            google.maps.event.trigger(this, 'edit');
        });

        const deleteDiv = document.createElement('div');
        deleteDiv.style.display = 'inline-block';
        const deleteIcon = document.createElement('i');
        deleteIcon.classList.add('fa');
        deleteIcon.classList.add('fa-trash');
        deleteIcon.style.fontSize = '14px';
        deleteIcon.style.color = '#fff';
        deleteIcon.style.cursor = 'pointer';
        deleteDiv.appendChild(deleteIcon);
        actionsDiv.appendChild(deleteDiv);
        deleteDiv.addEventListener('click', (e) => {
            e.stopPropagation();
            google.maps.event.trigger(this, 'delete');
        });

        const toggleVisibilityDiv = document.createElement('div');
        toggleVisibilityDiv.style.display = 'inline-block';
        const toggleVisibilityIcon = document.createElement('i');
        toggleVisibilityIcon.classList.add('fa');
        toggleVisibilityIcon.classList.add('fa-eye');
        toggleVisibilityIcon.style.fontSize = '14px';
        toggleVisibilityIcon.style.color = '#fff';
        toggleVisibilityIcon.style.cursor = 'pointer';
        toggleVisibilityDiv.appendChild(toggleVisibilityIcon);
        toggleVisibilityDiv.addEventListener('click', (e) => {
            e.stopPropagation();
            google.maps.event.trigger(this, 'toggle-visibility');
        });

        actionsDiv.appendChild(toggleVisibilityDiv);

        this.div.appendChild(actionsDiv);

        const panes = this.getPanes();
        panes.overlayMouseTarget.appendChild(this.containerDiv);

        google.maps.event.addDomListener(this.get('map').getDiv(),
            'mouseleave',
            () => {
                google.maps.event.trigger(moveDiv, 'mouseup');
            }
        );

        google.maps.event.addDomListener(moveDiv,
            'mousedown',
            (e) => {
                e.stopPropagation();

                moveDiv.style.cursor='move';
                this.origin = e;

                this.moveHandler = google.maps.event.addDomListener(this.get('map').getDiv(),
                    'mousemove',
                    (e) => {
                        const origin = this.origin,
                            left   = origin.clientX-e.clientX,
                            top    = origin.clientY-e.clientY,
                            pos    = this.getProjection().fromLatLngToDivPixel(this.position),
                            latLng = this.getProjection().fromDivPixelToLatLng(new google.maps.Point(pos.x-left, pos.y-top));
                        this.origin = e;
                        this.position = latLng;
                        // this.draw();

                        google.maps.event.trigger(this, 'move', { position: this.position });
                    });

                google.maps.event.addDomListener(moveDiv, 'click', (e) => {
                    e.stopPropagation();
                });

                google.maps.event.addDomListener(moveDiv, 'mouseup', (e) => {
                    if (e) {
                        e.stopPropagation();
                    }

                    moveDiv.style.cursor = 'pointer';
                    google.maps.event.removeListener(this.moveHandler);

                });
            });
    }

    onRemove() {
        this.containerDiv.parentNode.removeChild(this.containerDiv);
    }

    draw() {
        if (this.type === 'marker' || this.type === 'label') {
            const pos = this.getProjection().fromLatLngToDivPixel(this.position);
            this.containerDiv.style.left = pos.x - 100 + 'px';
            this.containerDiv.style.top = pos.y + 'px';
            this.div.style.position = 'relative';
            this.div.style.top = '20px';
            this.containerDiv.style.width = "200px";
            this.containerDiv.style.height = "200px";
        } else {
            if (!this.bounds) return;
            const overlayProjection = this.getProjection();
            const sw = overlayProjection.fromLatLngToDivPixel(
                this.bounds.getSouthWest()
            );
            const ne = overlayProjection.fromLatLngToDivPixel(
                this.bounds.getNorthEast()
            );

            if (!this.containerDiv) return;
            this.containerDiv.style.left = sw.x + "px";
            this.containerDiv.style.top = ne.y + "px";
            this.div.style.position = 'relative';
            this.div.style.top = ((sw.y - ne.y) / 2 + "px");
            this.containerDiv.style.width = ne.x - sw.x + "px";
            this.containerDiv.style.height = sw.y - ne.y + "px";
        }
    }

    setName(text) {
        if (text === this.text) return;
        this.text = text;
        this.textSpan.innerHTML = text;
    }

    setColor(color) {
        if (color === this.color) return;
        this.color = color;
        this.div.style.backgroundColor = this.color;
    }

    hide() {
        this.containerDiv.style.display = 'none';
    }

    show() {
        this.containerDiv.style.display = 'block';
    }
}