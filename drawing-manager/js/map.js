/*****   Define const variables   ******/
const HAND_MODE = 0;
const MARKER_MODE = 1;
const POLYGON_MODE = 2;
const LINE_MODE = 3;
const RECTANGLE_MODE = 4;
const CIRCLE_MODE = 5;
const LABEL_MODE = 6;
const MEASURE_MODE = 7;
const MOVE_MODE = 8;
const MODE_LABEL = ["Hand", "Marker", "Polygon", "Polyline", "Rectangle", "Circle", "Label", "Measure", "Move"];
const SET_TYPE = 0;
const EDIT_TYPE = 1;
const LABEL_FONT_SIZE = 18;
const MARKER_LABEL_FONT_SIZE = 16;
const defaultColor = palette && palette[0];

/*****    Define global variables  *****/

let marker_array;
let shape_array;
let label_array;
let measure_array;
let overlay_array = [];

let showMarkers = true;
let showShapes = true;
let showLabels = true;
let showMeasurements = true;

let mode = HAND_MODE;
let type = SET_TYPE;
let idToEdit;
let overlayName;
let color;

/******* variables for Map  ***********/
let map = null;
let drawingManager;
jq2 = jQuery.noConflict();

/******** variables for measure tool ******/
let isMeasureStatus = 0; // 0: start 1: dragging 2: end
let measureInfoWnd;

/******** dom elements ******/

let $showPoints;
let $showLengths;
let $toggleButtons;

let $menuEl;

function addOverlays(overlays) {
    overlays.forEach(function(overlay) {
        switch (overlay.type) {
            case 'polyline':
                const polyline = new google.maps.Polyline(overlay.data);
                polyline.setMap(map);
                mode = LINE_MODE;
                handleShapeOverlay(polyline, overlay.name, overlay.color, overlay.type);
                break;
            case 'polygon':
                const polygon = new google.maps.Polygon(overlay.data);
                polygon.setMap(map);
                mode = POLYGON_MODE;
                handleShapeOverlay(polygon, overlay.name, overlay.color, overlay.type);
                break;
            case 'rectangle':
                const rectangle = new google.maps.Rectangle(overlay.data);
                rectangle.setMap(map);
                mode = RECTANGLE_MODE;
                handleShapeOverlay(rectangle, overlay.name, overlay.color, overlay.type);
                break;
            case 'circle':
                const circle = new google.maps.Circle(overlay.data);
                circle.setMap(map);
                handleShapeOverlay(circle, overlay.name, overlay.color, overlay.type);
                break;
            case 'marker':
                const marker = new google.maps.Marker(overlay.data);
                marker.setMap(map);
                mode = MARKER_MODE;
                handleMarkerOverlay(marker, overlay.name, overlay.color, overlay.type);
                break;
            case 'label':
                const label = new google.maps.Marker(Object.assign({}, overlay.data, {id: label_id}));
                label.setMap(map);
                mode = LABEL_MODE;
                handleLabelOverlay(label, overlay.name, overlay.color, overlay.type);
                break;
            case 'measure_polyline':
                const polylinePath = overlay.data.path;
                mode = MEASURE_MODE;
                polylinePath.forEach(function(node) {
                    addMeasureNode(new google.maps.LatLng(node), overlay.name, overlay.color, overlay.mode);
                });
                break;
            case 'measure_polygon':
                const polygonPath = overlay.data.path;
                mode = MEASURE_MODE;
                polygonPath.forEach(function(node) {
                    addMeasureNode(new google.maps.LatLng(node), overlay.name, overlay.color, 'area');
                });
                createMeasurePolygon(measure_array[measure_array.length - 1].tool, overlay.color);
                break;
        }
    })
}

function addMeasureNode(latLng, name, color, measureMode) {
    const id = uuidv4();
    if (!measureMode) {
        measureMode = jq2('input[name="measure-mode"]:checked').val();
    }
    if (isMeasureStatus == 0) {
        let meLine = new google.maps.Polyline({
            id: id,
            map: map,
            path: [],
            strokeColor: color,
            strokeOpacity: 1.0,
            strokeWeight: 2,
            clickable: true,
            draggable: true,
            editable: true,
            zIndex: MEASURE_MODE,
            mode: measureMode
        });

        google.maps.event.addListener(meLine.getPath(), 'set_at', function () {
            let paths = meLine.getPath();
            updateMeasureLine(meLine.id, paths, 'polyline');
        });

        google.maps.event.addListener(meLine.getPath(), 'insert_at', function () {
            let paths = meLine.getPath();
            updateMeasureLine(meLine.id, paths, 'polyline');
        });

        google.maps.event.addListener(meLine, 'mousemove', function (event) {
            let appendHtml = '';
            let cal = calculateMeasure(this.id, this.getPath());
            if (cal) {
                if (measureMode === 'linear') {
                    appendHtml = 'distance : <span class="measure-distance">' + cal[0] + '</span> linear feet <br>';
                } else if (measureMode === 'area') {
                    appendHtml = 'distance : <span class="measure-distance">' + cal[0] + '</span> linear feet <br>'
                        + 'area : <span class="measure-area">' + cal[1] + '</span> square feet <br>' +
                        '<span>Roof squares: ' + cal[2] + '</span> ';
                }

                if (measureInfoWnd) {
                    measureInfoWnd.close();
                }
                measureInfoWnd = new google.maps.InfoWindow({
                    content: appendHtml
                });

                let latLng = event.latLng;
                measureInfoWnd.setPosition(latLng);
                measureInfoWnd.open(map);
            }
        });

        google.maps.event.addListener(meLine, 'mouseout', function () {
            if (measureInfoWnd) {
                measureInfoWnd.close();
            }
        });

        google.maps.event.addListener(meLine, 'mousedown', function (event) {
            if (event.vertex === 0) {

                createMeasurePolygon(this, color);
            }
        });

        let sMeasure = new Tool(meLine, id, name, color, 'measure_polyline');
        measure_array.push(sMeasure);

        let appendHtml = `
            <li id="${id}">
                <span class="name-span">${name}</span>
                <span class="color-span" style="background-color: ${color}">&nbsp; &nbsp; &nbsp; &nbsp;</span>
                <a href="#" onclick="onEditMeasure('${id}');" data-toggle="modal" data-target="#set-options-modal"><i class="fa fa-edit" aria-hidden="true"></i></a>
                <a href="#" onclick="onDeleteMeasure('${id}');"><i class="fa fa-trash" aria-hidden="true"></i></a>
                <a href="#" class="btn-visibility" onclick="onToggleVisibleMeasurements('${id}');"><i class="fa fa-eye" aria-hidden="true"></i></a>
                <br>
                Measurements:<br>
                distance : <span class="measure-distance">0.000</span> linear feet <br>
                area : <span class="measure-area">0.000</span> square feet <br>
                Roof squares : <span class="measure-roof">0.000</span><br>
                <div style="display:flex" class="measure-points-wrapper"><span style="width:25%">points: </span><span class="measure-points"></span></div> <br>
                <div style="display:flex" class="measure-lengths-wrapper"><span style="width:25%">lengths: </span><span class="measure-lengths"></span></div> <br>
            </li>`;

        jq2('.measure-list').append(appendHtml);
        isMeasureStatus = 1;
    }

    let measureLine = measure_array[measure_array.length - 1].tool;
    measureLine.getPath().push(latLng);
    measure_array[measure_array.length - 1].tool = measureLine;

    if (isMeasureStatus == 2) {
        isMeasureStatus = 0;
    }
}

function calculateDistance(point1, point2) {
    return google.maps.geometry.spherical.computeDistanceBetween(point1, point2) * 3.28084;
}

/**
 * calculate area and length by measure tool
 * @param paths : measure points
 */
function calculateMeasure(mLineId, paths) {
    let ret = [];
    if (paths.length > 1) {
        let len = google.maps.geometry.spherical.computeLength(paths) / 0.3048;
        len = len.toFixed(3);

        let square = google.maps.geometry.spherical.computeArea(paths) / 0.3048 / 0.3048;
        square = square.toFixed(3);

        let roofSq = Math.round(square / 100);

        let li = jq2(".measure-list").find('li input[value=' + mLineId + '].option-id').parent();

        li.find('.measure-distance').text(len);
        li.find('.measure-area').text(square);
        li.find('.measure-roof').text(roofSq);
        ret.push(len);
        ret.push(square);
        ret.push(roofSq);
        return ret;
    }
}

function calculateInchPerPx() {
    return 156543.03392 * Math.cos(map.getCenter().lat() * Math.PI / 180) / Math.pow(2, map.getZoom()) * 39.3701;
}

/**
 * Create Maker with color param
 * @param  color : Marker color
 */
function createMarkerSymbol(color) {
    return {
        path: 'M 0,0 C -2,-20 -10,-22 -10,-30 A 10,10 0 1,1 10,-30 C 10,-22 2,-20 0,0 z',
        fillColor: color,
        fillOpacity: 1,
        strokeColor: '#e7e7e7',
        strokeWeight: 1.5,
        scale: 1,
        labelOrigin: new google.maps.Point(0, 10)
    };
}

/**
 * Create Label with color param
 * @param  color : Label text color
 */
function createLabelSymbol(color) {
    return {
        path: 'M 0,0 C -2,-20 -10,-22 -10,-30 A 10,10 0 1,1 10,-30 C 10,-22 2,-20 0,0 z',
        fillColor: "#ffffff",
        fillOpacity: 0,
        strokeColor: color,
        strokeWeight: 0,
        scale: 1,
        labelOrigin: new google.maps.Point(0, 0)
    };
}

function createMeasurePolygon(polyline, color) {
    polyline.setMap(null);

    let mePolygon = new google.maps.Polygon({
        id: polyline.id,
        map: map,
        path: polyline.getPath(),
        fillOpacity: 0,
        strokeColor: color,
        strokeWeight: 2,
        clickable: true,
        editable: true,
        zIndex: MEASURE_MODE
    });

    google.maps.event.addListener(mePolygon, 'mousemove', function (event) {
        let cal = calculateMeasure(this.id, this.getPath());
        let appendHtml = 'distance : <span class="measure-distance">' + cal[0]
          + '</span> linear feet <br>'
          + 'area : <span class="measure-area">' + cal[1] + '</span> square feet <br>'
          + '<span>Roof squares: ' + cal[2] + '</span> ';

        if (measureInfoWnd) {
            measureInfoWnd.close();
        }
        measureInfoWnd = new google.maps.InfoWindow({
            content: appendHtml
        });

        let latLng = event.latLng;
        measureInfoWnd.setPosition(latLng);
        measureInfoWnd.open(map);
    });

    google.maps.event.addListener(mePolygon, 'mouseout', function () {
        measureInfoWnd.close();
    });

    measure_array[measure_array.length - 1].tool = mePolygon;
    measure_array[measure_array.length - 1].type = 'measure_polygon';
    updateMeasureLine(mePolygon.id, mePolygon.getPath(), 'polygon');
    isMeasureStatus = 0;
}

function getMapParams() {
    const zoom = map.getZoom();
    const center = map.getCenter();
    const overlays = [];

    shape_array.concat(marker_array).concat(label_array).concat(measure_array).forEach(function(el) {
        let elParams = {};
        if (!el.visible) return;

        elParams.type = el.type;
        elParams.name = el.name;
        elParams.color = el.color;
        elParams.data = {};
        switch(el.type) {
            case 'polyline':
                getShapeParams(elParams.data, el.tool);
                elParams.data.path = el.tool.getPath().getArray();
                break;
            case 'polygon':
                getShapeParams(elParams.data, el.tool);
                elParams.data.paths = el.tool.getPath().getArray();
                break;
            case 'rectangle':
                getShapeParams(elParams.data, el.tool);
                elParams.data.bounds = el.tool.getBounds();
                break;
            case 'circle':
                getShapeParams(elParams.data, el.tool);
                elParams.data.radius = el.tool.getRadius();
                elParams.data.center = el.tool.getCenter();
                break;
            case 'marker':
            case 'label':
                elParams.data.position = el.tool.getPosition();
                elParams.data.icon = el.tool.getIcon();
                elParams.data.label = el.tool.getLabel();
                break;
            case 'measure_polygon':
            case 'measure_polyline':
                elParams.data.path = el.tool.getPath().getArray();
                break;
        }

        overlays.push(elParams);
    });

    return {
        zoom: zoom,
        center: center,
        overlays: overlays
    }
}

function getShapeParams(params, tool) {
    params.fillColor = tool.get('fillColor');
    params.strokeColor = tool.get('strokeColor');
    params.strokeWeight = tool.get('strokeWeight');
    params.fillOpacity = tool.get('fillOpacity');
    params.strokeOpacity = tool.get('strokeOpacity');
}

function handleShapeOverlay(overlay, name, color, type) {
    const id = uuidv4();
    let appendHtml = `
    <li id="${id}">
        <span  class="name-span">${name}</span>
        <span class="shape-span" style="display:none;">${type}</span> 
        <span class="color-span" style="background-color: ${color}">&nbsp; &nbsp; &nbsp; &nbsp;</span>
        <a href="#" class="btn-edit-option" onclick="onEditShape('${id}');">
            <i class="fa fa-edit" aria-hidden="true"></i>
        </a>
        <a href="#" onclick="onDeleteShape('${id}');"><i class="fa fa-trash" aria-hidden="true"></i></a>
        <a href="#"  class="btn-visibility ${showShapes ? "" : "inactive"}" onclick="onToggleVisibleShapes('${id}');">
            <i class="fa fa-eye" aria-hidden="true"></i>
        </a>
    </li>`;
    jq2('.shape-list').append(appendHtml);

    overlay.id = id;
    let cOverlay = new Tool(overlay, id, name, color, type, showShapes);
    shape_array.push(cOverlay);
    setShapeName(mode);

    google.maps.event.addListener(overlay, 'rightclick', function (e) {
        if (mode == HAND_MODE || mode == MOVE_MODE){
            this.setEditable(true);
        }
    });

    const bounds = getOverlayBounds(overlay, type);
    const coords = getShapeCoords(overlay, type);

    const newOverlay = new ShapeOverlay(bounds, coords, color, name, type, overlay.id);

    newOverlay.setMap(map);
    overlay_array.push(newOverlay);

    google.maps.event.addDomListener(newOverlay, "edit", function () {
        onEditShape(newOverlay.shapeId);
    });

    google.maps.event.addDomListener(newOverlay, "delete", function () {
        onDeleteShape(newOverlay.shapeId);
    });

    google.maps.event.addDomListener(newOverlay, "toggle-visibility", function (e) {
        onToggleVisibleShapes(newOverlay.shapeId);
    });

    google.maps.event.addDomListener(newOverlay, "move", function (e) {
        if (type === 'polyline' || type === 'polygon') {
            const position = overlay.getPath().getAt(0);
            const newPosition = e.position;
            const xDiff = position.lat() - newPosition.lat();
            const yDiff = position.lng() - newPosition.lng();
            const newPoints = [];
            overlay.getPath().forEach(item => {
                const x = item.lat();
                const y = item.lng();
                const newX = x - xDiff;
                const newY = y - yDiff;
                newPoints.push(new google.maps.LatLng(newX, newY));
            });
            overlay.setPath(newPoints);
        }

        if (type === 'circle') {
            overlay.setCenter(e.position);
        }

        if (type === 'rectangle') {
            const bounds = overlay.getBounds();
            const sw = bounds.getSouthWest();
            const ne = bounds.getNorthEast();

            const newPosition = e.position;
            const xDiff = sw.lat() - newPosition.lat();
            const yDiff = sw.lng() - newPosition.lng();

            const newswx = sw.lat() - xDiff;
            const newswy = sw.lng() - yDiff;
            const newsw = new google.maps.LatLng(newswx, newswy);
            const newnex = ne.lat() - xDiff;
            const newney = ne.lng() - yDiff;
            const newne = new google.maps.LatLng(newnex, newney);

            const newBounds = new google.maps.LatLngBounds();
            newBounds.extend(newsw);
            newBounds.extend(newne);
            overlay.setBounds(newBounds);
        }

        newOverlay.bounds = getOverlayBounds(overlay, type);
        newOverlay.draw();

    });

    if (type === 'rectangle' || type === 'circle') {
        google.maps.event.addListener(overlay, 'bounds_changed', function(e) {
            moveShapeOverlay(overlay, type);
        });
    }

    if (type === 'polygon') {
        overlay.getPaths().forEach(function(path, index){
            google.maps.event.addListener(path, 'insert_at', function(){
                moveShapeOverlay(overlay, type);
            });
            google.maps.event.addListener(path, 'remove_at', function(){
                moveShapeOverlay(overlay, type);
            });
            google.maps.event.addListener(path, 'set_at', function(){
                moveShapeOverlay(overlay, type);
            });
        });

        google.maps.event.addListener(overlay, 'drag', function(e) {
            moveShapeOverlay(overlay, type);
        });
    }

    if (type === 'polyline') {
        google.maps.event.addListener(overlay.getPath(), "insert_at", function() {
            moveShapeOverlay(overlay, type);
        });
        google.maps.event.addListener(overlay.getPath(), "remove_at", function() {
            moveShapeOverlay(overlay, type);
        });
        google.maps.event.addListener(overlay.getPath(), "set_at", function() {
            moveShapeOverlay(overlay, type);
        });
        google.maps.event.addListener(overlay, "drag", function() {
            moveShapeOverlay(overlay, type);
        });
    }
}

function getShapeCoords(overlay, type) {
    let coords;
    if (type === 'polyline' || type === 'polygon') {
        coords = overlay.getPath().getAt(0);
    }
    if (type === 'rectangle') {
        coords = overlay.getBounds().getSouthWest();
    }
    if (type === 'circle') {
        coords = overlay.getCenter();
    }
    return coords;
}

function moveShapeOverlay(overlay, type) {
    const newOverlay = overlay_array.find(o => o.shapeId === overlay.id);
    if (newOverlay) {
        const bounds = getOverlayBounds(overlay, type);
        newOverlay.bounds = bounds;
        newOverlay.draw();
    }
}

function getOverlayBounds(overlay, type) {
    let bounds;

    if (type === 'polygon' || type === 'polyline') {
        bounds = new google.maps.LatLngBounds();
        overlay.getPath().forEach((item) => {
            bounds.extend(new google.maps.LatLng(item.lat(), item.lng()));
        });
    }

    if (type === 'rectangle' || type === 'circle') {
        bounds = overlay.getBounds();
    }

    return bounds;
}

function handleMarkerOverlay(overlay, name, color, type) {
    const id = uuidv4();
    let appendHtml = `
    <li id="${id}">
        <span class="name-span">${name}</span>
        <span class="color-span" style="background-color: ${color}">&nbsp; &nbsp; &nbsp; &nbsp;</span>
        <a href="#" class="btn-edit-option" onclick="onEditMarker('${id}');">
            <i class="fa fa-edit" aria-hidden="true"></i>
        </a>
        <a href="#" onclick="onDeleteMarker('${id}');">
            <i class="fa fa-trash" aria-hidden="true"></i>
        </a>
        <a href="#" class="btn-visibility" onclick="onToggleVisibleMarkers('${id}');">
            <i class="fa fa-eye" aria-hidden="true"></i>
        </a>
    </li>
    `;
    jq2('.marker-list').append(appendHtml);

    overlay.id = id;
    let cMarker = new Tool(overlay, id, name, color, type);
    marker_array.push(cMarker);
    setShapeName(mode);

    const newOverlay = new ShapeOverlay(null, overlay.getPosition(), color, name, type, overlay.id);
    newOverlay.setMap(map);
    overlay_array.push(newOverlay);

    google.maps.event.addDomListener(newOverlay, "edit", function () {
        onEditMarker(newOverlay.shapeId);
    });

    google.maps.event.addDomListener(newOverlay, "delete", function () {
        onDeleteMarker(newOverlay.shapeId);
    });

    google.maps.event.addDomListener(newOverlay, "toggle-visibility", function () {
        onToggleVisibleMarkers(newOverlay.shapeId);
    });

    google.maps.event.addDomListener(newOverlay, 'move', function(e) {
        overlay.setPosition(e.position);
    });

    google.maps.event.addListener(overlay, "drag", function() {
        const newOverlay = overlay_array.find(o => o.shapeId === overlay.id);
        if (newOverlay) {
            const coords = overlay.getPosition();
            newOverlay.position = coords;
            newOverlay.draw();
        }
    });
}

function handleLabelOverlay(overlay, name, color, type) {
    const id = uuidv4();
    overlay.id = id;
    let appendHtml = `
        <li id="${id}">
            <span class="name-span">${name}</span>
            <span class="color-span" style="background-color: ${color}">&nbsp; &nbsp; &nbsp; &nbsp;</span>
            <a href="#" class="btn-edit-option" onclick="onEditLabel('${id}');">
                <i class="fa fa-edit" aria-hidden="true"></i>
            </a>
            <a href="#" onclick="onDeleteLabel('${id}');">
                <i class="fa fa-trash" aria-hidden="true"></i>
            </a>
            <a href="#" class="btn-visibility" onclick="onToggleVisibleLabels('${id}');">
                <i class="fa fa-eye" aria-hidden="true"></i>
            </a>
        </li>
    `;
    jq2('.label-list').append(appendHtml);

    let cLabel = new Tool(overlay, id, name, color, type);
    label_array.push(cLabel);
    setShapeName(mode);

    const newOverlay = new ShapeOverlay(null, overlay.getPosition(), color, name, type, overlay.id);
    newOverlay.setMap(map);
    overlay_array.push(newOverlay);

    google.maps.event.addDomListener(newOverlay, "edit", function () {
        onEditLabel(newOverlay.shapeId);
    });

    google.maps.event.addDomListener(newOverlay, "delete", function () {
        onDeleteLabel(newOverlay.shapeId);
    });

    google.maps.event.addDomListener(newOverlay, "toggle-visibility", function (e) {
        onToggleVisibleLabels(newOverlay.shapeId);
    });

    google.maps.event.addDomListener(newOverlay, 'move', function(e) {
        overlay.setPosition(e.position)
    });

    google.maps.event.addListener(overlay, "drag", function() {
        const newOverlay = overlay_array.find(o => o.shapeId === overlay.id);
        if (newOverlay) {
            const coords = overlay.getPosition();
            newOverlay.position = coords;
            newOverlay.draw();
        }
    });
}

function isShowPoints() {
    return $showPoints.is(':checked');
}

function isShowLengths() {
    return $showLengths.is(':checked');
}

/**
 * Initialize Map
 */



function initMap(data = {}) {

    marker_array = [];
    shape_array = [];
    label_array = [];
    measure_array = [];

    jq2('.shape-list').html('');
    jq2('.marker-list').html('');
    jq2('.label-list').html('');
    jq2('.measure-list').html('');

    const location = data.center || {lat: 26.6309231, lng: -81.8286991};
    const zoom = data.zoom || 18;

    map = new google.maps.Map(document.getElementById('map'), {
        zoom: zoom,
        mapTypeId: "hybrid",
        tilt: 0,
        center: location,
        gestureHandling: 'cooperative',
        zoomControl: true,
        zoomControlOptions: {
            position: google.maps.ControlPosition.LEFT_TOP
        },
        scaleControl: false,
        streetViewControl: false,
        rotateControl: false
    });

    google.maps.event.addListener(map, 'mousemove', function (event) {

    });

    google.maps.event.addListener(map, "rightclick", function(event) {
        if ($menuEl) {
            $menuEl.remove();
        }

        const menuHtml = `
            <ul id="action-menu">
                <li id="use-hand">Use Hand</li>
                <li id="add-marker">Add Marker</li>
                <li id="add-polygon">Add Polygon</li>
                <li id="add-line">Add Line</li>
                <li id="add-rectangle">Add Rectangle</li>
                <li id="add-circle">Add Circle</li>
                <li id="add-label">Add Label</li>
                <li id="add-measure">Add Measurements</li>
            </ul>
        `;

        $menuEl = jq2(menuHtml);

        $menuEl.css('position', 'absolute');
        $menuEl.css('top', event.pixel.y + 52);
        $menuEl.css('left', event.pixel.x);

        $menuEl.on('click', '#use-hand', () => {
            jq2('#hand').click();
        });

        $menuEl.on('click', '#add-marker', () => {
            jq2('#marker').click();
        });

        $menuEl.on('click', '#add-polygon', () => {
            jq2('#polygon').click();
        });

        $menuEl.on('click', '#add-line', () => {
            jq2('#line').click();
        });

        $menuEl.on('click', '#add-rectangle', () => {
            jq2('#rectangle').click();
        });

        $menuEl.on('click', '#add-circle', () => {
            jq2('#circle').click();
        });

        $menuEl.on('click', '#add-label', () => {
            jq2('#label').click();
        });

        $menuEl.on('click', '#add-measure', () => {
            jq2('#measure').click();
        });

        jq2('body').append($menuEl);
    });


    if (data.overlays) {
        addOverlays(data.overlays);
    }

    drawingManager = new google.maps.drawing.DrawingManager({drawingControl: false});
    drawingManager.setMap(map);

    google.maps.event.addListener(drawingManager, 'overlaycomplete', function (event) {
        let gOverlay = event.overlay;

        if (event.type == 'marker') {
            handleMarkerOverlay(gOverlay, overlayName, color, event.type);
        }
        if (event.type == 'circle' || event.type == 'rectangle' || event.type == 'polyline' || event.type == 'polygon') {
            handleShapeOverlay(gOverlay, overlayName, color, event.type);
        }
    });

    /**
     *  Click event on Map for label tool
     */
    map.addListener('click', function (args) {
        const id = uuidv4();

        if (mode == LABEL_MODE) {
            let gLabel = new google.maps.Marker({
                position: args.latLng,
                label: {
                    text: overlayName,
                    color: color,
                    fontSize: LABEL_FONT_SIZE + "px",
                    fontWeight: "bold"
                },
                icon: createLabelSymbol(color),
                map: map
            });

            handleLabelOverlay(gLabel, overlayName, color, 'label');
        }

        if (mode == MEASURE_MODE) {
            setTimeout(function () {
                addMeasureNode(args.latLng, overlayName, color);
            }, 300);
        }
    });

    /**
     * double click event on map when using measure tool
     */
    map.addListener('dblclick', function () {
        if (mode == MEASURE_MODE && isMeasureStatus != 0) {
            isMeasureStatus = 2;
        }
    });

    /**
     * close infowindow for measure when mouse moving
     */
    map.addListener('mousemove', function () {
        if (measureInfoWnd && mode == MEASURE_MODE) {
            measureInfoWnd.close();
        }
    });

}


/**
 * update measure line values
 */
function updateMeasureLine(mLineId, paths, type) {
    calculateMeasure(mLineId, paths);
    let li = jq2(".measure-list").find('li input[value=' + mLineId + '].option-id').parent();
    if (isShowPoints()) {
        li.find('.measure-points').empty();
        for (let i = 0; i < paths.length; i++) {
            li.find('.measure-points').append("(" + paths.getAt(i).lat() + ", " + paths.getAt(i).lng() + ")");
        }
    }

    if (isShowLengths() && paths.length > 1) {
        const $lengthsList = li.find('.measure-lengths');
        let lengths = '';

        for (let i = 1; i < paths.length; i++) {
            lengths += Math.round(calculateDistance(paths.getAt(i-1), paths.getAt(i))) + '<br/>';
        }

        if (type === 'polygon') {
            lengths += Math.round(calculateDistance(paths.getAt(0), paths.getAt(paths.length - 1))) + '<br/>';
        }

        $lengthsList.html(lengths);
    }
}

/**
 * Set Name & Color options for each tool
 */
function setDrawingOptions() {
    overlayName = jq2("#modal-name").val();
    color = jq2("#modal-color").val();
    let id = idToEdit;

    if (type == SET_TYPE) {
        jq2('#menu').find('.btn-mode').removeClass('mode-selected');
        jq2('#menu').find('.btn-mode').eq(mode).addClass('mode-selected');
        map.setOptions({disableDoubleClickZoom: false, clickableIcons: true});

        if (mode == MARKER_MODE) {
            drawingManager.setOptions({
                drawingMode: google.maps.drawing.OverlayType.MARKER,
                markerOptions: {
                    label: {
                        text: overlayName,
                        color: color,
                        fontSize: MARKER_LABEL_FONT_SIZE + "px",
                        fontWeight: "bold"
                    },
                    icon: createMarkerSymbol(color),
                }
            });
        }
        if (mode == MEASURE_MODE) {
            map.setOptions({disableDoubleClickZoom: true, clickableIcons: false, draggableCursor: 'crosshair'});
            drawingManager.setOptions({
                drawingMode: google.maps.drawing.OverlayType.HAND
            });
        }
        if (mode == POLYGON_MODE) {
            drawingManager.setOptions({
                drawingMode: google.maps.drawing.OverlayType.POLYGON,
                polygonOptions: {
                    fillColor: color,
                    strokeColor: color,
                    strokeWeight: 2,
                    clickable: true,
                    zIndex: POLYGON_MODE
                }
            });
        }
        if (mode == LINE_MODE) {
            drawingManager.setOptions({
                drawingMode: google.maps.drawing.OverlayType.POLYLINE,
                polylineOptions: {
                    strokeColor: color,
                    strokeWeight: 2,
                    clickable: true,
                    zIndex: LINE_MODE
                }
            });
        }
        if (mode == RECTANGLE_MODE) {
            drawingManager.setOptions({
                drawingMode: google.maps.drawing.OverlayType.RECTANGLE,
                rectangleOptions: {
                    fillColor: color,
                    strokeColor: color,
                    strokeWeight: 2,
                    clickable: true,
                    zIndex: RECTANGLE_MODE
                }
            });
        }
        if (mode == CIRCLE_MODE) {
            drawingManager.setOptions({
                drawingMode: google.maps.drawing.OverlayType.CIRCLE,
                circleOptions: {
                    fillColor: color,
                    strokeColor: color,
                    strokeWeight: 2,
                    clickable: true,
                    zIndex: CIRCLE_MODE
                }
            });
        }
        if (mode == LABEL_MODE) {
            drawingManager.setOptions({
                drawingMode: google.maps.drawing.OverlayType.HAND
            });
        }
    } else if (type == EDIT_TYPE) {
        if (mode == MARKER_MODE) {
            let li = jq2("#"+id);
            li.find('.name-span').text(overlayName);
            li.find('.color-span').css('background-color', color);
            const marker = marker_array.find(marker => marker.id == id);
            const label = marker.tool.label;
            label.text = overlayName;
            label.color = color;
            marker.tool.setLabel(label);
            marker.tool.setIcon(createMarkerSymbol(color));
            marker.name = overlayName;
            marker.color = color;
        }
        if (mode == CIRCLE_MODE || mode == LINE_MODE || mode == POLYGON_MODE || mode == RECTANGLE_MODE) {
            let li = jq2(".shape-list").find('#'+id);
            li.find('.name-span').text(overlayName);
            li.find('.color-span').css('background-color', color);
            shape_array.forEach(function (obj) {
                if (obj.tool.id == id) {
                    obj.tool.setMap(null);

                    let tmpOverlay;
                    if (mode == CIRCLE_MODE) {
                        tmpOverlay = new google.maps.Circle({
                            id: obj.tool.id,
                            fillColor: color,
                            strokeColor: color,
                            strokeWeight: 2,
                            clickable: false,
                            zIndex: CIRCLE_MODE,
                            map: map,
                            center: obj.tool.getCenter(),
                            radius: obj.tool.getRadius()
                        });
                    }
                    if (mode == LINE_MODE) {
                        tmpOverlay = new google.maps.Polyline({
                            id: obj.tool.id,
                            strokeColor: color,
                            strokeWeight: 2,
                            clickable: false,
                            zIndex: LINE_MODE,
                            map: map,
                            path: obj.tool.getPath()
                        });
                    }
                    if (mode == POLYGON_MODE) {
                        tmpOverlay = new google.maps.Polygon({
                            id: obj.tool.id,
                            fillColor: color,
                            strokeColor: color,
                            strokeWeight: 2,
                            clickable: false,
                            zIndex: POLYGON_MODE,
                            map: map,
                            paths: obj.tool.getPaths()
                        });
                    }
                    if (mode == RECTANGLE_MODE) {
                        tmpOverlay = new google.maps.Rectangle({
                            id: obj.tool.id,
                            fillColor: color,
                            strokeColor: color,
                            strokeWeight: 2,
                            clickable: false,
                            zIndex: RECTANGLE_MODE,
                            map: map,
                            bounds: obj.tool.getBounds()
                        });
                    }
                    obj.name = overlayName;
                    obj.color = color;
                    obj.tool = tmpOverlay;
                }
            });
        }
        if (mode == LABEL_MODE) {
            let li = jq2("#"+id);
            li.find('.name-span').text(overlayName);
            li.find('.color-span').css('background-color', color);
            const label = label_array.find(label => label.id === id);
            label.name = overlayName;
            label.color = color;
            const lbl = label.tool.label;
            lbl.text = overlayName;
            lbl.color = color;
            label.tool.setLabel(lbl);
        }
        if (mode == MEASURE_MODE) {
            let li = jq2("#"+id);
            li.find('.name-span').text(overlayName);
            li.find('.color-span').css('background-color', color);
            const measure = measure_array.find(measure => measure.id == id);
            measure.name = overlayName;
            measure.color = color;
            measure.tool.set('strokeColor', color);
        }
    }
    jq2('#set-options-modal').find('.close').trigger('click');

    const overlay = overlay_array.find(overlay => {
        return overlay.shapeId == id;
    });

    if (overlay) {
        overlay.setName(overlayName);
        overlay.setColor(color);
    }

    const el = jq2('#'+id);

    if (el) {
        el.find('.name-span').html(overlayName);
        el.find('.color-span').css('background-color', color);
    }
}

/**
 * Event when you fire edit button to marker
 */
function onEditMarker(id) {
    idToEdit = id;
    const marker = marker_array.find(marker => marker._id == id);
    if (!marker) return;
    let name = marker.name;
    let color = marker.color;
    mode = MARKER_MODE;
    type = EDIT_TYPE;
    jq2("#modal-name").val(name);
    jq2("#modal-color").val(color);
    jq2("#name-input").text(MODE_LABEL[mode] + " name");
    jq2("#color-input").text(MODE_LABEL[mode] + " color");
    jq2('.dropdown-toggle').css('backgroundColor', color);

    jq2('#set-options-modal').modal('show');
};

/**
 * Event when you fire delete button to marker
 */
function onDeleteMarker(id) {
    let marker = marker_array.find(marker => marker._id == id);
    if (!marker) return;
    jq2('#'+id).remove();
    const si = marker_array.indexOf(marker);
    marker_array[si].tool.setMap(null);
    marker_array.splice(si, 1);
    marker = null;

    let overlay = overlay_array.find(overlay => overlay.shapeId == id);
    if (!overlay) return;
    overlay.setMap(null);
    overlay = null;
    const oi = overlay_array.indexOf(overlay);
    overlay_array.splice(oi, 1);
};

/**
 * Event when you fire edit button to shape
 */
function onEditShape(id) {
    idToEdit = id;
    const shape = shape_array.find(shape => shape._id == id);
    if (!shape) return;
    let name = shape.name;
    let color = shape.color;
    type = EDIT_TYPE;
    mode = typeToMode(shape.type);
    jq2("#modal-name").val(name);
    jq2("#modal-color").val(color);
    jq2("#name-input").text(MODE_LABEL[mode] + " name");
    jq2("#color-input").text(MODE_LABEL[mode] + " color");
    jq2('.dropdown-toggle').css('backgroundColor', color);

    jq2('#set-options-modal').modal('show');
};

function typeToMode(type) {
    let mode;
    switch (type) {
        case 'polygon':
            mode = POLYGON_MODE;
            break;
        case 'polyline':
            mode = LINE_MODE;
            break;
        case 'circle':
            mode = CIRCLE_MODE;
            break;
        case 'rectangle':
            mode = RECTANGLE_MODE;
            break;
    }
    return mode;
}

/**
 * Event when you fire delete button to shape
 */
function onDeleteShape(id) {
    let shape = shape_array.find(shape => shape._id == id);
    if (!shape) return;
    jq2('#'+id).remove();
    const si = shape_array.indexOf(shape);
    shape_array[si].tool.setMap(null);
    shape_array.splice(si, 1);
    shape = null;

    let overlay = overlay_array.find(overlay => overlay.shapeId == id);
    if (!overlay) return;
    overlay.setMap(null);
    overlay = null;
    const oi = overlay_array.indexOf(overlay);
    overlay_array.splice(oi, 1);
};

/**
 * Event when you fire edit button to label
 */
function onEditLabel(id) {
    idToEdit = id;
    const label = label_array.find(label => label._id == id);
    if (!label) return;
    let name = label.name;
    let color = label.color;
    mode = LABEL_MODE;
    type = EDIT_TYPE;
    jq2("#modal-name").val(name);
    jq2("#modal-color").val(color);
    jq2("#name-input").text(MODE_LABEL[mode] + " name");
    jq2("#color-input").text(MODE_LABEL[mode] + " color");
    jq2('.dropdown-toggle').css('backgroundColor', color);

    jq2('#set-options-modal').modal('show');
};

/**
 * Event when you fire delete button to label
 */
function onDeleteLabel(id) {
    let label = label_array.find(label => label._id == id);
    if (!label) return;
    jq2('#'+id).remove();
    const si = label_array.indexOf(label);
    label_array[si].tool.setMap(null);
    label_array.splice(si, 1);
    label = null;

    let overlay = overlay_array.find(overlay => overlay.shapeId == id);
    if (!overlay) return;
    overlay.setMap(null);
    overlay = null;
    const oi = overlay_array.indexOf(overlay);
    overlay_array.splice(oi, 1);
};

/**
 * Event when you fire edit button to measure
 */
function onEditMeasure(id) {
    idToEdit = id;
    const marker = marker_array.find(marker => marker._id == id);
    if (!marker) return;
    let name = marker.name;
    let color = marker.color;
    mode = MEASURE_MODE;
    type = EDIT_TYPE;
    jq2("#modal-name").val(name);
    jq2("#modal-color").val(color);
    jq2("#name-input").text(MODE_LABEL[mode] + " name");
    jq2("#color-input").text(MODE_LABEL[mode] + " color");
    jq2('.dropdown-toggle').css('backgroundColor', color);

    jq2('#set-options-modal').modal('show');
};

/**
 * Event when you fire delete button to measure
 */
function onDeleteMeasure(id) {
    let measure = measure_array.find(measure => measure._id == id);
    if (!measure) return;
    jq2('#'+id).remove();
    const si = measure_array.indexOf(measure);
    measure_array[si].tool.setMap(null);
    measure_array.splice(si, 1);
    measure = null;
};

function onToggleVisibleMarkers(id, visible) {
    const marker = marker_array.find(label => label._id == id);
    if (!marker) return;
    const el = jq2('#'+id);
    const visIcon = el.find('.btn-visibility');
    const isVisible = marker._visible;
    marker._visible = visible ? visible : !isVisible;
    marker.tool.setMap(marker._visible ? map : null);
    visIcon.toggleClass('inactive', !marker._visible);

    let overlay = overlay_array.find(overlay => overlay.shapeId == id);
    if (!overlay) return;
    if (marker._visible) {
        overlay.show();
    } else {
        overlay.hide();
    }
}

function onToggleVisibleShapes(id, visible) {
    const shape = shape_array.find(shape => shape._id == id);
    if (!shape) return;
    const el = jq2('#'+id);
    const visIcon = el.find('.btn-visibility');
    const isVisible = shape._visible;
    shape._visible = visible ? visible : !isVisible;
    shape.tool.setMap(shape._visible ? map : null);
    visIcon.toggleClass('inactive', !shape._visible);

    let overlay = overlay_array.find(overlay => overlay.shapeId == id);
    if (!overlay) return;
    if (shape._visible) {
        overlay.show();
    } else {
        overlay.hide();
    }
}

function onToggleVisibleLabels(id, visible) {
    const label = label_array.find(label => label._id == id);
    if (!label) return;
    const el = jq2('#'+id);
    const visIcon = el.find('.btn-visibility');
    const isVisible = label._visible;
    label._visible = visible ? visible : !isVisible;
    label.tool.setMap(label._visible ? map : null);
    visIcon.toggleClass('inactive', !label._visible);

    let overlay = overlay_array.find(overlay => overlay.shapeId == id);
    if (!overlay) return;
    if (label._visible) {
        overlay.show();
    } else {
        overlay.hide();
    }
}

function onToggleVisibleMeasurements(id, visible) {
    const measurement = measure_array.find(measure => measure._id == id);
    if (!measurement) return;
    const el = jq2('#'+id);
    const visIcon = el.find('.btn-visibility');
    const isVisible = measurement._visible;
    measurement._visible = visible ? visible : !isVisible;
    label.tool.setMap(label._visible ? map : null);
    visIcon.toggleClass('inactive', !label._visible);
}

function toggleDraggable(draggable) {
    shape_array.concat(marker_array).concat(label_array).concat(measure_array).forEach(function(el) {
        el.tool.setDraggable(draggable);
    });
};

function saveJSON(data, filename){
    if(!data) {
        return;
    }
    if(!filename) filename = 'console.json';

    if (typeof data === "object") {
	MapUpdateIntegrator(data);
        data = JSON.stringify(data, undefined, 4)
    }

    var blob = new Blob([data], {type: 'text/json'}),
      e = document.createEvent('MouseEvents'),
      a = document.createElement('a');

    a.download = filename;
    a.href = window.URL.createObjectURL(blob);
    a.dataset.downloadurl =  ['text/json', a.download, a.href].join(':');
    e.initMouseEvent('click', true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
    a.dispatchEvent(e);
};

function populatePalette() {
    let selectedColor = palette && palette[0];

    if (selectedColor) {
        jq2('.dropdown-toggle').css('backgroundColor', selectedColor);
        jq2('#modal-color').val(selectedColor);
    }

    const $palette = jq2('.palette');
    palette.forEach(color => {
        const $color = jq2('<li class="color" data-color="'+color+'" style="background-color: '+color+'"></li>');
        $color.on('click', () => {
            selectedColor = $color.attr('data-color');
            jq2('.dropdown-toggle').css('backgroundColor', selectedColor);
            jq2('#modal-color').val(selectedColor);
        });
        $palette.append($color)
    })
}

function getShapeName(mode) {
    let name = '';

    switch (mode) {
        case MARKER_MODE:
            name = 'Marker ' + (marker_array.length + 1);
            break;
        case POLYGON_MODE:
            const polygons = shape_array.filter(shape => shape.type === 'polygon');
            name = 'Polygon ' + (polygons.length + 1);
            break;
        case LINE_MODE:
            const lines = shape_array.filter(shape => shape.type === 'polyline');
            name = 'Line ' + (lines.length + 1);
            break;
        case RECTANGLE_MODE:
            const rectangles = shape_array.filter(shape => shape.type === 'rectangle');
            name = 'Rectangle ' + (rectangles.length + 1);
            break;
        case CIRCLE_MODE:
            const circles = shape_array.filter(shape => shape.type === 'circle');
            name = 'Circle ' + (circles.length + 1);
            break;
        case LABEL_MODE:
            name = 'Label ' + (label_array.length + 1);
            break;
        case MEASURE_MODE:
            name = 'Measurement ' + (marker_array.length + 1);
            break;
    }

    return name;
}

function setShapeName(mode) {
    const defaultShapeName = getShapeName(mode);
    jq2('#modal-name').val(defaultShapeName);
}

function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

jq2(function ($) {

    $('body').on('click', function(e) {
        if (!$menuEl) return;
        const $target = $(e.target);
        if (!$menuEl.closest($target).length && $menuEl.is(':visible')) {
            $menuEl.remove();
        }
    });

    populatePalette();

    initMap();

    $showPoints = $('.show-points');
    $showLengths = $('.show-lengths');
    $toggleButtons = $('.visibility-toggle');

    $('#panel-toggle').click(function () {
        $('#data-panel').slideToggle();
        let value = $(this).html();

        if (value == '[ hide ]') {
            $(this).html('[ show ]');
        } else {
            $(this).html('[ hide ]');
        }
    });

    /**
     * Change event when you change tool
     */
    $('.btn-mode').click(function () {
        mode = jq2(this).parent().index();
        type = SET_TYPE;

        jq2('#menu').find('.btn-mode').removeClass('mode-selected');
        jq2('#menu').find('.btn-mode').eq(mode).addClass('mode-selected');

        if (mode == HAND_MODE) {
            map.setOptions({disableDoubleClickZoom: false, draggableCursor: null});
            drawingManager.setOptions({drawingMode: google.maps.drawing.OverlayType.HAND});

            toggleDraggable(false);

            return;
        }

        if (mode == MOVE_MODE) {
            map.setOptions({disableDoubleClickZoom: false, draggableCursor: null});
            drawingManager.setOptions({drawingMode: google.maps.drawing.OverlayType.HAND});

            toggleDraggable(true);

            return;
        }

        if (mode == MEASURE_MODE) {
            jq2('#measure-mode').show();
        } else {
            jq2('#measure-mode').hide();
        }

        jq2("#modal-color").val(palette[0]);
        jq2(".dropdown-toggle").css('background-color', palette[0]);
        jq2("#name-input").text(MODE_LABEL[mode] + " name");
        jq2("#color-input").text(MODE_LABEL[mode] + " color");
        jq2('#modal-type').val(SET_TYPE);
        jq2('#modal-mode').val(mode);  //change mode
        jq2('#modal-id').val(0);

        setShapeName(mode);
    });

    $('.save-map').click(function() {
        const mapParams = getMapParams();
        saveJSON(mapParams, 'map.json');
    });

    $('.open-map').click(function() {
        $('.file').click();
    });

    $('.file').change(function(e) {
        const files = e.target.files;

        if (files.length <= 0) {
            return false;
        }

        const fr = new FileReader();

        fr.onload = function(e) {p
            const result = JSON.parse(e.target.result);
            initMap(result);
        };

        fr.readAsText(files.item(0));
    });

    $showPoints.on('change', function() {
        $('.measure-points-wrapper').toggle(isShowPoints());
    });

    $showLengths.on('change', function() {
        $('.measure-lengths-wrapper').toggle(isShowLengths());
    });

    $('.modal-form').on('submit', function(e) {
        setDrawingOptions();
        $('#set-options-modal').modal('hide');
        e.preventDefault();
    });

    $toggleButtons.on('click', function() {
        let $elements;
        switch ($(this).attr('type')) {
            case 'markers':
                $elements = $('.marker-list .btn-visibility');
                showMarkers = !showMarkers;
                $(this).toggleClass('inactive', !showMarkers);

                $elements.each(function() {
                    const id = $(this).closest('li').attr('id');
                    onToggleVisibleMarkers(id, showMarkers);
                });

                break;
            case 'shapes':
                $elements = $('.shape-list .btn-visibility');
                showShapes = !showShapes;
                $(this).toggleClass('inactive', !showShapes);

                $elements.each(function() {
                    const id = $(this).closest('li').attr('id');
                    onToggleVisibleShapes(id, showShapes);
                });

                break;
            case 'labels':
                $elements = $('.label-list .btn-visibility');
                showLabels = !showLabels;
                $(this).toggleClass('inactive', !showLabels);

                $elements.each(function() {
                    const id = $(this).closest('li').attr('id');
                    onToggleVisibleLabels(id, showLabels);
                });

                break;
            case 'measurements':
                $elements = $('.measure-list .btn-visibility');
                showMeasurements = !showMeasurements;
                $(this).toggleClass('inactive', !showMeasurements);

                $elements.each(function() {
                    const id = $(this).closest('li').attr('id');
                    onToggleVisibleMeasurements(id, showMeasurements);
                });

                break;
        }

    });

    $('.snap').on('click', function() {
        const transform=$(".gm-style>div:first>div").css("transform");
        const comp=transform.split(",");
        const mapleft=parseFloat(comp[4]);
        const maptop=parseFloat(comp[5]);
        $(".gm-style>div:first>div").css({
            "transform":"none",
            "left":mapleft,
            "top":maptop
        });

        html2canvas($("#map"), {
            useCORS: true,
            onrendered: function(canvas) {
                const filename = '1px-' + calculateInchPerPx() + 'in--' + Date.now() + '.png';
                canvas.toBlob(function(blob) {
                    saveAs(blob, filename);
                });
            }
        });
    });

    // focus on name field when modal is opened
    $('#set-options-modal').on('shown.bs.modal', function () {
        $('#modal-name').focus();

        if (type === SET_TYPE) {
            idToEdit = null;
            overlayName = '';
            color = defaultColor;
        }
    });

    $('#set-options-modal').on('hidden.bs.modal', function () {
        $('#modal-name').val('');
    });
});







