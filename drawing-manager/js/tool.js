class Tool {
  constructor (tool, name, color, type, visible) {
    this.move(tool, name, color, type, visible)
  }
  move (tool, id, name, color, type, visible = true) {
    this._tool = tool
    this._id = id
    this._name = name
    this._color = color
    this._type = type
    this._visible = visible
  }
  set tool    (tool)     { this._tool = tool }
  get tool    ()         { return this._tool }
  set name    (name)     { this._name = name }
  get name    ()         { return this._name }
  set color   (color)    { this._color = color }
  get color   ()         { return this._color }
  set type    (type)     { this._type = type }
  get type    ()         { return this._type }
  set visible (visible)  { this._visible = visible }
  get visible ()         { return this._visible }
  set id      (id)       { this._id = id }
  get id      ()         { return this._id }
}