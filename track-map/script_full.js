const tracks = ['1-13-1-alternativroute-luebeck.gpx', '2-1-1-bergedorf.gpx'];
const colors = ['blue', 'red'];
mapboxgl.accessToken = 'pk.eyJ1IjoiYWJyYWFvYWx2ZXMiLCJhIjoiY2oxbTRzZXBmMDA1ZjJ3bzI3ODZucTM2dCJ9.AICaNFFp-vS2tD5mEHulmA';

$(document).ready(() => {
    const map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/mapbox/streets-v9',
        center: [10.6212949, 53.8809191],
        zoom: 5
    });

    const bounds = new mapboxgl.LngLatBounds();

    map.on('load', () => {
        tracks.forEach((track, i) => {
            $.ajax(`./tracks/${track}`).done((gpx) => {
                if (typeof gpx === 'string') {
                    gpx = (new DOMParser()).parseFromString(gpx, 'text/xml')
                }
                const geojson = toGeoJSON.gpx(gpx);
                const coordinates = geojson.features[0].geometry.coordinates;
                coordinates.forEach(coords => {
                    bounds.extend([coords[0], coords[1]]);
                });
                map.fitBounds(bounds, {
                    padding: 20
                });

                map.addSource('path'+i, {
                    'type': 'geojson',
                    data: geojson
                });
                map.addLayer({
                    'id': 'outline'+i,
                    'type': 'line',
                    'source': 'path'+i,
                    'layout': {},
                    'paint': {
                        'line-color': colors[i],
                        'line-width': 5
                    }
                });
            });

        });

    });
});


