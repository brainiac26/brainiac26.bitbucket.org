(function () {
    var tableid = "1mVRVN3FFcho7ofExA_TX5FjMjo0AtIlYNDpWVJKt", // Mortens Test Table
     // tableid = "1gAAAVeNo8NxLj4eQUJ67fk4sGedmIWhy4kzRUqIs", // Nettalliansens Felleskart med Vanskelige Målere
     // key = 'AIzaSyCMMZV0uCSuciFkg5M5q9aBYc5yguC4PCI', // Shit key
        key = "AIzaSyB7_-xAtza4ZozbzA9IePk7APnFjBPnEcY"; // Mortens Table Fusion API Key (Google Developer Console)

    google.maps.event.addDomListener(window, 'load', initialize);

    function initialize() {
        var map,
            drawingManager,
            overlays = [],
            dataRows = [],

            $table = $('#results').hide(),
            $resultsTableBody = $table.find('tbody'),
            $countItems = $('#countItems'),
            $clearButton = $('#clear'),
            $exportButton = $('#export').prop('disabled', true);

        bindEvents();

        map = new google.maps.Map(document.getElementById('googft-mapCanvas'), {
            center: new google.maps.LatLng(59.28192089536628, 11.299659078369132),
            zoom: 11,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        new google.maps.FusionTablesLayer({
            map: map,
            heatmap: {enabled: false},
            query: {
                select: "col0",
                from: tableid,
                where: ""
            },
            options: {
                styleId: 2,
                templateId: 2
            }
        });

        drawingManager = new google.maps.drawing.DrawingManager({
            drawingMode: google.maps.drawing.OverlayType.CIRCLE,
            drawingControl: true,
            drawingControlOptions: {
                position: google.maps.ControlPosition.TOP_CENTER,
                drawingModes: [
                    google.maps.drawing.OverlayType.CIRCLE,
                    google.maps.drawing.OverlayType.POLYGON,
                    google.maps.drawing.OverlayType.RECTANGLE
                ]
            },
            polygonOptions: {
                fillOpacity: 0.2,
                strokeWeight: 1,
                editable: true,
                draggable: true
            },
            rectangleOptions: {
                fillOpacity: 0.2,
                strokeWeight: 1,
                editable: true,
                draggable: true
            },
            circleOptions: {
                fillOpacity: 0.2,
                strokeWeight: 1,
                clickable: false,
                editable: true,
                zIndex: 1
            }
        });
        drawingManager.setMap(map);

        google.maps.event.addListener(drawingManager, 'overlaycomplete', function (e) {
            overlays.push(e);
        });

        // Circle
        google.maps.event.addListener(drawingManager, 'circlecomplete', function (circle) {
            var circleObj = circle,
                index = dataRows.length;
            requestData(getCircleQueryString(circle), index);

            google.maps.event.addListener(circleObj, 'center_changed', function () {
                requestData(getCircleQueryString(circle), index);
            });
            google.maps.event.addListener(circleObj, 'radius_changed', function () {
                requestData(getCircleQueryString(circle), index);
            });
        });

        // Polygon
        google.maps.event.addListener(drawingManager, 'polygoncomplete', function (polygon) {
            var polygonObj = polygon,
                polygonPath = polygon.getPath(),
                index = dataRows.length;
            requestData(getPolygonQueryString(polygonObj), index);

            google.maps.event.addListener(polygonObj, 'dragend', function () {
                requestData(getPolygonQueryString(polygonObj), index);
            });

            google.maps.event.addListener(polygonPath, 'set_at', function() {
                requestData(getPolygonQueryString(polygonObj), index);
            });

            google.maps.event.addListener(polygonPath, 'insert_at', function() {
                requestData(getPolygonQueryString(polygonObj), index);
            });
        });

        // Rectangle
        google.maps.event.addListener(drawingManager, 'rectanglecomplete', function (rectangle) {
            var rectangleObj = rectangle,
                index = dataRows.length;
            requestData(getRectangleQueryString(rectangleObj), index);

            google.maps.event.addListener(rectangleObj, 'bounds_changed', function () {
                requestData(getRectangleQueryString(rectangleObj), index);
            });
        });

        function bindEvents() {
            $clearButton.on('click', function () {
                clearAll();
            });
            $exportButton.on('click', function() {
                exportDataToCSV();
            });
        }

        function getCircleQueryString(circle) {
            return 'SELECT col0 FROM ' + tableid +
                ' WHERE ST_INTERSECTS(col0, CIRCLE(LATLNG' + circle.getCenter().toLocaleString() + ', ' + circle.getRadius() + '))' +
                '&key=' + key;
        }

        function getPolygonQueryString(polygon) {
            var coords = '';
            for (var i = 0; i < polygon.getPath().getLength(); i++) {
                coords += "LATLNG" + polygon.getPath().getAt(i).toLocaleString() + ",";
            }
            coords = coords.substring(0, coords.length - 1);

            return 'SELECT col0 FROM ' + tableid +
                ' WHERE ST_INTERSECTS(col0, POLYGON(' + coords + '))&key=' + key;
        }


        function getRectangleQueryString(rectangle) {
            var rectangleCoords = rectangle.getBounds();

            return 'SELECT col0 FROM ' + tableid +
                ' WHERE ST_INTERSECTS(col0, RECTANGLE(' +
                ' LATLNG' + rectangleCoords.getSouthWest().toLocaleString() + ', LATLNG' + rectangleCoords.getNorthEast().toLocaleString() + '))&key=' + key;
        }

        function clearAll() {
            deleteAllShape();
            $table.hide();
            $exportButton.prop('disabled', true);
            $resultsTableBody.html('');
            $countItems.html(0);
            dataRows = [];
        }

        function deleteAllShape() {
            for (var i = 0; i < overlays.length; i++) {
                overlays[i].overlay.setMap(null);
            }
            overlays = [];
        }

        var download = function(content, fileName, mimeType) {
            var a = document.createElement('a');
            mimeType = mimeType || 'application/octet-stream';

            if (navigator.msSaveBlob) { // IE10
                return navigator.msSaveBlob(new Blob([content], { type: mimeType }),     fileName);
            } else if ('download' in a) { //html5 A[download]
                a.href = 'data:' + mimeType + ',' + encodeURIComponent(content);
                a.setAttribute('download', fileName);
                document.body.appendChild(a);
                setTimeout(function() {
                    a.click();
                    document.body.removeChild(a);
                }, 66);
                return true;
            } else { //do iframe dataURL download (old ch+FF):
                var f = document.createElement('iframe');
                document.body.appendChild(f);
                f.src = 'data:' + mimeType + ',' + encodeURIComponent(content);

                setTimeout(function() {
                    document.body.removeChild(f);
                }, 333);
                return true;
            }
        };

        function exportDataToCSV() {
            var filename = 'export';
            dataRows.forEach(function (row, i) {
                var csvContent = "";
                row.forEach(function(data, index) {
                   csvContent +=  index < row.length ? data + "\n" : data;
                });
                download(csvContent, filename+"_"+(i+1)+".csv", "text/csv");
            });
        }

        function requestData(query, index) {
            $.ajax({
                type: "POST",
                url: 'https://www.googleapis.com/fusiontables/v1/query?sql=' + query,
                data: '',
                success: function (data) {
                    renderResults(data, index);
                }
            });
        }

        function renderResults(data, index) {
            if (data.rows) {
                dataRows[index] = data.rows;
            } else if (dataRows[index].length) {
                dataRows.splice(index, 1);
            }
            renderContent();
        }

        function renderContent() {
            if (dataRows.length) {
                var count = 0;
                $table.show();
                $resultsTableBody.html('');
                $exportButton.prop('disabled', false);
                dataRows.forEach(function(rows) {
                    count += rows.length;
                    var htmlContent = '';
                    rows.forEach(function(row) {
                        htmlContent += '<tr><td>' + row[0] + '</td></tr>';
                    });
                    $resultsTableBody.prepend(htmlContent);
                });
                $countItems.html(count);
            } else {
                $table.hide();
                $exportButton.prop('disabled', true);
                $countItems.html(0);
            }
        }
    }
})();