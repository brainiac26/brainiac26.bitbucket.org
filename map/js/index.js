const autoCompleteConfig = {
    renderOption(product) {
        const imgSrc = product.img === 'N/A' ? '' : product.img;
        return `
      <img src="${imgSrc}" />
      ${product.designName} (${product.qualityNumber}) <br />${product.designName} (${product.qualityNumber}) 
    `;
    },
    inputValue(product) {
        // substitute "title" for "designName"
        return product.designName;
    },
    async fetchData(searchTerm) {
        const response = await axios.get('https://greendeal.network/Files/Templates/Greendealmapv2/testdata.json', {
            params: {
                //substitute "id" with the title of "Design" so one can search for e.g. Mimi
                designName: searchTerm
            }
        });

        if (response.data.Error) {
            return [];
        }

        return response.data;
    }
};

createAutoComplete({
    ...autoCompleteConfig,
    root: document.querySelector('#autocomplete'),
    onOptionSelect(product) {

        // This hides the "tutorial" below the search bar.
        //  document.querySelector('.tutorial').classList.add('is-hidden');


        // ADDED FOR SEARCH FUNCTION

        infoPane.classList.remove("halfOpen");
        profile.classList.add("doNotDisplayClass");
        journey.classList.add("doNotDisplayClass");
        searchResultsPane.classList.remove("doNotDisplayClass");
        infoPane.classList.remove("close");
        infoPane.classList.add("open");
        if (document.getElementById("infoPane").classList.contains = "open") {
            document.getElementById("toggleInfoPaneOrMap").innerHTML = "Show Map";
        } else {
            document.getElementById("toggleInfoPaneOrMap").innerHTML = "Show Journey";
        }

        // ADDED FOR SEARCH FUNCTION


        onSelect(product, document.querySelector('#summary'));
    }
});


const onSelect = async(product, summaryElement) => {
    const response = await axios.get('./data/testdata.json', {});

    const materialComposition = product.materialComposition;
    const carbonFootprint = product.carbonFootprint;
    const technology = product.technology;
    const designName = product.designName;
    const qualityNumber = product.qualityNumber;
    const imgSrc = product.img === 'N/A' ? '' : product.img;
    const manufacturingIconSrc = product.manufacturingIcon == 'N/A' ? '' : product.manufacturingIcon;
    const ginnerIconSrc = product.ginnerIcon == 'N/A' ? '' : product.ginnerIcon;
    const innovatorIconSrc = product.innovatorIcon == 'N/A' ? '' : product.innovatorIcon;
    const laundryIconSrc = product.laundryIcon == 'N/A' ? '' : product.laundryIcon;
    const circularIconSrc = product.circularIcon == 'N/A' ? '' : product.circularIcon;
    const manufacturingName = product.manufacturingName;
    const innovatorName = product.innovatorName;
    const circularName = product.circuarName;
    const ginnerName = product.ginnerName;
    const laundryName = product.laundryName;

    // Certification

    const certificationFairtrade = product.fairtrade === undefined ? '' : product.fairtrade;
    const certificationBeirtex = product.beirtex === undefined ? '' : product.beirtex;
    const certificationOrganicCotton = product.organicCotton === undefined ? '' : product.organicCotton;
    const certificationRecycledPolyester = product.recycledPolyester === undefined ? '' : product.recycledPolyester;
    const certificationGots = product.gots === undefined ? '' : product.gots;
    const certificationEuEcolabel = product.euEcolabel === undefined ? '' : product.euEcolabel;
    const certificationOekoTexMadeInGreen = product.madeInGreen === undefined ? '' : product.madeInGreen;
    const certificationBetterCottonInitiative = product.betterCottonInitiative === undefined ? '' : product.betterCottonInitiative;
    const certificationGreenButton = product.greenButton === undefined ? '' : product.greenButton;

    const template = detail => {

        return `
                <div class="card-body">
                    <img src="${imgSrc}" class="card-img-top" width="100%" height="100%" preserveAspectRatio="xMidYMid slice" focusable="false" alt="...">
                    <h3 class="card-title">${designName}</h3>
                </div>

                <div class="card-body">
                <div class="row row-cols-1 row-cols-md-2 g-2">
                <div class="col">
                    <div class="card shadow-sm rounded">
                        <div class="card-body">
                            <p class="card-text text-muted"><strong>Raw materials</strong></p>
                            <p class="card-text" style="font-size: 1.4rem; font-weight: bold;">${materialComposition}</p>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card shadow-sm rounded">
                        <div class="card-body">
                            <p class="card-text text-muted"><strong>CO2 emissions</strong></p>
                            <p class="card-text" style="font-size: 1.4rem; font-weight: bold;">${carbonFootprint}</p>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card shadow-sm rounded">
                        <div class="card-body">
                            <p class="card-text text-muted"><strong>Quality</strong></p>
                            <p class="card-text" style="font-size: 1.4rem; font-weight: bold;">${qualityNumber} ${technology}</p>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card shadow-sm rounded">
                        <div class="card-body">
                            <p class="card-text text-muted"><strong>Design</strong></p>
                            <p class="card-text" style="font-size: 1.4rem; font-weight: bold;">${designName}</p>
                        </div>
                    </div>
                </div>
            </div>
</div>



                <div class="card-body">
                <h3 class="card-title" style="font-size: 2.5rem">Journey</h3>
        <div class="card shadow-sm rounded mt-2">
        <div class="d-flex card-text">
        <img src="${innovatorIconSrc}" class="flex-shrink-0 me-3 rounded" width="32" height="32" preserveAspectRatio="xMidYMid slice" focusable="false" alt="...">
          
          <p class="pb-3 mb-0 border-bottom" style="font-size: 1.0rem;">
            <strong class="d-block text-gray-dark" style="font-size: 1.4rem; font-weight: bold;">Textile Innovator</strong>
            <span class="text-muted">${designName} has been designed and developed by ${innovatorName}. To learn more about how ${innovatorName} creates sustainable performance textiles, <a class="card-link link-dark" onclick="filterParticipants(this.value);" value="${innovatorName}">click here</a>.
          </span></p>
        </div>

        <div class="d-flex card-text pt-3">
        <img src="${ginnerIconSrc}" class="flex-shrink-0 me-3 rounded" width="32" height="32" preserveAspectRatio="xMidYMid slice" focusable="false" alt="...">
  
        <p class="pb-3 mb-0 border-bottom" style="font-size: 1.0rem;">
          <strong class="d-block text-gray-dark" style="font-size: 1.4rem; font-weight: bold;">Ginning</strong>
          <span class="text-muted">By purchasing ${designName}, you effectively help to promote more sustainable cotton practices in Pakistan and world wide through ${ginnerName}. See <a class="card-link link-dark" onclick="filterParticipants(this.value);" value="${ginnerName}">ginners here</a>.
        </span></p>
      </div>

        <div class="d-flex card-text pt-3">
        <img src="${manufacturingIconSrc}" class="flex-shrink-0 me-3 rounded" width="32" height="32" preserveAspectRatio="xMidYMid slice" focusable="false" alt="...">
  
        <p class="pb-3 mb-0 border-bottom" style="font-size: 1.0rem;">
          <strong class="d-block text-gray-dark" style="font-size: 1.4rem; font-weight: bold;">Manufacturing</strong>
          <span class="text-muted">${designName} has been manufactured by ${manufacturingName}. To learn more about how ${manufacturingName} manufactures in a socially and environmentally responsible way, <a class="card-link link-dark" onclick="filterParticipants(this.value);" value="${manufacturingName}">click here</a>.
        </span></p>
      </div>

      <div class="d-flex card-text pt-3">
      <img src="${laundryIconSrc}" class="flex-shrink-0 me-3 rounded" width="32" height="32" preserveAspectRatio="xMidYMid slice" focusable="false" alt="...">

      <p class="pb-3 mb-0 border-bottom" style="font-size: 1.0rem;">
        <strong class="d-block text-gray-dark" style="font-size: 1.4rem; font-weight: bold;">Textile Services</strong>
        <span class="text-muted">${designName} has been freshly and hygienically cleaned by ${laundryName} in an environmentally friendly manner. To learn more about how ${laundryName} manages and cleans textiles, <a class="card-link link-dark" onclick="filterParticipants(this.value);" value="${laundryName}">click here</a>.
      </span></p>
    </div>
        
        <small class="d-block text-end mt-3">
          <a class="card-link link-dark" onclick="filterParticipants(this.value);" value="">See entire journey</a>
        </small>
      </div>

      </div>

      <div class="card-body">
      <h3 class="card-title" style="font-size: 2.5rem">Responsibility</h3>
                <p class="card-text text-muted mt-2" style="font-size: 1.4rem; font-weight: bold;">${designName} is certified according to the following standards</p>
                <div class="row row-cols-1 row-cols-2 g-2">
                ${certificationFairtrade}
                ${certificationBeirtex}
                ${certificationOrganicCotton}
                ${certificationRecycledPolyester}
                ${certificationGots}
                ${certificationEuEcolabel}
                ${certificationOekoTexMadeInGreen}
                ${certificationBetterCottonInitiative}
                ${certificationGreenButton}

                </div>
</div>

      `;
    };


    summaryElement.innerHTML = template(response.data);



};