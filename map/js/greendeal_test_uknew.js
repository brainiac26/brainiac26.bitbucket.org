let selectedSupplyType;
let selectedParticipant;

let map;
let userMarker;
let markers = [];

let participants = [];

let iconBase = 'https://greendeal.network/Files/Images/Icons/';

let infoPanelHeight;

// Define marker icons

let icons = {
    ginner: {
        icon: iconBase + 'farming-ginning.png' //  'red-circle.png' // 'farming-ginning.png',
    },
    harbor: {
        icon: iconBase + 'harbour.png' //'red-circle.png'
    },
    vessel: {
        icon: iconBase + 'container-vessel.png' //'red-circle.png'
    },
    truck: {
        icon: iconBase + 'truck-left-pointing.png' //'red-circle.png'
    },
    manufacturer: {
        icon: iconBase + 'manufacturing.png' // 'red-circle.png'
    },
    innovator: {
        icon: iconBase + 'sustainable-textile-innovation.png' //  'red-circle.png' //
    },
    laundry: {
        icon: iconBase + 'laundry-services.png'
    },
    hotel: {
        icon: iconBase + 'hotel.png'
    },
    circular: {
        icon: iconBase + 'circular-restart.png'
    },
};

// GreenDeal Network journey icons

let participantsJourney;

// Sea route and truck coordinates from Pakistan to Hamburg

let seaRoutePlanCoordinates = [
    { lat: 24.86, lng: 67.00 },
    { lat: 12.72, lng: 53.88 },
    { lat: 11.39, lng: 44.95 },
    { lat: 15.11, lng: 41.40 },
    { lat: 27.74, lng: 33.69 },
    { lat: 32.04, lng: 31.96 },
    { lat: 36.35, lng: 12.09 },
    { lat: 38.03, lng: 8.93 },
    { lat: 35.81, lng: -3.7 },
    { lat: 36.38, lng: -12.35 },
    { lat: 47.98, lng: -7.85 },
    { lat: 49.97, lng: -1.88 },
    { lat: 51.09, lng: 1.76 },
    { lat: 54.11, lng: 5.41 },
    { lat: 53.87, lng: 8.84 },
    { lat: 53.53, lng: 9.75 },
];

let truckRoutePlanCoordinatesSapphire = [
    { lat: 31.54, lng: 74.34 },
    { lat: 28.53, lng: 69.93 },
    { lat: 24.86, lng: 67.00 },
];

// Container vessel and truck symbols

let vesselSymbol ;

let truckSymbol;

// Sea and trucking route dotted lines

let lineSymbol = {
    path: 'M 0,-1 0,1',
    strokeOpacity: 1,
    scale: 3,
};

let seaRoute;

let truckRouteSapphire;

// Animated container ship on sea route

let line;

// Initiate google map

function initMap() {
    participantsJourney = [{
        position: new google.maps.LatLng(24.84, 67.00),
        type: 'harbor',
    }, {
        position: new google.maps.LatLng(53.57, 10.01),
        type: 'harbor',
    }, {
        position: new google.maps.LatLng(36.35, 12.09),
        type: 'vessel',
    }, {
        position: new google.maps.LatLng(28.53, 69.93),
        type: 'truck',
    }, ];

    vesselSymbol = {
        path: 'M434.89,108.46c-1.42-2.51-4.08-4.06-6.96-4.06H393.3V88.2c0-4.42-3.58-8-8-8h-24.33V48.28c0-4.42-3.58-8-8-8H321.3V8c0-4.42-3.58-8-8-8h-79.67c-4.42,0-8,3.58-8,8v32.28h-31.67c-4.42,0-8,3.58-8,8V80.2h-24.33c-4.42,0-8,3.58-8,8v31.58H121.3v-87.5c0-4.42-3.58-8-8-8h-96c-4.42,0-8,3.58-8,8v87.5H8c-2.88,0-5.53,1.54-6.95,4.04c-1.42,2.5-1.4,5.57,0.07,8.04l48.63,81.98c6.02,10.03,19.39,17.6,31.09,17.6H345.4c11.7,0,25.07-7.57,31.1-17.61l58.29-97.32C436.27,114.05,436.31,110.97,434.89,108.46zM377.3,104.41h-56V96.2h56V104.41z M305.3,96.2v8.31c-13.41,1.06-22.96,10.08-27.4,15.27h-36.26V96.2L305.3,96.2L305.3,96.2zM344.97,80.2h-64V56.28h64V80.2z M241.64,16h63.67v24.28h-63.67L241.64,16L241.64,16z M201.97,56.28h63V80.2h-63L201.97,56.28L201.97,56.28z M169.64,96.2h56v23.58h-56V96.2z M25.3,40.28h80V55.7h-16c-4.42,0-8,3.58-8,8s3.58,8,8,8h16v16.25h-16c-4.42,0-8,3.58-8,8s3.58,8,8,8h16v15.83h-80C25.3,119.78,25.3,40.28,25.3,40.28z M362.78,205.61c-3.09,5.15-11.37,9.83-17.38,9.83H80.84c-6,0-14.29-4.69-17.35-9.8l-41.44-69.87h91.25h168.5c2.66,0,5.18-1.36,6.66-3.57c0.08-0.12,8.07-11.8,19.45-11.8h76.06c0.43,0.07,0.88,0.12,1.33,0.12s0.9-0.05,1.33-0.12h27.17L362.78,205.61z M65.3,55.7c-2.11,0-4.17,0.85-5.66,2.34c-1.49,1.49-2.34,3.55-2.34,5.66c0,2.1,0.85,4.16,2.34,5.65c1.49,1.49,3.55,2.35,5.66,2.35c2.11,0,4.17-0.86,5.66-2.35s2.34-3.55,2.34-5.65c0-2.11-0.85-4.17-2.34-5.66S67.41,55.7,65.3,55.7z M41.63,55.7c-2.1,0-4.17,0.85-5.65,2.34c-1.49,1.49-2.35,3.55-2.35,5.66c0,2.1,0.86,4.16,2.35,5.65c1.49,1.49,3.55,2.35,5.65,2.35c2.11,0,4.17-0.86,5.66-2.35c1.49-1.49,2.34-3.55,2.34-5.65c0-2.11-0.85-4.17-2.34-5.66S43.74,55.7,41.63,55.7z M65.3,87.95c-2.11,0-4.17,0.85-5.66,2.34c-1.49,1.49-2.34,3.55-2.34,5.66c0,2.1,0.85,4.16,2.34,5.65c1.49,1.49,3.55,2.35,5.66,2.35c2.11,0,4.17-0.86,5.66-2.35s2.34-3.55,2.34-5.65c0-2.11-0.85-4.17-2.34-5.66S67.41,87.95,65.3,87.95z M41.63,87.95c-2.1,0-4.16,0.85-5.65,2.34c-1.49,1.49-2.35,3.55-2.35,5.66c0,2.1,0.86,4.16,2.35,5.65c1.49,1.49,3.55,2.35,5.65,2.35c2.11,0,4.17-0.86,5.66-2.35c1.49-1.49,2.34-3.55,2.34-5.65c0-2.11-0.85-4.17-2.34-5.66S43.74,87.95,41.63,87.95z M121.48,152.03c-2.1,0-4.16,0.85-5.65,2.34s-2.35,3.55-2.35,5.66c0,2.1,0.86,4.17,2.35,5.66c1.48,1.49,3.55,2.34,5.65,2.34c2.11,0,4.17-0.85,5.66-2.34s2.34-3.55,2.34-5.66c0-2.11-0.85-4.17-2.34-5.66C125.65,152.88,123.59,152.03,121.48,152.03z M153.55,152.03c-2.11,0-4.17,0.85-5.66,2.34s-2.34,3.55-2.34,5.66c0,2.11,0.85,4.17,2.34,5.66c1.49,1.49,3.56,2.34,5.66,2.34c2.11,0,4.17-0.85,5.66-2.34s2.34-3.55,2.34-5.66c0-2.11-0.85-4.17-2.34-5.66C157.72,152.88,155.66,152.03,153.55,152.03z M185.62,152.03c-2.11,0-4.17,0.85-5.66,2.34s-2.34,3.56-2.34,5.66s0.85,4.17,2.34,5.66c1.49,1.49,3.55,2.34,5.66,2.34c2.1,0,4.16-0.85,5.65-2.34c1.49-1.49,2.35-3.56,2.35-5.66s-0.86-4.17-2.35-5.66C189.78,152.88,187.72,152.03,185.62,152.03z M217.68,152.03c-2.1,0-4.17,0.85-5.65,2.34c-1.49,1.49-2.35,3.55-2.35,5.66c0,2.11,0.86,4.17,2.35,5.66c1.48,1.49,3.55,2.34,5.65,2.34c2.11,0,4.17-0.85,5.66-2.34s2.34-3.55,2.34-5.66c0-2.11-0.85-4.17-2.34-5.66C221.85,152.88,219.79,152.03,217.68,152.03z M249.75,152.03c-2.1,0-4.17,0.85-5.66,2.34s-2.34,3.55-2.34,5.66c0,2.11,0.85,4.17,2.34,5.66c1.49,1.49,3.56,2.34,5.66,2.34c2.11,0,4.17-0.85,5.66-2.34c1.49-1.49,2.34-3.55,2.34-5.66c0-2.11-0.85-4.17-2.34-5.66C253.92,152.88,251.86,152.03,249.75,152.03zM281.82,152.03c-2.11,0-4.17,0.85-5.66,2.34s-2.34,3.55-2.34,5.66c0,2.11,0.85,4.17,2.34,5.66s3.55,2.34,5.66,2.34c2.1,0,4.16-0.85,5.65-2.34s2.35-3.55,2.35-5.66c0-2.11-0.86-4.17-2.35-5.66C285.98,152.88,283.92,152.03,281.82,152.03z',
        scale: 0.07,
        strokeOpacity: 1,
        color: 'black',
        strokeWeight: 0.2,
        rotation: 90,
        anchor: new google.maps.Point(250, 250),
    };

    truckSymbol = {
        path: 'M434.89,108.46c-1.42-2.51-4.08-4.06-6.96-4.06H393.3V88.2c0-4.42-3.58-8-8-8h-24.33V48.28c0-4.42-3.58-8-8-8H321.3V8c0-4.42-3.58-8-8-8h-79.67c-4.42,0-8,3.58-8,8v32.28h-31.67c-4.42,0-8,3.58-8,8V80.2h-24.33c-4.42,0-8,3.58-8,8v31.58H121.3v-87.5c0-4.42-3.58-8-8-8h-96c-4.42,0-8,3.58-8,8v87.5H8c-2.88,0-5.53,1.54-6.95,4.04c-1.42,2.5-1.4,5.57,0.07,8.04l48.63,81.98c6.02,10.03,19.39,17.6,31.09,17.6H345.4c11.7,0,25.07-7.57,31.1-17.61l58.29-97.32C436.27,114.05,436.31,110.97,434.89,108.46zM377.3,104.41h-56V96.2h56V104.41z M305.3,96.2v8.31c-13.41,1.06-22.96,10.08-27.4,15.27h-36.26V96.2L305.3,96.2L305.3,96.2zM344.97,80.2h-64V56.28h64V80.2z M241.64,16h63.67v24.28h-63.67L241.64,16L241.64,16z M201.97,56.28h63V80.2h-63L201.97,56.28L201.97,56.28z M169.64,96.2h56v23.58h-56V96.2z M25.3,40.28h80V55.7h-16c-4.42,0-8,3.58-8,8s3.58,8,8,8h16v16.25h-16c-4.42,0-8,3.58-8,8s3.58,8,8,8h16v15.83h-80C25.3,119.78,25.3,40.28,25.3,40.28z M362.78,205.61c-3.09,5.15-11.37,9.83-17.38,9.83H80.84c-6,0-14.29-4.69-17.35-9.8l-41.44-69.87h91.25h168.5c2.66,0,5.18-1.36,6.66-3.57c0.08-0.12,8.07-11.8,19.45-11.8h76.06c0.43,0.07,0.88,0.12,1.33,0.12s0.9-0.05,1.33-0.12h27.17L362.78,205.61z M65.3,55.7c-2.11,0-4.17,0.85-5.66,2.34c-1.49,1.49-2.34,3.55-2.34,5.66c0,2.1,0.85,4.16,2.34,5.65c1.49,1.49,3.55,2.35,5.66,2.35c2.11,0,4.17-0.86,5.66-2.35s2.34-3.55,2.34-5.65c0-2.11-0.85-4.17-2.34-5.66S67.41,55.7,65.3,55.7z M41.63,55.7c-2.1,0-4.17,0.85-5.65,2.34c-1.49,1.49-2.35,3.55-2.35,5.66c0,2.1,0.86,4.16,2.35,5.65c1.49,1.49,3.55,2.35,5.65,2.35c2.11,0,4.17-0.86,5.66-2.35c1.49-1.49,2.34-3.55,2.34-5.65c0-2.11-0.85-4.17-2.34-5.66S43.74,55.7,41.63,55.7z M65.3,87.95c-2.11,0-4.17,0.85-5.66,2.34c-1.49,1.49-2.34,3.55-2.34,5.66c0,2.1,0.85,4.16,2.34,5.65c1.49,1.49,3.55,2.35,5.66,2.35c2.11,0,4.17-0.86,5.66-2.35s2.34-3.55,2.34-5.65c0-2.11-0.85-4.17-2.34-5.66S67.41,87.95,65.3,87.95z M41.63,87.95c-2.1,0-4.16,0.85-5.65,2.34c-1.49,1.49-2.35,3.55-2.35,5.66c0,2.1,0.86,4.16,2.35,5.65c1.49,1.49,3.55,2.35,5.65,2.35c2.11,0,4.17-0.86,5.66-2.35c1.49-1.49,2.34-3.55,2.34-5.65c0-2.11-0.85-4.17-2.34-5.66S43.74,87.95,41.63,87.95z M121.48,152.03c-2.1,0-4.16,0.85-5.65,2.34s-2.35,3.55-2.35,5.66c0,2.1,0.86,4.17,2.35,5.66c1.48,1.49,3.55,2.34,5.65,2.34c2.11,0,4.17-0.85,5.66-2.34s2.34-3.55,2.34-5.66c0-2.11-0.85-4.17-2.34-5.66C125.65,152.88,123.59,152.03,121.48,152.03z M153.55,152.03c-2.11,0-4.17,0.85-5.66,2.34s-2.34,3.55-2.34,5.66c0,2.11,0.85,4.17,2.34,5.66c1.49,1.49,3.56,2.34,5.66,2.34c2.11,0,4.17-0.85,5.66-2.34s2.34-3.55,2.34-5.66c0-2.11-0.85-4.17-2.34-5.66C157.72,152.88,155.66,152.03,153.55,152.03z M185.62,152.03c-2.11,0-4.17,0.85-5.66,2.34s-2.34,3.56-2.34,5.66s0.85,4.17,2.34,5.66c1.49,1.49,3.55,2.34,5.66,2.34c2.1,0,4.16-0.85,5.65-2.34c1.49-1.49,2.35-3.56,2.35-5.66s-0.86-4.17-2.35-5.66C189.78,152.88,187.72,152.03,185.62,152.03z M217.68,152.03c-2.1,0-4.17,0.85-5.65,2.34c-1.49,1.49-2.35,3.55-2.35,5.66c0,2.11,0.86,4.17,2.35,5.66c1.48,1.49,3.55,2.34,5.65,2.34c2.11,0,4.17-0.85,5.66-2.34s2.34-3.55,2.34-5.66c0-2.11-0.85-4.17-2.34-5.66C221.85,152.88,219.79,152.03,217.68,152.03z M249.75,152.03c-2.1,0-4.17,0.85-5.66,2.34s-2.34,3.55-2.34,5.66c0,2.11,0.85,4.17,2.34,5.66c1.49,1.49,3.56,2.34,5.66,2.34c2.11,0,4.17-0.85,5.66-2.34c1.49-1.49,2.34-3.55,2.34-5.66c0-2.11-0.85-4.17-2.34-5.66C253.92,152.88,251.86,152.03,249.75,152.03zM281.82,152.03c-2.11,0-4.17,0.85-5.66,2.34s-2.34,3.55-2.34,5.66c0,2.11,0.85,4.17,2.34,5.66s3.55,2.34,5.66,2.34c2.1,0,4.16-0.85,5.65-2.34s2.35-3.55,2.35-5.66c0-2.11-0.86-4.17-2.35-5.66C285.98,152.88,283.92,152.03,281.82,152.03z',
        scale: 0.07,
        strokeOpacity: 1,
        color: 'black',
        strokeWeight: 0.2,
        rotation: 90,
        anchor: new google.maps.Point(250, 250),
    };

    seaRoute = new google.maps.Polyline({
        path: seaRoutePlanCoordinates,
        geodesic: true,
        strokeColor: '#8B9C88',
        strokeOpacity: 0,
        icons: [{
            icon: lineSymbol,
            offset: '0',
            repeat: '50px'
        }],
    });

    truckRouteSapphire = new google.maps.Polyline({
        path: truckRoutePlanCoordinatesSapphire,
        geodesic: true,
        strokeColor: '#8B9C88',
        strokeOpacity: 0,
        icons: [{
            icon: lineSymbol,
            offset: '0',
            repeat: '50px'
        }],
    });

    line = new google.maps.Polyline({
        path: seaRoutePlanCoordinates,
        strokeOpacity: 0,
        icons: [{
            icon: vesselSymbol,
            offset: '100%',
            repeat: '900px',
            zIndex: 2,
        }],
        visible: false, // hides the animated marker
        map: map
    });

    map = new google.maps.Map(
        document.getElementById('map'), {
            center: new google.maps.LatLng(52.37, 9.73),
            zoom: 3,
            zoomControl: true,
            mapTypeControl: false,
            streetViewControl: false,
            fullscreenControl: true,
            mapId: '4168ee3918e4bebc',
            zoomControlOptions: {
                position: google.maps.ControlPosition.RIGHT_CENTER,
            },
        });

    userMarker = new google.maps.Marker({
        map: map,
        icon: 'https://greendeal.network/Files/Images/Icons/userMarker.png'
    });

    // Get Geolocation from user

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            const pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };

            userMarker.setPosition(pos);
            map.setCenter(pos);

        }, function() {
            handleLocationError(true, userMarker, map.getCenter());
        });
    } else {
        // Browser doesn't support Geolocation
        handleLocationError(false, userMarker, map.getCenter());
    }
}

// Call animate vessel function

function animateVessel(line) {
    let count = 0;
    window.setInterval(function() {
        count = (count + 1) % 2000;

        let icons = line.get('icons');
        icons[0].offset = (count / 20) + '%';
        line.set('icons', icons);
    }, 20);
}

// Product search

const autoCompleteConfig = {
    renderOption(product) {
        const imgSrc = product.img === 'N/A' ? '' : product.img;
        return `
          <img src="${imgSrc}" /> 
          <span>${product.designName}</span>
        `;
    },
    inputValue(product) {
        // substitute "title" for "designName"
        return product.designName;
    },
    async fetchData(searchTerm) {
        if (!searchTerm) return [];
        const response = await axios.get('./data/productdata.json');

        if (response.data.Error) {
            return [];
        }

        return response.data.filter((product) => product.designName.toLowerCase().includes(searchTerm.toLowerCase()));
    }
};

createAutoComplete({
    ...autoCompleteConfig,
    root: document.querySelector('#searchContainer'),
    onOptionSelect(product) {
        selectedProduct = product;
        onSelectProduct(product);
    }
});

function handleLocationError() {
    console.log('cound not retrieve user location');
}

function onSelectProduct(product) {
    const $infoPanel = $('#infoPanel');
    $('#autocomplete').val(product.designName);
    if (!$infoPanel.hasClass('open')) {
        $infoPanel.addClass('open');
        infoPanelHeight = $infoPanel.height();
    }

    showBackButton();

    const innovators = participants.filter(participant => participant.id === product.innovatorID);
    const ginners = participants.filter(participant => participant.id === product.ginnerID);
    const manufacturers = participants.filter(participant => participant.id === product.manufacturingID);
    const services = participants.filter(participant => participant.id === product.laundryID);
    const circular = participants.filter(participant => participant.id === product.circularID);
    const hotels = participants.filter(participant => participant.id === product.hotelID);

    const allParticipants = [...innovators, ...ginners, ...manufacturers, ...services, ...circular, ...hotels];

    setMarkers(allParticipants);

    seaRoute.setMap(map);
    truckRouteSapphire.setMap(map);

    const materialComposition = product.materialComposition;
    const carbonFootprint = product.carbonFootprint;
    const technology = product.technology;
    const designName = product.designName;
    const qualityNumber = product.qualityNumber;
    const imgSrc = product.img === 'N/A' ? '' : product.img;
    const manufacturingIconSrc = product.manufacturingIcon == 'N/A' ? '' : product.manufacturingIcon;
    const ginnerIconSrc = product.ginnerIcon == 'N/A' ? '' : product.ginnerIcon;
    const innovatorIconSrc = product.innovatorIcon == 'N/A' ? '' : product.innovatorIcon;
    const laundryIconSrc = product.laundryIcon == 'N/A' ? '' : product.laundryIcon;
    const circularIconSrc = product.circularIcon == 'N/A' ? '' : product.circularIcon;
    const manufacturingName = manufacturers.length > 1 ? 'manufacturers' : manufacturers[0].name;
    const innovatorName = innovators.length > 1 ? 'textile innovators' : innovators[0].name;
    const circularName = circular.length > 1 ? 'circular' : circular[0].name;
    const ginnerName = ginners.length > 1 ? 'Ginners' : ginners[0].name;
    const laundryName = services.length > 1 ? 'Textile Services' : services[0].name;
    const hotelName = hotels.length > 1 ? 'Hotels' : hotels[0].name;

    // Certification

    const certificationFairtrade = product.fairtrade === undefined ? '' : product.fairtrade;
    const certificationBeirtex = product.beirtex === undefined ? '' : product.beirtex;
    const certificationOrganicCotton = product.organicCotton === undefined ? '' : product.organicCotton;
    const certificationRecycledPolyester = product.recycledPolyester === undefined ? '' : product.recycledPolyester;
    const certificationGots = product.gots === undefined ? '' : product.gots;
    const certificationEuEcolabel = product.euEcolabel === undefined ? '' : product.euEcolabel;
    const certificationOekoTexMadeInGreen = product.madeInGreen === undefined ? '' : product.madeInGreen;
    const certificationBetterCottonInitiative = product.betterCottonInitiative === undefined ? '' : product.betterCottonInitiative;
    const certificationGreenButton = product.greenButton === undefined ? '' : product.greenButton;

    const $content = $('<div class="content"></div>');

    const productContent = `
        <div class="slide"></div>
        <img class="img-fluid mb-3 img-preview" src="${product.img}">
        
        <h3 class="mb-2 p-2">${product.designName}</h3>

        <div class="mobile-hidden">
            <div class="card-body p-2">
                <div class="row row-cols-1 row-cols-md-2 g-2">
                    <div class="col">
                        <div class="card shadow-sm rounded">
                            <div class="card-body">
                                <p class="card-text text-muted"><strong>Raw materials</strong></p>
                                <p class="card-text" style="font-size: 1.4rem; font-weight: bold;">${materialComposition}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card shadow-sm rounded">
                            <div class="card-body">
                                <p class="card-text text-muted"><strong>CO2 emissions</strong></p>
                                <p class="card-text" style="font-size: 1.4rem; font-weight: bold;">${carbonFootprint}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card shadow-sm rounded">
                            <div class="card-body">
                                <p class="card-text text-muted"><strong>Quality</strong></p>
                                <p class="card-text" style="font-size: 1.4rem; font-weight: bold;">${qualityNumber} ${technology}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card shadow-sm rounded">
                            <div class="card-body">
                                <p class="card-text text-muted"><strong>Design</strong></p>
                                <p class="card-text" style="font-size: 1.4rem; font-weight: bold;">${designName}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="p-2">
                <h3 style="font-size: 2.5rem">Journey</h3>
                <div class="mt-4">
                    <div class="d-flex">
                        <img src="${innovatorIconSrc}" class="flex-shrink-0 me-4 img-journey" width="50" height="50" preserveAspectRatio="xMidYMid slice" focusable="false" alt="...">
            
                        <p class="pb-3 mb-0 flex-grow-1" style="font-size: 1.2rem;">
                            <strong class="d-block" style="font-size: 1.4rem; font-weight: bold;">Textile Innovator</strong>
                            <span class="text-muted">${designName} has been designed and developed by ${innovatorName}.</span>
                            <small class="d-block mt-1">
                                <button type="button" class="btn btn-outline-primary" style="width:70%" onclick="focusOnMarker(${product.innovatorID});" value="innovator">Visit ${innovatorName}</button>
                            </small>
            
                        </p>
            
                    </div>
            
                    <div class="d-flex pt-3">
                        <img src="${ginnerIconSrc}" class="flex-shrink-0 me-4 img-journey" width="50" height="50" preserveAspectRatio="xMidYMid slice" focusable="false" alt="...">
            
                        <p class="pb-3 mb-0 flex-grow-1" style="font-size: 1.2rem;">
                            <strong class="d-block" style="font-size: 1.4rem; font-weight: bold;">Ginning</strong>
                            <span class="text-muted">By purchasing ${designName}, you effectively help to promote more sustainable cotton practices in Pakistan and world wide through ${ginnerName}.</span>
                            <small class="d-block mt-1">
                                <button type="button" class="btn btn-outline-primary" style="width:70%" onclick="focusOnMarker(${product.ginnerID});" value="ginner">See ${ginnerName}</button>
                            </small>
                        </p>
                    </div>
            
                    <div class="d-flex pt-3">
                        <img src="${manufacturingIconSrc}" class="flex-shrink-0 me-4 img-journey" width="50" height="50" preserveAspectRatio="xMidYMid slice" focusable="false" alt="...">
            
                        <p class="pb-3 mb-0 flex-grow-1" style="font-size: 1.2rem;">
                            <strong class="d-block" style="font-size: 1.4rem; font-weight: bold;">Manufacturing</strong>
                            <span class="text-muted">${designName} has been manufactured by ${manufacturingName} in a socially and environmentally responsible way.</span>
                            <small class="d-block mt-1">
                                <button type="button" class="btn btn-outline-primary" style="width:70%" onclick="focusOnMarker(${product.manufacturingID});" value="manufacturer">Visit ${manufacturingName}</button>
                            </small>
                        </p>
                    </div>
            
                    <div class="d-flex pt-3">
                        <img src="${laundryIconSrc}" class="flex-shrink-0 me-4 img-journey" width="50" height="50" preserveAspectRatio="xMidYMid slice" focusable="false" alt="...">
            
                        <p class="pb-3 mb-0 flex-grow-1" style="font-size: 1.2rem;">
                            <strong class="d-block" style="font-size: 1.4rem; font-weight: bold;">Textile Services</strong>
                            <span class="text-muted">${designName} has been freshly and hygienically cleaned by ${laundryName} in an environmentally friendly manner.</span>
                            <small class="d-block mt-1">
                                <button type="button" class="btn btn-outline-primary" style="width:70%" onclick="focusOnMarker(${product.laundryID}});" value="laundry">Visit ${laundryName}</button>
                            </small>
                        </p>
                    </div>
                    
                    <div class="d-flex pt-3">
                        <img src="../images/hotel.png" class="flex-shrink-0 me-4 img-journey" width="50" height="50" preserveAspectRatio="xMidYMid slice" focusable="false" alt="...">
            
                        <p class="pb-3 mb-0 flex-grow-1" style="font-size: 1.2rem;">
                            <strong class="d-block" style="font-size: 1.4rem; font-weight: bold;">Hotel</strong>
                            <span class="text-muted">some text</span>
                            <small class="d-block mt-1">
                                <button type="button" class="btn btn-outline-primary" style="width:70%" onclick="focusOnMarker(${product.hotelID});" value="hotel">Visit ${hotelName}</button>
                            </small>
                        </p>
                    </div>
                </div>
            </div>
            
            <div class="card-body">
                    <h3 class="card-title" style="font-size: 2.5rem">Responsibility</h3>
                    <p class="card-text text-muted mt-2" style="font-size: 1.4rem; font-weight: bold;">${designName} is certified according to the following standards</p>
                    <div class="row row-cols-1 row-cols-2 g-2">
                        ${certificationFairtrade}
                        ${certificationBeirtex}
                        ${certificationOrganicCotton}
                        ${certificationRecycledPolyester}
                        ${certificationGots}
                        ${certificationEuEcolabel}
                        ${certificationOekoTexMadeInGreen}
                        ${certificationBetterCottonInitiative}
                        ${certificationGreenButton}
            
                    </div>
            </div>
        </div>
    `;

    $content.append(productContent);

    $infoPanel.html($content);
}

function onSelectSupplyType(type) {
    const participantsToShow = participants.filter(participant => participant.type === type);
    setMarkers(participantsToShow);
}

function onSelectParticipant(participant) {
    $('#autocomplete').val('');
    const $infoPanel = $('#infoPanel');
    if (!$infoPanel.hasClass('open')) {
        $infoPanel.addClass('open');
        infoPanelHeight = $infoPanel.height();
    }
    showBackButton();

    const $content = $('<div class="content"></div>');

    const participantContent = `
        <div class="slide"></div>
        <img class="img-fluid mb-3 img-preview" src="${participant.img}">
        <div class="p-2">
            <h3>${participant.name}</h3>
            <div class="mb-1">${participant.companyAddress}</div>
            <div class="mobile-hidden">
                <h3>About</h3>
                <div class="mb-2">${participant.infoPaneContent}</div>
                <div class="row row-cols-1 row-cols-2 g-2 mb-3">
                    <div class="col">
                        <div class="card shadow-sm rounded">
                            <div class="card-body">
                                <h3>Role</h3>
                                <div>${participant.type}</div>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card shadow-sm rounded">
                            <div class="card-body">
                                <h3>Company Type</h3>
                                <div>${participant.companyOwner}</div>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card shadow-sm rounded">
                            <div class="card-body">
                                <h3>Founding Year</h3>
                                <div>${participant.foundingYear}</div>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card shadow-sm rounded">
                            <div class="card-body">
                                <h3>Website</h3>
                                <div>${participant.websiteAddress}</div>
                            </div>
                        </div>
                    </div>
                </div>
                ${participant.certification}
            </div>
        </div>    
    `;

    $content.append(participantContent);

    $infoPanel.html($content);

}

function setMarkers(participants) {
    seaRoute.setMap(null);
    truckRouteSapphire.setMap(null);

    markers.forEach(marker => {
        marker.setMap(null);
        marker = null;
    });
    markers = [];
    const bounds = new google.maps.LatLngBounds();

    participants.forEach(participant => {
        const category = participant.type;
        const name = participant.name;
        const id = participant.id;
        const position = new google.maps.LatLng(participant.position[0], participant.position[1]);
        let marker = new google.maps.Marker({
            position: position,
            icon: icons[participant.type].icon,
            map: map,
            animation: google.maps.Animation.DROP,
            category: category,
            name: name,
            id: id,
        });

        markers.push(marker);

        bounds.extend(position);

        google.maps.event.addListener(marker, 'click', function(marker, i) {
            const selectedParticipant = participant;
            onSelectParticipant(selectedParticipant);

            setTimeout(() => {
                map.setCenter(marker.latLng)
            }, 0)
        });
    });
    map.fitBounds(bounds);
    if (map.getZoom() > 5) {
        map.setZoom(5);
    }
}

function focusOnMarker(value) {
    if ($('#infoPanel').hasClass('full-screen')) {
        $('#infoPanel').removeClass('full-screen');
    }
    const m = markers.filter(marker => marker.id == value);
    const bounds = new google.maps.LatLngBounds();
    m.forEach(marker => {
        bounds.extend(marker.getPosition())
    });
    map.fitBounds(bounds);
}

function showBackButton() {
    $('#back-btn').css('visibility', 'visible');
}

function hideBackButton() {
    $('#back-btn').css('visibility', 'hidden');
}

$(document).ready(function() {
     axios.get('./data/participants.json').then(res => {
         participants = res.data;
     });
    const appHeight = () => {
        if (window.innerWidth < 992) {
            const doc = document.getElementById('gdn');
            doc.style.setProperty('height', `${window.innerHeight}px`)
        }
    };
    window.addEventListener('resize', appHeight);
    appHeight();


    $('#supplyChainPanel .supply').on('click', function() {
        selectedSupplyType = $(this).attr('data-type');
        onSelectSupplyType(selectedSupplyType);
    });

    $('#back-btn').on('click', function() {
        $('#autocomplete').val('');
        $('#infoPanel').removeClass('open');
        $('#infoPanel').removeClass('full-screen');
        hideBackButton();
    });

    const infoPanel = document.getElementById("infoPanel");

    infoPanel.addEventListener("touchstart", handleStart, {passive: false});
    infoPanel.addEventListener("touchend", handleEnd, {passive: false});
    infoPanel.addEventListener("touchmove", handleMove, {passive:false});

    let startX, startY;
    let prevX, prevY;
    let height;
    let direction;

    function handleStart(e) {
        if (window.innerWidth > 991) return;
        const evt = e.changedTouches[0];
        startX = prevX = evt.pageX;
        startY = prevY = evt.pageY;
        height = infoPanel.offsetHeight;
    }

    function handleMove(e) {
        if (window.innerWidth > 991) return;
        const evt = e.changedTouches[0];
        const x = evt.pageX;
        const y = evt.pageY;
        const diffY = y - prevY;
        if (diffY < 0) { // move up
            if (infoPanel.classList.contains('full-screen') ) return;
            direction = 'up';
            const newHeight = height + (startY - y);
            if (newHeight >= window.innerHeight) {
                infoPanel.classList.remove('swipe');
                infoPanel.classList.remove('full-screen');
            }
            infoPanel.classList.add('swipe');
            infoPanel.style.height = newHeight + 'px';
        }
        if (diffY > 0) { // move down
            direction = 'down';
            if (infoPanel.classList.contains('full-screen')) {
                if (infoPanel.getElementsByClassName('content')[0].contains(e.target)) {
                    if (infoPanel.getElementsByClassName('content')[0].scrollTop === 0) {
                        infoPanel.classList.remove('full-screen');
                    } else {
                        return false;
                    }
                } else {
                    infoPanel.classList.remove('full-screen');
                }
            }
            const newHeight = height + (startY - y);
            if (newHeight <= infoPanelHeight) {
                infoPanel.classList.remove('swipe');
                infoPanel.style.height = infoPanelHeight + 'px';
            }
            if (!infoPanel.classList.contains('swipe')) {
                infoPanel.classList.add('swipe');
            }
            infoPanel.style.height = newHeight + 'px';
        }
        prevX = x;
        prevY = y;
        e.preventDefault();
        e.stopPropagation();
    }

    function handleEnd(e) {
        if (window.innerWidth > 991) return;
        if (direction === 'up') { // move up
            infoPanel.classList.remove('swipe');
            infoPanel.classList.add('full-screen');
            infoPanel.style.height = 'auto';
        }
        if (direction === 'down') {
            infoPanel.classList.remove('swipe');
            infoPanel.style.height = 'auto';
        }
        startX = null;
        startY = null;
    }
});