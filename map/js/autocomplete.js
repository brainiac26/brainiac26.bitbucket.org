const createAutoComplete = ({
    root,
    renderOption,
    onOptionSelect,
    inputValue,
    fetchData
}) => {
    const input = root.querySelector('input');
    const dropdown = root.querySelector('#dropdown');

    const onInput = async event => {
        const query = event.target.value;
        const items = await fetchData(query);

        dropdown.innerHTML = '';
        if (items.length) {
            dropdown.style.display = 'block';
            for (let item of items) {
                const option = document.createElement('li');

                option.innerHTML = renderOption(item);
                option.addEventListener('click', () => {
                    dropdown.style.display = 'none';
                    input.value = inputValue(item);
                    onOptionSelect(item);

                });

                dropdown.appendChild(option);
            }
        } else {
            dropdown.style.display = 'none';
        }

    };
    input.addEventListener('input', debounceTwo(onInput, 500));
};