(function () {
  angular.module('myApp')
      .component('myMap', {
        template: `
          <div class="map-wrapper">
            <div id="map">map</div>
            
            <div class="map-panel">
            
                <div ng-if="!vm.isEdit">
                  <div class="pull-left area-info">
                    <b class="area-code">{{vm.selectedPolygon.areaGroup.areaCode}}</b>
                    <b>{{vm.selectedPolygon.areaGroup.areaDescription}}</b>
                  </div>
                  <button class="btn btn-primary pull-right" ng-if="vm.canAdd" ng-click="vm.add()">
                      Add area group
                      <i class="glyphicon glyphicon-plus"></i>
                  </button>
  
                  <button class="btn btn-info pull-right" ng-if="vm.canEdit && vm.selectedPolygon" ng-click="vm.edit()">
                      Edit area group
                      <i class="glyphicon glyphicon-pencil"></i>
                  </button>  
                  
                  <button class="btn btn-danger pull-right" ng-if="vm.canDelete && vm.selectedPolygon" ng-click="vm.remove()">
                      Delete area group
                      <i class="glyphicon glyphicon-remove"></i>
                  </button>
                </div>
                
                <div ng-if="vm.isEdit">
                
                  <form class="form-inline pull-left">
                    <input type="text" class="form-control" ng-model="vm.areaCode" ng-model-options="{ updateOn: 'blur' }" ng-change="vm.onAreaChanges('areaCode', vm.areaCode)">
                    <input type="text" class="form-control" ng-model="vm.areaDescription" ng-model-options="{ updateOn: 'blur' }" ng-change="vm.onAreaChanges('areaDescription', vm.areaDescription)">
                  </form>
                  
                  <button class="btn btn-success pull-right" ng-click="vm.cancelEdit()">
                    Stop editing
                    <i class="glyphicon glyphicon-ok"></i>
                  </button>
                
                  <button class="btn btn-primary pull-right" ng-click="vm.addBound()">
                    Add bound
                    <i class="glyphicon glyphicon-plus"></i>
                  </button>
                  
                  <button class="btn btn-danger pull-right" ng-if="vm.selectedVertex" ng-click="vm.deleteVertex()">
                    Delete bound
                    <i class="glyphicon glyphicon-remove"></i>
                  </button>
                
                </div>
                
            </div>
          </div>
        `,
        controller: ('mapController', mapController),
        controllerAs: 'vm',
        bindings: {
          outerAreaGroups: '<areaGroups',
          centre: '@',
          createAreaGroup: '&',
          deleteAreaGroup: '&',
          editAreaGroup: '&',
          createBound: '&',
          deleteBound: '&',
          editBound: '&',
          save: '&',
          cancel: '&',
          canDelete: '<',
          canEdit: '<',
          canAdd: '<'
        }
      });

  function mapController($scope) {

    var vm = this;

    vm.map = null;

    vm.drawingManager = null;

    vm.polygons = [];

    vm.selectedPolygon = null;

    vm.polyline = null;

    vm.isEdit = false;

    vm.markers = [];

    vm.selectedVertex = null;

    vm.$onChanges = onChanges;

    vm.$postLink = postLink;

    vm.add = add;

    vm.addBound = addBound;

    vm.cancelEdit = cancelEdit;

    vm.deleteVertex = deleteVertex;

    vm.edit = edit;

    vm.remove = remove;

    vm.onAreaChanges = onAreaChanges;

    function onChanges() {
      vm.areaGroups = angular.copy(vm.outerAreaGroups);

      if (vm.map) {
        drawAreas();
      }

    }

    function postLink() {
      initMap();
    }

    function add() {
      if (vm.selectedPolygon) {
        unselectPolygon(vm.selectedPolygon);
        vm.selectedPolygon = null;
      }
      vm.drawingManager.setDrawingMode(google.maps.drawing.OverlayType.POLYGON);
    }

    function addBound() {
      var lastPoint = vm.selectedPolygon.getPath().getAt(vm.selectedPolygon.getPath().length - 1);
      var newPoint = angular.copy(lastPoint);

      const lat = newPoint.lat() + 0.1;
      const lng = newPoint.lng() + 0.1;

      vm.createBound({ areaId: vm.selectedPolygon.areaGroup.areaID, gps: lat+ ',' + lng});
    }

    function cancelEdit() {
      vm.isEdit = false;

      vm.selectedPolygon.setOptions({ draggable: true });

      clearMarkers();
    }

    function deleteVertex() {
      vm.deleteBound({ areaId: vm.selectedPolygon.areaGroup.areaID, boundId: vm.selectedVertex.boundID });
      vm.selectedVertex.setMap(null);
      vm.selectedVertex = null;
    }

    function edit() {
      vm.isEdit = true;

      vm.areaCode = vm.selectedPolygon.areaGroup.areaCode;
      vm.areaDescription = vm.selectedPolygon.areaGroup.areaDescription;

      vm.selectedPolygon.setOptions({ draggable: false });

      addMarkers(vm.selectedPolygon);

    }

    function update_polygon_closure(polygon, i){
      return function(event){
        polygon.getPath().setAt(i, event.latLng);
      }
    }

    function selectVertex(polygon, i) {
      return function() {
        if (vm.selectedVertex) {
          vm.selectedVertex.setIcon(getIcon(polygon));
        }

        $scope.$apply(function() {
          vm.selectedVertex = vm.markers[i];
        });

        vm.selectedVertex.setIcon(getSelectedIcon());
      }
    }

    function addMarkers(polygon) {
      clearMarkers();

      var marker_options = {
        map: vm.map,
        icon: getIcon(polygon),
        flat: true,
        draggable: true,
        raiseOnDrag: false
      };

      const path = polygon.getPath();

      for (var i=0; i < path.length; i++){
        marker_options.position = path.getAt(i);
        var point = new google.maps.Marker(Object.assign(marker_options, { boundID: polygon.areaGroup.bounds[i].boundID }));

        vm.markers.push(point);

        google.maps.event.addListener(point, 'drag', update_polygon_closure(polygon, i));

        google.maps.event.addListener(point, 'click', selectVertex(polygon, i));
      }
    }

    function getIcon(polygon) {
      return {
        path: "M -1 -1 L 1 -1 L 1 1 L -1 1 z",
        strokeColor: polygon.strokeColor,
        strokeOpacity: 0,
        fillColor: polygon.fillColor,
        fillOpacity: 1,
        scale: 4
      }
    }

    function getSelectedIcon() {
      return {
        path: "M -1 -1 L 1 -1 L 1 1 L -1 1 z",
        strokeColor: '#000',
        strokeOpacity: 0,
        fillColor: '#000',
        fillOpacity: 1,
        scale: 5
      }
    }

    function clearMarkers() {
      vm.markers.forEach(function(marker) {
        marker.setMap(null);
        marker = null;
        vm.markers = [];
      });

      if (vm.selectedVertex) {
        vm.selectedVertex.setMap(null);
        vm.selectedVertex = null;
      }
    }

    function remove() {
      vm.deleteAreaGroup({ areaID: vm.selectedPolygon.areaGroup.areaID });
      vm.selectedPolygon = null;
    }

    function initMap() {
      var mapProps = {
        center: new google.maps.LatLng(parseCoordsStr(vm.centre)),
        zoom: 9
      };

      vm.map = new google.maps.Map(document.getElementById('map'), mapProps);

      initDrawingManager();
    }

    function onAreaChanges(field, value) {
      vm.editAreaGroup({ areaId: vm.selectedPolygon.areaGroup.areaID, field: field, value: value });
    }

    function initDrawingManager() {
      vm.drawingManager = new google.maps.drawing.DrawingManager({
        drawingControl: false,
        polygonOptions: {
          fillColor: '#FF0000',
          strokeColor: '#FF0000',
          fillOpacity: 0.2,
          strokeWeight: 1,
          clickable: true,
          editable: false,
          draggable: true
        }
      });

      vm.drawingManager.setMap(vm.map);

      drawAreas();

      google.maps.event.addListener(vm.drawingManager, 'polygoncomplete', function(polygon) {
        vm.drawingManager.setOptions({
          drawingMode: null
        });

        $scope.$apply(function() {
          vm.createAreaGroup({ coords: stringifyCoords(polygon) });
        });
        polygon.setMap(null);
        polygon = null;
      });

      google.maps.event.addListener(vm.drawingManager, 'markercomplete', function(marker) {
        vm.drawingManager.setOptions({
          drawingMode: null
        });

        const coord = marker.position;

        marker.setMap(null);
        marker = null;

        $scope.$apply(function() {
          vm.createBound({ areaId: vm.selectedPolygon.areaGroup.areaID, gps: stringifyCoord(coord)});
        })

      });

    }

    function clearPolygons() {
      vm.polygons.forEach(function(polygon) {
        polygon.setMap(null);
      });

      vm.polygons = [];
    }

    function drawAreas() {
      if (vm.polygons.length) {
        clearPolygons();
      }

      vm.areaGroups.forEach(function(areaGroup) {
        var coords = areaGroup.bounds.map(function(bound) {
          return parseCoordsStr(bound.gPS);
        });

        var polygon = new google.maps.Polygon({
          path: coords,
          fillColor: areaGroup.color,
          strokeColor: areaGroup.color,
          strokeOpacity: 1.0,
          strokeWeight: 1,
          areaGroup: angular.copy(areaGroup),
          draggable: true
        });

        polygon.setMap(vm.map);

        vm.polygons.push(polygon);

        if (vm.selectedPolygon && vm.selectedPolygon.areaGroup.areaID === polygon.areaGroup.areaID) {
          selectPolygon(polygon);

          if (vm.isEdit) {
            vm.edit()
          }
        }

        google.maps.event.addListener(polygon, 'click', function () {

          if (vm.isEdit) return;

          vm.polygons.forEach(function(p) {
            unselectPolygon(p);
          });

          $scope.$apply(function() {
            selectPolygon(polygon);
          });

        });


        var polygonPath = polygon.getPath();

        google.maps.event.addListener(polygonPath, 'set_at', function (index) {
          vm.editBound({ areaId: polygon.areaGroup.areaID, index: index, gps: stringifyCoord(polygonPath.getAt(index))});
        });

        google.maps.event.addListener(polygonPath, 'insert_at', function (index) {
          vm.createBound({ areaId: polygon.areaGroup.areaID, index: index, gps: stringifyCoord(polygonPath.getAt(index))})
        });

      });
    }

    function selectPolygon(polygon) {

      polygon.setOptions({fillOpacity: 0.5, strokeWeight: 2 });
      vm.selectedPolygon = polygon;
    }

    function unselectPolygon(polygon) {
      polygon.setOptions({fillOpacity: 0.2, strokeWeight: 1 });
    }

    function parseCoordsStr(str) {
      var coords = str.split(',').map(function(coordStr) {
        return parseFloat(coordStr);
      });
      return {
        lat: coords[0],
        lng: coords[1]
      }
    }

    function stringifyCoords(polygon) {
      var coords = [];
      var l = polygon.getPath().getLength();
      for (var i = 0; i < l; i++) {
        var coord = polygon.getPath().getAt(i);
        coords.push(stringifyCoord(coord));
      }
      return coords;
    }

    function stringifyCoord(coord) {
      return coord.lat().toString() + ',' + coord.lng().toString();
    }

  }


})();