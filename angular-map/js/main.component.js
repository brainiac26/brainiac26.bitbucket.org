(function () {
  angular.module('myApp')
      .component('myMain', {
        template: `
          <div class="main">
            <my-map centre="{{vm.centre}}" 
                    area-groups="vm.areaGroups" 
                    create-area-group="vm.createAreaGroup(coords)"
                    delete-area-group="vm.deleteAreaGroup(areaID)"
                    edit-area-group="vm.editAreaGroup(areaId, field, value)"
                    create-bound="vm.createBound(areaId, gps)"
                    delete-bound="vm.deleteBound(areaId, boundId)"
                    edit-bound="vm.editBound(areaId, index, gps)"
                    save="vm.save(areaGroups)"
                    cancel="vm.calcel()"
                    can-delete="vm.canDelete" 
                    can-edit="vm.canEdit" 
                    can-add="vm.canAdd">
            </my-map>
          </div>
        `,
        controller: ('mainController', mainController),
        controllerAs: 'vm'
      });

  function mainController() {

    const vm = this;

    vm.centre = '34.1201439,18.9065825';

    vm.canDelete = true;

    vm.canEdit = true;

    vm.canAdd = true;

    vm.areaGroups = [
      {
        areaID: 2,
        areaCode: 'AB',
        areaDescription: 'South',
        color: '#0091ff',
        bounds:[
          {
            areaID: 2,
            boundID: 5,
            gPS: '34.9501439,18.0965825'
          },
          {
            areaID: 2,
            boundID: 6,
            gPS: '34.8551339,18.0065825'
          },
          {
            areaID: 2,
            boundID: 7,
            gPS: '34.7221439,18.9095825'
          }
        ]
      },
      {
        areaID: 3,
        areaCode: 'CD',
        areaDescription: 'North',
        color: '#0091ff',
        bounds:[
          {
            areaID: 3,
            boundID: 5,
            gPS: '34.2901439,18.7065825'
          },
          {
            areaID: 3,
            boundID: 6,
            gPS: '34.1231439,18.6075825'
          },
          {
            areaID: 3,
            boundID: 7,
            gPS: '34.3201439,18.8075825'
          }
        ]
      }
    ];

    vm.createAreaGroup = createAreaGroup;

    vm.deleteAreaGroup = deleteAreaGroup;

    vm.editAreaGroup = editAreaGroup;

    vm.cancel = cancel;

    vm.save = save;

    vm.createBound = createBound;

    vm.editBound = editBound;

    vm.deleteBound = deleteBound;

    function createAreaGroup(coords) {

      const areaID = vm.areaGroups[vm.areaGroups.length - 1].areaID + 1;
      var newAreaGroup = {
        areaID: areaID,
        areaCode: '',
        areaDescription: '',
        color: '#0091ff',
        bounds: coords.map(function(coord, i) {
          return {
            areaID: areaID,
            boundID: i,
            gPS: coord
          }
        })
      };

      var newAreaGroups = angular.copy(vm.areaGroups);
      newAreaGroups.push(newAreaGroup);
      vm.areaGroups = newAreaGroups;
    }

    function deleteAreaGroup(areaID) {
      vm.areaGroups = vm.areaGroups.filter(function(areaGroup) {
        return areaGroup.areaID !== areaID;
      });
    }

    function editAreaGroup(areaId, field, value) {
      vm.areaGroups = vm.areaGroups.map(function(areaGroup) {
        if (areaGroup.areaID === areaId) {
          var newAreaGroup = angular.copy(areaGroup);
          newAreaGroup[field] = value;
          return newAreaGroup;
        } else {
          return areaGroup;
        }
      });
    }

    function cancel() {

    };

    function save(areaGroups) {

    };

    function createBound(areaId, gps) {
      console.log(areaId, gps);
      vm.areaGroups = vm.areaGroups.map(function(areaGroup) {
        if (areaGroup.areaID === areaId) {
          var newAreaGroup = angular.copy(areaGroup);
          newAreaGroup.bounds.push({
            areaID: newAreaGroup.areaID,
            boundID: newAreaGroup.bounds[newAreaGroup.bounds.length - 1].boundID + 1,
            gPS: gps
          });
          return newAreaGroup;
        } else {
          return areaGroup;
        }
      });
      console.log(vm.areaGroups);
    }

    function editBound(areaId, index, gps) {
      vm.areaGroups = vm.areaGroups.map(function(areaGroup) {
        if (areaGroup.areaID === areaId) {
          var newAreaGroup = angular.copy(areaGroup);
          newAreaGroup.bounds[index].gPS = gps;
          return newAreaGroup;
        } else {
          return areaGroup;
        }
      });
    }

    function deleteBound(areaId, boundId) {
      vm.areaGroups = vm.areaGroups.map(function(areaGroup) {
        if (areaGroup.areaID === areaId) {
          var newAreaGroup = angular.copy(areaGroup);
          newAreaGroup.bounds = newAreaGroup.bounds.filter(function(bound) {
            return bound.boundID !== boundId;
          });
          return newAreaGroup;
        } else {
          return areaGroup;
        }

      });
      console.log(vm.areaGroups);
    }
  }


})();