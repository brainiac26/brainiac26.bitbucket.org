webpackJsonp(["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<my-app-location></my-app-location>\n\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'app';
    }
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'app-root',
            template: __webpack_require__("./src/app/app.component.html"),
            styles: [__webpack_require__("./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__location_location_module__ = __webpack_require__("./src/app/location/location.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__("./src/app/app.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["E" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* AppComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2__location_location_module__["a" /* LocationModule */]
            ],
            providers: [],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/location/intro/intro.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/location/intro/intro.component.html":
/***/ (function(module, exports) {

module.exports = "<p><strong>Setting your profile location</strong></p>\n<div class=\"well\">\n  <p><strong>1. Enter your location in the above field and hit the \"Search\" button</strong></p>\n  <p>You can start with a zip code or city name, but need to provide a bit more information if the zip code or city are not unique on our planet.</p>\n  <p><strong>ZIP Code examples</strong></p>\n  <p>90010 / 90010, US / 80331, Germany / W1C, UK</p>\n  <p><strong>City examples</strong></p>\n  <p>Hanover / Hanover, US / Hannover, Germany\n    Birmingham / Birmingham, US / Birmingham, UK</p>\n</div>\n\n<div class=\"well\">\n  <p><strong>2. We will try to find the coordinates for your location</strong></p>\n  <p>If successful you will see a marker on a map and you can move it with your mouse to your exact location, if you like to.</p>\n</div>\n\n<div class=\"well\">\n  <p><strong>3. Verify your location (on the right)</strong></p>\n  <p>In some instances region and city cannot be resolved properly from the previously found coordinates. You will need to enter them manually or try to relocate the marker on the map just a tiny bit.\n       You can also add/change your neighborhood on the right side.</p>\n</div>\n\n<div class=\"well\">\n  <p><strong>4. Submit your data (on the right)</strong></p>\n  <p>If everything seems to look alright, submit your data, otherwise just cancel your request.</p>\n</div>\n"

/***/ }),

/***/ "./src/app/location/intro/intro.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IntroComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var IntroComponent = /** @class */ (function () {
    function IntroComponent() {
    }
    IntroComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'my-app-intro',
            template: __webpack_require__("./src/app/location/intro/intro.component.html"),
            styles: [__webpack_require__("./src/app/location/intro/intro.component.css")]
        })
    ], IntroComponent);
    return IntroComponent;
}());



/***/ }),

/***/ "./src/app/location/location.component.css":
/***/ (function(module, exports) {

module.exports = ".container {\n  padding-top: 20px;\n}\n"

/***/ }),

/***/ "./src/app/location/location.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid mt-5\">\n  <div class=\"row\">\n    <div class=\"col-md-7 col-sm-7\">\n      <my-app-search-bar (addressFound)=\"onAddressFound($event)\"></my-app-search-bar>\n    </div>\n  </div>\n\n  <div class=\"row\">\n    <div class=\"col-md-5 col-sm-5 col-md-push-7 col-sm-push-7\">\n      <my-app-ui-panel [location]=\"location\"></my-app-ui-panel>\n    </div>\n    <div class=\"col-md-7 col-sm-7 col-md-pull-5 col-sm-pull-5\">\n      <my-app-intro *ngIf=\"!showMap\"></my-app-intro>\n      <my-app-map-area [mapElementId]=\"mapElementId\"></my-app-map-area>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/location/location.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LocationComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_leaflet_service__ = __webpack_require__("./src/app/location/shared/leaflet.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_address_search_service__ = __webpack_require__("./src/app/location/shared/address-search.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_util_service__ = __webpack_require__("./src/app/location/shared/util.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__shared_location_model__ = __webpack_require__("./src/app/location/shared/location.model.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var LocationComponent = /** @class */ (function () {
    function LocationComponent(mapService, addressSearch, util) {
        this.mapService = mapService;
        this.addressSearch = addressSearch;
        this.util = util;
        this.showMap = false;
        this.mapElementId = 'map';
    }
    LocationComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.util.initValues(this.mode, this.url, this.protocol);
        if (this.predefinedLocation) {
            if (this.predefinedLocation.latitude && this.predefinedLocation.longitude) {
                this.addressSearch.searchByCoordinates(this.predefinedLocation.latitude + "," + this.predefinedLocation.longitude)
                    .subscribe(function (location) {
                    _this.location = location;
                }, function (err) { return alert(err); });
            }
            else if (this.predefinedLocation.address) {
                this.addressSearch.searchByQuery(this.predefinedLocation.address).subscribe(function (location) {
                    _this.location = location;
                }, function (err) { return alert(err); });
            }
        }
        this.mapService.markerPositionChange.subscribe(function (coordinates) {
            _this.addressSearch.searchByCoordinates(coordinates).subscribe(function (location) {
                _this.location = location;
                _this.mapService.setMarker(_this.location.latitude, _this.location.longitude);
            }, function (err) { return alert(err); });
        });
    };
    LocationComponent.prototype.onAddressFound = function (location) {
        this.location = location;
        this.initMap();
    };
    LocationComponent.prototype.initMap = function () {
        var _this = this;
        this.showMap = true;
        this.mapService.initMap(this.mapElementId).then(function () {
            _this.mapService.setMarker(_this.location.latitude, _this.location.longitude);
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["z" /* Input */])(),
        __metadata("design:type", String)
    ], LocationComponent.prototype, "mode", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["z" /* Input */])(),
        __metadata("design:type", String)
    ], LocationComponent.prototype, "url", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["z" /* Input */])(),
        __metadata("design:type", String)
    ], LocationComponent.prototype, "protocol", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["z" /* Input */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_4__shared_location_model__["a" /* Location */])
    ], LocationComponent.prototype, "predefinedLocation", void 0);
    LocationComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'my-app-location',
            template: __webpack_require__("./src/app/location/location.component.html"),
            styles: [__webpack_require__("./src/app/location/location.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__shared_leaflet_service__["a" /* LeafletService */],
            __WEBPACK_IMPORTED_MODULE_2__shared_address_search_service__["a" /* AddressSearchService */],
            __WEBPACK_IMPORTED_MODULE_3__shared_util_service__["a" /* UtilService */]])
    ], LocationComponent);
    return LocationComponent;
}());



/***/ }),

/***/ "./src/app/location/location.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LocationModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__location_component__ = __webpack_require__("./src/app/location/location.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__intro_intro_component__ = __webpack_require__("./src/app/location/intro/intro.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__map_area_map_area_component__ = __webpack_require__("./src/app/location/map-area/map-area.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__search_bar_search_bar_component__ = __webpack_require__("./src/app/location/search-bar/search-bar.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ui_panel_ui_panel_component__ = __webpack_require__("./src/app/location/ui-panel/ui-panel.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__shared_address_search_service__ = __webpack_require__("./src/app/location/shared/address-search.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__shared_leaflet_service__ = __webpack_require__("./src/app/location/shared/leaflet.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__shared_util_service__ = __webpack_require__("./src/app/location/shared/util.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};












var LocationModule = /** @class */ (function () {
    function LocationModule() {
    }
    LocationModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_4__location_component__["a" /* LocationComponent */],
                __WEBPACK_IMPORTED_MODULE_5__intro_intro_component__["a" /* IntroComponent */],
                __WEBPACK_IMPORTED_MODULE_6__map_area_map_area_component__["a" /* MapAreaComponent */],
                __WEBPACK_IMPORTED_MODULE_7__search_bar_search_bar_component__["a" /* SearchBarComponent */],
                __WEBPACK_IMPORTED_MODULE_8__ui_panel_ui_panel_component__["a" /* UiPanelComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_common__["a" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormsModule */]
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_4__location_component__["a" /* LocationComponent */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_9__shared_address_search_service__["a" /* AddressSearchService */],
                __WEBPACK_IMPORTED_MODULE_10__shared_leaflet_service__["a" /* LeafletService */],
                __WEBPACK_IMPORTED_MODULE_11__shared_util_service__["a" /* UtilService */]
            ]
        })
    ], LocationModule);
    return LocationModule;
}());



/***/ }),

/***/ "./src/app/location/map-area/map-area.component.css":
/***/ (function(module, exports) {

module.exports = ".map {\n  height: 400px;\n  width: 100%;\n}\n"

/***/ }),

/***/ "./src/app/location/map-area/map-area.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"map\" [attr.id]=\"mapElementId\"></div>\n"

/***/ }),

/***/ "./src/app/location/map-area/map-area.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MapAreaComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var MapAreaComponent = /** @class */ (function () {
    function MapAreaComponent() {
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["z" /* Input */])(),
        __metadata("design:type", String)
    ], MapAreaComponent.prototype, "mapElementId", void 0);
    MapAreaComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'my-app-map-area',
            template: __webpack_require__("./src/app/location/map-area/map-area.component.html"),
            styles: [__webpack_require__("./src/app/location/map-area/map-area.component.css")]
        })
    ], MapAreaComponent);
    return MapAreaComponent;
}());



/***/ }),

/***/ "./src/app/location/search-bar/search-bar.component.css":
/***/ (function(module, exports) {

module.exports = ".search {\n  margin-bottom: 20px;\n}\n\n#query {\n  min-width: 300px;\n}\n"

/***/ }),

/***/ "./src/app/location/search-bar/search-bar.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"search form-inline\">\n  <div class=\"form-group\">\n    <div class=\"input-group\">\n      <input type=\"text\" id=\"query\" class=\"form-control\"\n             placeholder=\"Enter address\" [(ngModel)]=\"query\"\n             (keyup.enter)=\"search()\">\n      <div class=\"input-group-append\">\n        <button type=\"button\" class=\"btn btn-primary\" (click)=\"search()\"\n                [disabled]=\"!query\">Search</button>\n      </div>\n    </div>\n\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/location/search-bar/search-bar.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchBarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_address_search_service__ = __webpack_require__("./src/app/location/shared/address-search.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SearchBarComponent = /** @class */ (function () {
    function SearchBarComponent(addressSearch) {
        this.addressSearch = addressSearch;
        this.query = '';
        this.addressFound = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* EventEmitter */]();
    }
    SearchBarComponent.prototype.search = function () {
        var _this = this;
        if (this.query) {
            this.addressSearch.searchByQuery(this.query).subscribe(function (address) {
                _this.addressFound.emit(address);
            }, function (err) {
                window.alert(err);
            });
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["K" /* Output */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* EventEmitter */])
    ], SearchBarComponent.prototype, "addressFound", void 0);
    SearchBarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'my-app-search-bar',
            template: __webpack_require__("./src/app/location/search-bar/search-bar.component.html"),
            styles: [__webpack_require__("./src/app/location/search-bar/search-bar.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__shared_address_search_service__["a" /* AddressSearchService */]])
    ], SearchBarComponent);
    return SearchBarComponent;
}());



/***/ }),

/***/ "./src/app/location/shared/address-search.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddressSearchService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_switchMap__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/switchMap.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__shared_location_model__ = __webpack_require__("./src/app/location/shared/location.model.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AddressSearchService = /** @class */ (function () {
    function AddressSearchService(http) {
        this.http = http;
        this.baseUrl = '//maps.googleapis.com/maps/api/geocode/json';
    }
    AddressSearchService.prototype.searchByQuery = function (query) {
        var url = this.baseUrl + '?address=' + encodeURIComponent(query);
        return this.search(url);
    };
    AddressSearchService.prototype.searchByCoordinates = function (coordinates) {
        var url = this.baseUrl + '?latlng=' + coordinates;
        return this.search(url);
    };
    AddressSearchService.prototype.getCurrentLocation = function () {
        var _this = this;
        return this.http
            .get('http://ipv4.myexternalip.com/json')
            .map(function (res) { return res.json().ip; })
            .switchMap(function (ip) { return _this.http.get('http://freegeoip.net/json/' + ip); })
            .map(function (res) { return res.json(); })
            .map(function (result) {
            return { latitude: result.latitude, longitude: result.longitude };
        });
    };
    AddressSearchService.prototype.search = function (url) {
        return this.http
            .get(url)
            .map(function (res) { return res.json(); })
            .map(function (result) {
            if (result.status !== 'OK') {
                throw new Error(result.status || 'An error occurred');
            }
            if (!result['results'].length) {
                throw new Error('No addresses found');
            }
            return new __WEBPACK_IMPORTED_MODULE_4__shared_location_model__["a" /* Location */](result['results'][0]);
        });
    };
    AddressSearchService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */]])
    ], AddressSearchService);
    return AddressSearchService;
}());



/***/ }),

/***/ "./src/app/location/shared/leaflet.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LeafletService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_ReplaySubject__ = __webpack_require__("./node_modules/rxjs/_esm5/ReplaySubject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__address_search_service__ = __webpack_require__("./src/app/location/shared/address-search.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_leaflet__ = __webpack_require__("./node_modules/leaflet/dist/leaflet-src.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_leaflet___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_leaflet__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LeafletService = /** @class */ (function () {
    function LeafletService(addressSearch) {
        this.addressSearch = addressSearch;
        this.markerPositionChange = new __WEBPACK_IMPORTED_MODULE_1_rxjs_ReplaySubject__["a" /* ReplaySubject */]();
        this.baseCoordinates = { latitude: 40.731253, longitude: -73.996139 };
        this.baseMaps = {
            OpenStreetMap: __WEBPACK_IMPORTED_MODULE_3_leaflet__["tileLayer"]("http://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png", {
                attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>, Tiles courtesy of <a href="http://hot.openstreetmap.org/" target="_blank">Humanitarian OpenStreetMap Team</a>'
            }),
            Esri: __WEBPACK_IMPORTED_MODULE_3_leaflet__["tileLayer"]("http://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}", {
                attribution: 'Tiles &copy; Esri &mdash; Esri, DeLorme, NAVTEQ, TomTom, Intermap, iPC, USGS, FAO, NPS, NRCAN, GeoBase, Kadaster NL, Ordnance Survey, Esri Japan, METI, Esri China (Hong Kong), and the GIS User Community'
            }),
            CartoDB: __WEBPACK_IMPORTED_MODULE_3_leaflet__["tileLayer"]("http://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png", {
                attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> &copy; <a href="http://cartodb.com/attributions">CartoDB</a>'
            })
        };
    }
    LeafletService.prototype.initMap = function (id) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (!_this.map) {
                _this.addressSearch.getCurrentLocation().subscribe(function (coords) {
                    _this.createMap(id, coords);
                    resolve();
                }, function () {
                    _this.createMap(id, _this.baseCoordinates);
                    resolve();
                });
            }
            else {
                resolve();
            }
        });
    };
    LeafletService.prototype.setMarker = function (lat, lng) {
        var _this = this;
        if (!this.marker) {
            var myIcon = __WEBPACK_IMPORTED_MODULE_3_leaflet__["icon"]({
                iconUrl: 'http://maps.google.com/mapfiles/ms/icons/blue.png',
                shadowSize: [0, 0]
            });
            this.marker = __WEBPACK_IMPORTED_MODULE_3_leaflet__["marker"]([lat, lng], { draggable: true, icon: myIcon }).addTo(this.map);
            this.map.panTo([lat, lng]);
            this.marker.on('dragend', function (event) {
                var coordinates = event.target.getLatLng();
                _this.markerPositionChange.next(_this.getCoordsString(coordinates));
            });
        }
        else {
            this.marker.setLatLng([lat, lng]);
            this.map.panTo([lat, lng]);
        }
    };
    LeafletService.prototype.getCoordsString = function (coordinates) {
        return coordinates.lat + "," + coordinates.lng;
    };
    LeafletService.prototype.createMap = function (id, coords) {
        this.map = __WEBPACK_IMPORTED_MODULE_3_leaflet__["map"](id, {
            zoomControl: false,
            center: __WEBPACK_IMPORTED_MODULE_3_leaflet__["latLng"](coords),
            zoom: 12,
            minZoom: 4,
            maxZoom: 19,
            layers: [this.baseMaps.OpenStreetMap]
        });
        __WEBPACK_IMPORTED_MODULE_3_leaflet__["control"].zoom({ position: 'topright' }).addTo(this.map);
        __WEBPACK_IMPORTED_MODULE_3_leaflet__["control"].layers(this.baseMaps).addTo(this.map);
        __WEBPACK_IMPORTED_MODULE_3_leaflet__["control"].scale().addTo(this.map);
    };
    LeafletService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__address_search_service__["a" /* AddressSearchService */]])
    ], LeafletService);
    return LeafletService;
}());



/***/ }),

/***/ "./src/app/location/shared/location.model.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Location; });
var Location = /** @class */ (function () {
    function Location(addressResult) {
        var _this = this;
        this.latitude = addressResult.geometry.location.lat;
        this.longitude = addressResult.geometry.location.lng;
        this.address = addressResult.formatted_address;
        addressResult.address_components.forEach(function (component) {
            if (component.types.indexOf('country') !== -1) {
                _this.country = component.short_name;
            }
            if (component.types.indexOf('administrative_area_level_1') !== -1) {
                _this.region = component.long_name;
            }
            if (component.types.indexOf('administrative_area_level_2') !== -1) {
                _this.city = component.long_name;
            }
            if (component.types.indexOf('neighborhood') !== -1) {
                _this.neighbourhood = component.long_name;
            }
        });
    }
    Location.prototype.isFilled = function () {
        return !!(this.country && this.region && this.city);
    };
    return Location;
}());



/***/ }),

/***/ "./src/app/location/shared/util.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UtilService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_ReplaySubject__ = __webpack_require__("./node_modules/rxjs/_esm5/ReplaySubject.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var UtilService = /** @class */ (function () {
    function UtilService() {
        this.initializeValues = new __WEBPACK_IMPORTED_MODULE_1_rxjs_ReplaySubject__["a" /* ReplaySubject */]();
    }
    UtilService.prototype.initValues = function (mode, url, protocol) {
        this.mode = mode || 'GET';
        this.url = url || 'https://brownhanky.com/index.php?r=test/use-location';
        this.protocol = protocol || this.getProtocol(this.url);
        this.initializeValues.next({ mode: this.mode, url: this.url, protocol: this.protocol });
    };
    UtilService.prototype.getProtocol = function (url) {
        var protocol = url.split('//')[0];
        if (protocol !== 'http:' && protocol !== 'https:') {
            protocol = window.location.protocol;
        }
        return protocol;
    };
    UtilService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* Injectable */])()
    ], UtilService);
    return UtilService;
}());



/***/ }),

/***/ "./src/app/location/ui-panel/ui-panel.component.css":
/***/ (function(module, exports) {

module.exports = ".margin-bottom {\n  margin-bottom: 10px;\n}\n"

/***/ }),

/***/ "./src/app/location/ui-panel/ui-panel.component.html":
/***/ (function(module, exports) {

module.exports = "<form>\n\n  <div class=\"row margin-bottom\">\n    <div class=\"col-md-2\">Full address:</div>\n    <div class=\"col-md-10\" *ngIf=\"location\">\n      <input type=\"text\" name=\"address\" class=\"form-control\" [value]=\"location.address || '' \" readonly>\n    </div>\n  </div>\n\n  <div class=\"row margin-bottom\">\n    <div class=\"col-md-2\">Country:</div>\n    <div class=\"col-md-10\" *ngIf=\"location\">\n      <input type=\"text\" name=\"country\" class=\"form-control\" [value]=\"location.country || '' \" readonly>\n    </div>\n  </div>\n\n  <div class=\"row margin-bottom\">\n    <div class=\"col-md-2\">Region:</div>\n    <div class=\"col-md-10\" *ngIf=\"location\">\n      <input type=\"text\" name=\"region\" class=\"form-control\" [value]=\"location.region || '' \" readonly>\n    </div>\n  </div>\n\n  <div class=\"row margin-bottom\">\n    <div class=\"col-md-2\">City:</div>\n    <div class=\"col-md-10\" *ngIf=\"location\">\n      <input type=\"text\" name=\"city\" class=\"form-control\" [value]=\"location.city || '' \" readonly>\n    </div>\n  </div>\n\n  <div class=\"row margin-bottom\">\n    <div class=\"col-md-2\">Neighborhood (optional):</div>\n    <div class=\"col-md-10\" *ngIf=\"location\">\n      <input type=\"text\" name=\"neighbourhood\" class=\"form-control\" [value]=\"location.neighbourhood || '' \">\n    </div>\n  </div>\n\n  <!--<div class=\"row margin-bottom\" *ngIf=\"location\">-->\n    <!--<div class=\"col\">-->\n      <!--<button type=\"submit\" class=\"btn btn-success btn-block\" [disabled]=\"!location || !location.isFilled()\" >-->\n        <!--Use location-->\n      <!--</button>-->\n    <!--</div>-->\n    <!--<div class=\"col\">-->\n      <!--<button type=\"button\" class=\"btn btn-danger btn-block\" (click)=\"cancel()\">Cancel</button>-->\n    <!--</div>-->\n  <!--</div>-->\n\n</form>\n\n"

/***/ }),

/***/ "./src/app/location/ui-panel/ui-panel.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UiPanelComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_location_model__ = __webpack_require__("./src/app/location/shared/location.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_util_service__ = __webpack_require__("./src/app/location/shared/util.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var UiPanelComponent = /** @class */ (function () {
    function UiPanelComponent(util) {
        var _this = this;
        this.util = util;
        this.util.initializeValues.subscribe(function (values) {
            _this.url = values.url;
            _this.method = values.mode;
        });
    }
    UiPanelComponent.prototype.cancel = function () {
        window.location.href = this.url;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["z" /* Input */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1__shared_location_model__["a" /* Location */])
    ], UiPanelComponent.prototype, "location", void 0);
    UiPanelComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'my-app-ui-panel',
            template: __webpack_require__("./src/app/location/ui-panel/ui-panel.component.html"),
            styles: [__webpack_require__("./src/app/location/ui-panel/ui-panel.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__shared_util_service__["a" /* UtilService */]])
    ], UiPanelComponent);
    return UiPanelComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false
};


/***/ }),

/***/ "./src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("./node_modules/@angular/platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("./src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("./src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_7" /* enableProdMode */])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map