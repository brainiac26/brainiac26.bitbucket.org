(function($) {
  var map;

  var center = [-33.8650, 151.2094];
  var autocomplete;
  var place;
  var sqft;

  var markerList = [];
  var calculatedPoly;

  var slide = 1;

  var $carousel;
  var $addressField;
  var $showMapButton;
  var $addressWrapper;
  var $address;
  var $edit;
  var $continueButton;
  var $deleteButton;
  var $backButton;
  var $undoButton;
  var $steps;
  var $trace;
  var $slope;
  var $info;
  var $form;

  $(document).ready(function() {
    $carousel = $('#funnelCarousel');
    $addressField = $('.address-field');
    $showMapButton = $('.show-map-button');
    $addressWrapper = $('.address-search-wrapper');
    $address= $('.address');
    $edit = $('.edit');
    $continueButton = $('#continue-button');
    $backButton = $('.back');
    $undoButton = $('.undo');
    $steps = $('.step-indicator > li');
    $trace = $('.trace');
    $slope = $('.slope');
    $info = $('.info');
    $form = $('.contactForm');

    $backButton.hide();
    $continueButton.attr('disabled', true);
    $undoButton.attr('disabled', true);
    $trace.removeClass('incomplete').addClass('active');

    init();
    bindEvents();
  });


  function init() {

    $carousel.carousel({
      interval: false,
      keyboard: false
    });

    $carousel.on('slide.bs.carousel', function (e) {
      if (e.direction === 'left') {
        slide++;
      } else {
        slide--;
      }
      if (slide === 1) {
        $backButton.hide();
      } else {
        $backButton.show();
      }

      switch (slide) {
        case 1:
          $steps.removeClass('active').removeClass('complete').addClass('incomplete');
          $trace.removeClass('incomplete').addClass('active');
          break;
        case 2:
          $steps.removeClass('active');
          $trace.removeClass('incomplete').addClass('complete');
          $info.removeClass('complete').addClass('incomplete');
          $slope.removeClass('incomplete').addClass('active');
          break;
        case 3:
          $steps.removeClass('active').addClass('complete');
          $info.removeClass('incomplete').addClass('active');
          break;
      }
    });

    map = L.map('map').setView(center, 6);

    L.tileLayer('http://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}',{
      maxZoom: 20,
      subdomains:['mt0','mt1','mt2','mt3']
    }).addTo(map);

    autocomplete = new google.maps.places.Autocomplete($addressField.get(0));

    autocomplete.addListener('place_changed', function () {
      place = autocomplete.getPlace();

      $address.text(place.formatted_address);

      if (place.geometry.location) {
        map.setView([place.geometry.location.lat(), place.geometry.location.lng()], 20);
      }
    });

    map.on('click', addMarker);

  }

 function addMarker(event) {
    var newMarker = new L.marker(event.latlng, {
      draggable: 'true',
      icon: L.divIcon({
        className: 'leaflet-mouse-marker',
        iconAnchor: [6, 6],
        iconSize: [12, 12]
      })
    }).on('dragend', function(event) {
      var marker = event.target;
      var position = marker.getLatLng();
      marker.setLatLng(new L.LatLng(position.lat, position.lng), {
        draggable: 'true'
      });
      drawPoly(markerList);
    }).addTo(map);
    markerList.push(newMarker);
    drawPoly(markerList);
  }

  function drawPoly(list) {
    if (list.length === 0) {
      return;
    }
    var polygonCoordinates = list.map(function(value) {
      return [value.getLatLng().lat, value.getLatLng().lng]
    });
    $continueButton.attr('disabled', list.length < 3);
    $undoButton.attr('disabled', list.length < 1);
    polygonCoordinates = polygonCoordinates.concat([new L.LatLng(polygonCoordinates[0][0], polygonCoordinates[0][1])]);
    if (calculatedPoly)
      calculatedPoly.removeFrom(map);
    calculatedPoly = L.polyline(polygonCoordinates, {
      'fill': true
    }).addTo(map);
    sqft = Math.round(L.GeometryUtil.geodesicArea(calculatedPoly.getLatLngs()) * 10.7639104);
  }

  function bindEvents() {
    $showMapButton.click(function() {
      $addressWrapper.hide();
    });

    $undoButton.click(function() {
      if (markerList.length == 0) return;
      markerList.pop().remove();
      $undoButton.attr('disabled', markerList.length < 1);
      drawPoly(markerList);
    });

    $edit.click(function() {
      $addressWrapper.show();
    });

    $form.find('button[type="submit"]').click(function(e) {
      var name = $('.nameField').val();
      var email = $('.emailField').val();
      var password = $('.passwordField').val();
      var phone = $('.phoneField').val();
      console.log(place);
      console.log('Name: ' + name + ', email: ' + email + ', password: ' + password + ', phone: ' + phone + 'address: ' + place.formatted_address +', sqft: ' + sqft + ', lat: ' + place.geometry.location.lat() + ', lng: ' + place.geometry.location.lng());
      e.preventDefault();
    })
  }
})(jQuery);