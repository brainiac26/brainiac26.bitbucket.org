(function($) {

  var googleMap,
    marker;

  function CustomMarker(latlng, map) {
    this.latlng = latlng;
    this.setMap(map);
  }

  CustomMarker.prototype = new google.maps.OverlayView();

  CustomMarker.prototype.draw = function() {

    var self = this;

    var div = this.div;

    if (!div) {

      div = this.div = document.createElement('div');

      div.className = 'markerWrapper';
      div.style.position = "absolute";

      var marker = document.createElement('div');

      marker.style.width = '60px';
      marker.style.height = '60px';
      marker.style.borderRadius = "50% 50% 50% 0";
      marker.style.background = "#ffa500";
      marker.style.transform = "rotate(-45deg)";
      marker.style.left = "50%";
      marker.style.top = "50%";
      marker.style.margin = "-20px 0 0 -20px";
      marker.style.zIndex = 1;

      div.appendChild(marker);

      var avatar = document.createElement('div');
      avatar.style.background = 'url(https://s3.amazonaws.com/uifaces/faces/twitter/kevinoh/128.jpg) center center no-repeat';
      avatar.style.bacgroundSize = 'cover';
      avatar.style.height = '50px';
      avatar.style.width = '50px';
      avatar.style.borderRadius = "50%";
      avatar.style.position = 'absolute';
      avatar.style.top = '-15px';
      avatar.style.right = '5px';
      avatar.style.zIndex = 2;

      div.appendChild(avatar);

      var year = document.createElement('div');
      year.style.position = 'absolute';
      year.style.background = '#fff';
      year.style.padding = '2px 5px';
      year.style.border = '2px solid #ffa500';
      year.style.borderRadius = '10px';
      year.style.left = '-8px';
      year.style.top = '25px';
      year.style.fontWeight = 'bold';
      year.style.color = '#333';
      year.style.zIndex = 3;

      year.innerHTML = '2015';

      div.appendChild(year);

      var title = document.createElement('div');
      title.style.position = 'absolute';
      title.style.background = '#ffa500';
      title.style.color = '#fff';
      title.style.textAlign = 'center';
      title.style.padding = '5px 18px';
      title.style.borderRadius = '10px';
      title.style.left = '-25px';
      title.style.top = '-40px';
      title.style.fontWeight = 'bold';
      title.style.zIndex = 3;

      title.innerHTML = 'Human Rights';

      div.appendChild(title);

      google.maps.event.addDomListener(div, "click", function(event) {
        google.maps.event.trigger(self, "click");
      });

      var panes = this.getPanes();
      panes.overlayImage.appendChild(div);
    }

    var point = this.getProjection().fromLatLngToDivPixel(this.latlng);

    if (point) {
      div.style.left = point.x + 'px';
      div.style.top = point.y + 'px';
    }
  };

  CustomMarker.prototype.remove = function() {
    if (this.div) {
      this.div.parentNode.removeChild(this.div);
      this.div = null;
    }
  };

  CustomMarker.prototype.getPosition = function() {
    return this.latlng;
  };

  $(document).ready(function() {
    googleMap = new google.maps.Map(document.getElementById('map'), {
      center: new google.maps.LatLng(36.2398864, -113.7644643),
      zoom: 6,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      mapTypeControl: false,
      streetViewControl: false
    });

    marker = new CustomMarker(new google.maps.LatLng(36.2398864, -113.7644643), googleMap);
  });

})($);

