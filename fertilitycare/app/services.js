(function() {
	angular.module('FertilityApp')

	.value("DefaultGeolocation", {latitude: 36.2397807, longitude: -113.7651516})

	.value("SearchString", "fertilitycare crms")

	.factory("GeolocationService", function ($q, $rootScope) {
		return {
			getCurrentGeolocation: getCurrentGeolocation
		}

		function getCurrentGeolocation() {
			var deferred = $q.defer(),
			geolocation;
			if (navigator.geolocation) {
				navigator.geolocation.getCurrentPosition(function (currentGeolocation) {
					$rootScope.$apply(function () {
						geolocation = {
							latitude: currentGeolocation.coords.latitude,
							longitude: currentGeolocation.coords.longitude
						};
						deferred.resolve(geolocation);
					});
				}, function () {
					deferred.reject();
				});
			} else {
				deferred.reject();
			}
			return deferred.promise;
		}
	})

	.factory('AddressSearchService', function($q, SearchString) {
		var service;

		return {
			activate: activate,
			getAddresses: getAddresses,
			getAddressDetails: getAddressDetails
		}

		function activate(map) {
			service = new google.maps.places.PlacesService(map);
		}
		
		function getAddresses(postalCode) {
			var deferred = $q.defer();
			service.textSearch({'query': SearchString + ' ' + postalCode}, function(results) {
				deferred.resolve(results);
			});
			return deferred.promise;
		}

		function getAddressDetails(address) {
			var deferred = $q.defer();
			service.getDetails({placeId: address.place_id}, function(results) {
				deferred.resolve(results);
			}, function() {
				deferred.reject();
			});
			return deferred.promise;
		}
	})
})();