(function() {
	angular.module('FertilityApp')

	.filter('minutes', function() {
		return function(input) {
			if (input.toString().length === 1) {
				return "0" + input;
			}
			return input;
		}
	})
	
})();