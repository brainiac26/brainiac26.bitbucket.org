(function() {
	angular.module('FertilityApp')

	.controller('MainController', function($scope, $timeout, $filter, DefaultGeolocation, GeolocationService, AddressSearchService) {
		$scope.map = {
			center: DefaultGeolocation,
			zoom: 6,
			options: {
				mapTypeId: google.maps.MapTypeId.ROADMAP,
				mapTypeControl: false
			},
			control: {},
			window: {
				coords: {},
				desc: "",
				show: false
			},
			markers: []
		}

		$scope.panelVisible = false;
		$scope.showAddressDetails = false;
		$scope.addresses = [];
		$scope.addressDetails;
		$scope.weekday = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];
		$scope.showReviews = false;
		$scope.request = "";

		$scope.chooseAddress = chooseAddress;
		$scope.getAddressDetails = getAddressDetails;
		$scope.getAddressFullImg = getAddressFullImg;
		$scope.getAddressThumbnail = getAddressThumbnail;
		$scope.getOpeningHours = getOpeningHours;
		$scope.hasImage = hasImage;
		$scope.hasReviews = hasReviews;
		$scope.hideAddressDetails = hideAddressDetails;
		$scope.isToday = isToday;
		$scope.noResults = noResults;
		$scope.openReviews = openReviews;
		$scope.removePostalCode = removePostalCode;
		$scope.searchAddresses = searchAddresses;
		$scope.searchAddressesOnKeypress = searchAddressesOnKeypress;
		
		activate();

		function activate() {
			GeolocationService.getCurrentGeolocation().then(function(coords) {
				$scope.map.center = coords;
			});
			$timeout(function() {
				AddressSearchService.activate($scope.map.control.getGMap());
			},0);
		}

		function addMarkers(addresses) {
			$scope.map.markers = [];
			addresses.forEach(function(address, index) {
				$scope.map.markers.push(getMarker(address, index));
			});
		}

		function chooseAddress(marker) {
			getAddressDetails($scope.addresses[marker.model.id]);
		}

		function getAddressFullImg(address) {
			return getAddressImg(address, 408, 300);
		}

		function getAddressThumbnail(address) {
			return getAddressImg(address, 100, 100);
		}

		function getAddressImg(address, maxWidth, minWidth) {
			return address.photos[0].getUrl({maxWidth: maxWidth, maxHeight: minWidth});
		}

		function getAddressDetails(address) {
			AddressSearchService.getAddressDetails(address).then(function(addressDetails) {
				$scope.showAddressDetails = true;
				$scope.addressDetails = addressDetails;
				$scope.map.markers = [getMarker(address, 0)];
			});
		}

		function getOpeningHours(openingHours) {
			if (!openingHours.close) {
				return 'open 24 hours';
			} else {
				return openingHours.open.hours + ':' + $filter('minutes')(openingHours.open.minutes) +
				' - ' + openingHours.close.hours + ':' + $filter('minutes')(openingHours.close.minutes);
			}
		}

		function getMarker(address, id) {
			return {
				location: {latitude: address.geometry.location.lat(), longitude: address.geometry.location.lng()},
				id: id
			}
		}


		function hasImage(address) {
			return address.photos && address.photos.length;
		}

		function hasReviews(addressDetails) {
			return addressDetails.reviews && addressDetails.reviews.length;
		}

		function hideAddressDetails() {
			$scope.showAddressDetails = false;
			addMarkers($scope.addresses);
		}

		function isToday(dayNumber) {
			return new Date().getDay() === dayNumber;
		}

		function removePostalCode() {
			$scope.postalCode = "";
			$scope.address = undefined;
			$scope.addresses = [];
			$scope.panelVisible = false;
			$scope.showAddressDetails = false;
			$scope.map.markers = [];
		}

		function getAddresses(postalCode) {
			AddressSearchService.getAddresses(postalCode).then(function(addresses) {
				$scope.request = postalCode;
				$scope.addresses = addresses;
				$scope.panelVisible = true;
				$scope.showAddressDetails = false;
				addMarkers(addresses);
			});			
		}

		function noResults() {
			return !$scope.addresses.length; 
		}

		function openReviews() {
			$scope.showReviews = true;
		}

		function searchAddresses(postalCode) {
			if (postalCode) {
				getAddresses(postalCode);
			}
		}

		function searchAddressesOnKeypress($event, postalCode) {
			if ($event.which === 13 && postalCode) {
				getAddresses(postalCode);
			}
		}
	});
})();