var MAXZOOM = 9,
    MINZOOM = 2,
    MAXFAKEZOOM = 14,
    TILESIZE = 1024,
    LATINICIAL = 30,
    LNGINICIAL = -3.42,
    
    map;

google.maps.event.addDomListener(window, 'load', initialize);

Number.prototype.mod = function (n) {
    return ((this % n) + n) % n;
}

function initialize() {
    var nightMapType = new google.maps.ImageMapType({
        getTileUrl: function (coord, zoom) {
            if ((zoom > MAXZOOM) && (zoom <= MAXFAKEZOOM))
                zoom = MAXZOOM;
            zoom -= Math.log(TILESIZE / 256) / Math.log(2);
            var zfactor = Math.pow(2, zoom);
            var ymax = 1 << zoom;
            var y = ymax - coord.y - 1;
            var url = "http://www.nightearth.com/nightmaps2000/" + zoom + "/" + coord.x.mod(zfactor) + "/" + y.mod(zfactor) + ".png"
            return url;
        },
        minZoom: 2,
        maxZoom: MAXFAKEZOOM,
        tileSize: new google.maps.Size(TILESIZE, TILESIZE),
        name: "Night"
    });
    map = new google.maps.Map(document.getElementById('map'), {   
        backgroundColor: '#00011d',
        center: new google.maps.LatLng(LATINICIAL, LNGINICIAL),
        minZoom: 1,
        zoom: 3
    });
    map.mapTypes.set('night', nightMapType);
    map.setMapTypeId('night');
    
    addMarkers();
}

function addMarkers() {
    var markers = [];
    var infowindow = new google.maps.InfoWindow({
        content: "contentString"
    });
    for (var i = 0; i < 100; i++) {
        var dataPhoto = data.photos[i];
        var latLng = new google.maps.LatLng(dataPhoto.latitude,
            dataPhoto.longitude);
        var marker = new google.maps.Marker({
            position: latLng,
            icon: 'img/star_40.png'
        });
        google.maps.event.addListener(marker , 'click', function() {
            infowindow.open(map, this);
        });
        markers.push(marker);
    }
    fitMarkersOnMap(markers);
    var styles = [{
        height: 70,
        url: "img/star_80.png",
        width: 70,
        textSize: 1
        }];
    var markerCluster = new MarkerClusterer(map, markers, {
        styles: styles
    }); 
}

function fitMarkersOnMap(markers) {
    var bounds = new google.maps.LatLngBounds();
    for(i=0;i<markers.length;i++) {
     bounds.extend(markers[i].getPosition());
    }

    map.fitBounds(bounds);
}

