var map;

google.maps.event.addDomListener(window, 'load', initialize);

function initialize() {  
    var nightStyle = [{     // style for google map that make it look dark
        featureType: "all",
        elementType: "all",
        stylers: [{
            invert_lightness: true
        }, {
            lightness: -70
        }, {
            saturation: -90
        }]
    }, {
        featureType: "road",
        elementType: "all",
        stylers: [{
            lightness: 33
        }, {
            saturation: -90
        }]
    }, {
        featureType: "water",
        elementType: "all",
        stylers: [{
            lightness: -99
        }, {
            saturation: -99
        }, {
            hue: "#000000"
        }]
    }, {
        featureType: "administrative",
        elementType: "all",
        stylers: [{
            lightness: 20
        }]
    }, {
        featureType: "landscape",
        elementType: "all",
        stylers: [{
            saturation: -99
        }]
    }, {
        featureType: "transit",
        elementType: "all",
        stylers: [{
            lightness: 20
        }]
    }, {
        featureType: "poi",
        elementType: "all",
        stylers: [{
            lightness: 10
        }]
    }];

    var nightMapType = new google.maps.ImageMapType({ // map type with night lights
        getTileUrl: function (tile, zoom) {
            tile = getNormalizedCoord(tile, zoom);
            if (!tile)
                return null;
            var resolution = String('000000' + 256 * Math.pow(2, zoom)).slice(-6);
            var yCoo = padTileCoordinate(tile.y, zoom);
            var xCoo = padTileCoordinate(tile.x, zoom);
            var subDir = resolution + (zoom == 10 ? '/' + yCoo.toString().substring(0, 2) : '');
            return 'http://blue-marble.de/google/tiles_n2008/' + subDir + '/' + resolution + '_' + yCoo + '_' + xCoo + '.jpg'
        },
        tileSize: new google.maps.Size(256, 256),
        maxZoom: 10,
        name: "Night"
    });
    map = new google.maps.Map(document.getElementById('map'), { // create google map
        backgroundColor: '#000',
        center: new google.maps.LatLng(0, 0),
        zoom: 2,
        minZoom: 2,
        disableDefaultUI: true
    });
    var nightStyledMapOptions = {
        map: map,
    };
    var nightStyledMap = new google.maps.StyledMapType(nightStyle, nightStyledMapOptions);
    map.mapTypes.set('night', nightMapType);
    map.mapTypes.set('dark', nightStyledMap);
    map.setMapTypeId('dark');

    addMarkers();

    google.maps.event.addDomListener(map, 'zoom_changed', function () { // show map with city lights or map with dark style depending on zoom
        var zoom = map.getZoom();
        if (zoom > 6) {
            map.setMapTypeId('dark');
            nightMapType.setOpacity(0.3);
            map.overlayMapTypes.push(nightMapType);
        } else {
            nightMapType.setOpacity(1);
            map.overlayMapTypes.clear();
            map.setMapTypeId('night');
        }
    });
}

function addMarkers() {
    var markers = [];
    var infowindow = new google.maps.InfoWindow({
        content: "contentString"
    });
    for (var i = 0; i < 100; i++) {
        var dataPhoto = data.photos[i];
        var latLng = new google.maps.LatLng(dataPhoto.latitude,
            dataPhoto.longitude);
        var marker = new google.maps.Marker({
            position: latLng,
            icon: 'img/star_40.png'
        });
        google.maps.event.addListener(marker, 'click', function () {
            infowindow.open(map, this);
        });
        markers.push(marker);
    }
    fitMarkersOnMap(markers);
    var styles = [{
        height: 70,
        url: "img/star_80.png",
        width: 70,
        textSize: 1
        }];
    var markerCluster = new MarkerClusterer(map, markers, {
        styles: styles
    });
}

function fitMarkersOnMap(markers) {
    var bounds = new google.maps.LatLngBounds();
    for (i = 0; i < markers.length; i++) {
        bounds.extend(markers[i].getPosition());
    }

    map.fitBounds(bounds);
}

function padTileCoordinate(number, zoomlevel) {
    switch (Math.floor(Math.log(Math.pow(2, zoomlevel)) / Math.log(10)) - Math.floor(Math.log(number) / Math.log(10))) {
    case 3:
        return '000' + number;
    case 2:
        return '00' + number;
    case 1:
        return '0' + number;
    default:
        return number;
    }
}

function getNormalizedCoord(tile, zoom) {
    var tileRange = 1 << zoom;
    if (tile.y < 0 || tile.y >= tileRange)
        return null;
    return {
        x: (tile.x % tileRange + tileRange) % tileRange,
        y: tile.y
    };
}