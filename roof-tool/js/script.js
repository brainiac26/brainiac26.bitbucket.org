(function () {

    var map,
        drawingManager,
        isDrawing = false,
        isMeasuring = false,
        overlays = [],
        measureTool,
        place,
        selectedShape,
        autocomplete,
        $draw, $ruler, $area, $sqm;

    google.maps.event.addDomListener(window, 'load', initialize);

    function initialize() {
        $draw = document.getElementById('draw');
        $ruler = document.getElementById('ruler');
        $area = document.getElementById('area');
        $sqm = document.getElementById('sqm');

        $area.style.display = 'none';

        map = new google.maps.Map(document.getElementById('map'), {
            center: new google.maps.LatLng(36.2398864, -113.7644643),
            zoom: 6,
            mapTypeId: google.maps.MapTypeId.SATELLITE,
            rotateControl: false,
            streetViewControl: false,
            mapTypeControl: false
        });

        autocomplete = new google.maps.places.Autocomplete(document.getElementById('address-search'));

        autocomplete.addListener('place_changed', function () {
            place = autocomplete.getPlace();

            map.setCenter(place.geometry.location);
            map.setZoom(20);
        });

        drawingManager = new google.maps.drawing.DrawingManager(
            {
                drawingControl: false,
                drawingMode: google.maps.drawing.OverlayType.POLYGON,
                polygonOptions: {
                    fillColor: '#eee',
                    strokeColor: '#000',
                    fillOpacity: 0.7,
                    strokeWeight: 1
                }
            }
        );

        measureTool = new MeasureTool(map, {
            contextMenu: false
        });

        google.maps.event.addListener(drawingManager, 'overlaycomplete', function (e) {
            var newShape = e.overlay;
            overlays.push(e);
            // google.maps.event.addListener(newShape, 'click', function () {
            //     setSelection(newShape);
            // });
            // setSelection(newShape);
            $area.style.display = 'block';
            $sqm.innerHTML = getSqm();
        });

        $draw.addEventListener('click', function() {
            toggleDrawing();
        })

        $ruler.addEventListener('click', function() {
            toggleMeasuring();
        })
    }

    function getSqm() {
        var areas = [],
            totalArea = 0;

        for (var i = 0, l = overlays.length; i < l; i++) {
            var overlay = overlays[i],
                area = getPolygonArea(overlay);
            totalArea += area;
            areas.push(area);
        }
        return totalArea;
    }

    function getPolygonArea(polygon) {
        return google.maps.geometry.spherical.computeArea(polygon.overlay.getPath());
    }

    function toggleDrawing() {
        isDrawing = !isDrawing;
        if (isDrawing) {
            $draw.innerHTML = 'Stop drawing polygon';
            if (isMeasuring) {
                toggleMeasuring();
            }
            drawingManager.setMap(map);
        } else {
            $draw.innerHTML = 'Draw new polygon';
            drawingManager.setMap(null);
        }
    }

    function toggleMeasuring() {
        isMeasuring = !isMeasuring;
        if (isMeasuring) {
            $ruler.innerHTML = 'Stop measurement';
            if (isDrawing) {
                toggleDrawing();
            }
            measureTool.start();
        } else {
            $ruler.innerHTML = 'Ruler to measure distance between points';
            measureTool.end();
        }
    }

})();

