/*****   Define const letiables   ******/
const HAND_MODE = 0;
const MARKER_MODE = 1;
const POLYGON_MODE = 2;
const LINE_MODE = 3;
const RECTANGLE_MODE = 4;
const CIRCLE_MODE = 5;
const LABEL_MODE = 6;
const MEASURE_MODE = 7;
const MOVE_MODE = 8;
const RESIZE_MODE = 9;
const MODE_LABEL = ["Hand", "Marker", "Polygon", "Line", "Rectangle", "Circle", "Label", "Measure", "Move", "Resize"];
const SET_TYPE = 0;
const EDIT_TYPE = 1;
const LABEL_FONT_SIZE = 18;
const MARKER_LABEL_FONT_SIZE = 16;

/*****    Define global letiables  *****/
let cur_mode = HAND_MODE;
let cur_options = [
    {name: "", color: "#ff0000"},
    {name: "", color: "#ff0000"},
    {name: "", color: "#ff0000"},
    {name: "", color: "#ff0000"},
    {name: "", color: "#ff0000"},
    {name: "", color: "#ff0000"},
    {name: "", color: "#ff0000"},
    {name: "", color: "#ff0000"},
];
let marker_array;
let marker_id;
let shape_array;
let shape_id;
let label_array;
let label_id;
let measure_array;
let measure_id;

let showMarkers = true;
let showShapes = true;
let showLabels = true;
let showMeasurements = true;

/******* letiables for Map  ***********/
let map = null;
let drawingManager;
jq2 = jQuery.noConflict();

/******** letiables for measure tool ******/
let isMeasureStatus = 0; // 0: start 1: dragging 2: end
let measureInfoWnd;

/******** dom elements ******/

let $showPoints;
let $showLengths;
let $toggleButtons;


function addOverlays(overlays) {
    overlays.forEach(function(overlay) {
        switch (overlay.type) {
            case 'polyline':
                const polyline = new google.maps.Polyline(overlay.data);
                polyline.setMap(map);
                cur_mode = LINE_MODE;
                handleShapeOverlay(polyline, overlay.name, overlay.color, overlay.type);
                break;
            case 'polygon':
                const polygon = new google.maps.Polygon({...overlay.data, editable: true});
                polygon.setMap(map);
                cur_mode = POLYGON_MODE;
                handleShapeOverlay(polygon, overlay.name, overlay.color, overlay.type);
                break;
            case 'rectangle':
                const rectangle = new google.maps.Rectangle(overlay.data);
                rectangle.setMap(map);
                cur_mode = RECTANGLE_MODE;
                handleShapeOverlay(rectangle, overlay.name, overlay.color, overlay.type);
                break;
            case 'circle':
                const circle = new google.maps.Circle(overlay.data);
                circle.setMap(map);
                handleShapeOverlay(circle, overlay.name, overlay.color, overlay.type);
                break;
            case 'marker':
                const marker = new google.maps.Marker(overlay.data);
                marker.setMap(map);
                cur_mode = MARKER_MODE;
                handleMarkerOverlay(marker, overlay.name, overlay.color, overlay.type);
                break;
            case 'label':
                const label = new google.maps.Marker(Object.assign({}, overlay.data, {id: label_id}));
                label.setMap(map);
                cur_mode = LABEL_MODE;
                handleLabelOverlay(label, overlay.name, overlay.color, overlay.type);
                break;
            case 'measure_polyline':
                const polylinePath = overlay.data.path;
                cur_mode = MEASURE_MODE;
                polylinePath.forEach(function(node) {
                    addMeasureNode(new google.maps.LatLng(node), overlay.name, overlay.color);
                });
                break;
            case 'measure_polygon':
                const polygonPath = overlay.data.path;
                cur_mode = MEASURE_MODE;
                polygonPath.forEach(function(node) {
                    addMeasureNode(new google.maps.LatLng(node), overlay.name, overlay.color);
                });
                createMeasurePolygon(measure_array[measure_array.length - 1].tool, overlay.color);
                break;
        }
    })
}

function addMeasureNode(latLng, name, color) {
    if (isMeasureStatus == 0) {
        let meLine = new google.maps.Polyline({
            id: measure_id,
            map: map,
            path: [],
            strokeColor: color,
            strokeOpacity: 1.0,
            strokeWeight: 2,
            clickable: true,
            draggable: true,
            editable: true,
            zIndex: MEASURE_MODE
        });

        google.maps.event.addListener(meLine.getPath(), 'set_at', function (e) {
            let paths = meLine.getPath();
            updateMeasureLine(meLine.id, paths, getPolyType(meLine.id));
        });

        google.maps.event.addListener(meLine.getPath(), 'insert_at', function () {
            let paths = meLine.getPath();
            updateMeasureLine(meLine.id, paths, getPolyType(meLine.id));
        });

        google.maps.event.addListener(meLine, 'mousemove', function (event) {
            let cal = calculateMeasure(this.id, this.getPath());
            if (cal) {
                let appendHtml = 'distance : <span class="measure-distance">' + cal[0] + '</span> linear feet <br>'
                  + 'area : <span class="measure-area">' + cal[1] + '</span> square feet <br>' +
                  '<span>Roof squares: ' + cal[2] + '</span> ';

                if (measureInfoWnd) {
                    measureInfoWnd.close();
                }
                measureInfoWnd = new google.maps.InfoWindow({
                    content: appendHtml
                });

                let latLng = event.latLng;
                measureInfoWnd.setPosition(latLng);
                measureInfoWnd.open(map);
            }
        });

        google.maps.event.addListener(meLine, 'mouseout', function () {
            if (measureInfoWnd) {
                measureInfoWnd.close();
            }
        });

        google.maps.event.addListener(meLine, 'mousedown', function (event) {
            if (event.vertex === 0) {
                createMeasurePolygon(this, color);
            }
        });

        let sMeasure = new Tool(meLine, name, color, 'measure_polyline');
        measure_array.push(sMeasure);

        let appendHtml = '<li><input type="hidden" class="option-id" value="' + measure_id + '">'
          + '<input type="hidden" class="option-color" value="' + color + '">'
          + '<span class="name-span">' + name + '</span> '
          + '<span class="color-span" style="background-color:' + color + '">&nbsp; &nbsp; &nbsp; &nbsp;</span>'
          + '<a href="#" onclick="onEditMeasure(this);" data-toggle="modal" data-target="#set-options-modal"><i class="fa fa-edit" aria-hidden="true"></i></a>'
          + '<a href="#" onclick="onDeleteMeasure(this);"><i class="fa fa-trash" aria-hidden="true"></i></a>'
          + '<a href="#" class="btn-visibility" onclick="onToggleVisibleMeasurements(this);"><i class="fa fa-eye" aria-hidden="true"></i></a>'
          + '<br>'
          + 'Measurements:<br>'
          + 'distance : <span class="measure-distance">0.000</span> linear feet <br>'
          + 'area : <span class="measure-area">0.000</span> square feet <br>'
          + 'Roof squares : <span class="measure-roof">0.000</span><br>'
          + '<div style="display:flex" class="measure-points-wrapper"><span style="width:25%">points: </span><span class="measure-points"></span></div> <br>'
          + '<div style="display:flex" class="measure-lengths-wrapper"><span style="width:25%">lengths: </span><span class="measure-lengths"></span></div> <br></li>';

        jq2('.measure-list').append(appendHtml);
        isMeasureStatus = 1;
    }

    let measureLine = measure_array[measure_array.length - 1].tool;
    measureLine.getPath().push(latLng);
    measure_array[measure_array.length - 1].tool = measureLine;

    if (isMeasureStatus == 2) {
        measure_id++;
        isMeasureStatus = 0;
    }
}

function calculateDistance(point1, point2) {
    return google.maps.geometry.spherical.computeDistanceBetween(point1, point2) * 3.28084;
}

/**
 * calculate area and length by measure tool
 * @param paths : measure points
 */
function calculateMeasure(mLineId, paths) {
    let ret = [];
    if (paths.length > 1) {
        let len = google.maps.geometry.spherical.computeLength(paths) / 0.3048;
        len = len.toFixed(3);

        let square = google.maps.geometry.spherical.computeArea(paths) / 0.3048 / 0.3048;
        square = square.toFixed(3);

        let roofSq = Math.round(square / 100);

        let li = jq2(".measure-list").find('li input[value=' + mLineId + '].option-id').parent();

        li.find('.measure-distance').text(len);
        li.find('.measure-area').text(square);
        li.find('.measure-roof').text(roofSq);
        ret.push(len);
        ret.push(square);
        ret.push(roofSq);
        return ret;
    }
}

function calculateInchPerPx() {
    return 156543.03392 * Math.cos(map.getCenter().lat() * Math.PI / 180) / Math.pow(2, map.getZoom()) * 39.3701;
}

/**
 * Create Maker with color param
 * @param  color : Marker color
 */
function createMarkerSymbol(color) {
    return {
        path: 'M 0,0 C -2,-20 -10,-22 -10,-30 A 10,10 0 1,1 10,-30 C 10,-22 2,-20 0,0 z',
        fillColor: color,
        fillOpacity: 1,
        strokeColor: '#e7e7e7',
        strokeWeight: 1.5,
        scale: 1,
        labelOrigin: new google.maps.Point(0, 10)
    };
}

/**
 * Create Label with color param
 * @param  color : Label text color
 */
function createLabelSymbol(color) {
    return {
        path: 'M 0,0 C -2,-20 -10,-22 -10,-30 A 10,10 0 1,1 10,-30 C 10,-22 2,-20 0,0 z',
        fillColor: "#ffffff",
        fillOpacity: 0,
        strokeColor: color,
        strokeWeight: 0,
        scale: 1,
        labelOrigin: new google.maps.Point(0, 0)
    };
}

function createMeasurePolygon(polyline, color) {
    polyline.setMap(null);

    let mePolygon = new google.maps.Polygon({
        id: polyline.id,
        map: map,
        path: polyline.getPath(),
        fillOpacity: 0,
        strokeColor: color,
        strokeWeight: 2,
        clickable: true,
        editable: true,
        zIndex: MEASURE_MODE
    });

    google.maps.event.addListener(mePolygon, 'mousemove', function (event) {
        let cal = calculateMeasure(this.id, this.getPath());
        let appendHtml = 'distance : <span class="measure-distance">' + cal[0]
          + '</span> linear feet <br>'
          + 'area : <span class="measure-area">' + cal[1] + '</span> square feet <br>'
          + '<span>Roof squares: ' + cal[2] + '</span> ';

        if (measureInfoWnd) {
            measureInfoWnd.close();
        }
        measureInfoWnd = new google.maps.InfoWindow({
            content: appendHtml
        });

        let latLng = event.latLng;
        measureInfoWnd.setPosition(latLng);
        measureInfoWnd.open(map);
    });

    google.maps.event.addListener(mePolygon, 'mouseout', function () {
        measureInfoWnd.close();
    });

    measure_array[measure_array.length - 1].tool = mePolygon;
    measure_array[measure_array.length - 1].type = 'measure_polygon';
    updateMeasureLine(mePolygon.id, mePolygon.getPath(), getPolyType(mePolygon.id));
    measure_id++;
    isMeasureStatus = 0;
}

function getMapParams() {
    const zoom = map.getZoom();
    const center = map.getCenter();
    const overlays = [];

    shape_array.concat(marker_array).concat(label_array).concat(measure_array).forEach(function(el) {
        let elParams = {};
        if (!el.visible) return;

        elParams.type = el.type;
        elParams.name = el.name;
        elParams.color = el.color;
        elParams.data = {};
        switch(el.type) {
            case 'polyline':
                getShapeParams(elParams.data, el.tool);
                elParams.data.path = el.tool.getPath().getArray();
                break;
            case 'polygon':
                getShapeParams(elParams.data, el.tool);
                elParams.data.paths = el.tool.getPath().getArray();
                break;
            case 'rectangle':
                getShapeParams(elParams.data, el.tool);
                elParams.data.bounds = el.tool.getBounds();
                break;
            case 'circle':
                getShapeParams(elParams.data, el.tool);
                elParams.data.radius = el.tool.getRadius();
                elParams.data.center = el.tool.getCenter();
                break;
            case 'marker':
            case 'label':
                elParams.data.position = el.tool.getPosition();
                elParams.data.icon = el.tool.getIcon();
                elParams.data.label = el.tool.getLabel();
                break;
            case 'measure_polygon':
            case 'measure_polyline':
                elParams.data.path = el.tool.getPath().getArray();
                break;
        }

        overlays.push(elParams);
    });

    return {
        zoom: zoom,
        center: center,
        overlays: overlays
    }
}

function getShapeParams(params, tool) {
    params.fillColor = tool.get('fillColor');
    params.strokeColor = tool.get('strokeColor');
    params.strokeWeight = tool.get('strokeWeight');
    params.fillOpacity = tool.get('fillOpacity');
    params.strokeOpacity = tool.get('strokeOpacity');
}

function handleShapeOverlay(overlay, name, color, type) {
    let appendHtml = '<li><input type="hidden" class="option-id" value="' + shape_id + '"><input type="hidden" class="option-mode" value="' + cur_mode + '"><input type="hidden" class="option-color" value="' + color + '"><span  class="name-span">' + name + '</span><span class="shape-span" style="display:none;">' + event.type + '</span> <span class="color-span" style="background-color:' + color + '">&nbsp; &nbsp; &nbsp; &nbsp;</span><a href="#" class="btn-edit-option" onclick="onEditShape(this);" data-toggle="modal" data-target="#set-options-modal"><i class="fa fa-edit" aria-hidden="true"></i></a><a href="#" onclick="onDeleteShape(this);"><i class="fa fa-trash" aria-hidden="true"></i></a><a href="#"  class="btn-visibility '+ (showShapes ? "" : "inactive") +'" onclick="onToggleVisibleShapes(this);"><i class="fa fa-eye" aria-hidden="true"></i></a></li>';
    jq2('.shape-list').append(appendHtml);

    overlay.id = shape_id;
    let cOverlay = new Tool(overlay, name, color, type, showShapes);
    shape_array.push(cOverlay);
    shape_id++;
}

function handleMarkerOverlay(overlay, name, color, type) {
    let appendHtml = '<li><input type="hidden" class="option-id" value="' + marker_id + '"><input type="hidden" class="option-color" value="' + color + '"><span class="name-span">' + name + '</span> <span class="color-span" style="background-color:' + color + '">&nbsp; &nbsp; &nbsp; &nbsp;</span><a href="#" class="btn-edit-option" onclick="onEditMarker(this);" data-toggle="modal" data-target="#set-options-modal"><i class="fa fa-edit" aria-hidden="true"></i></a><a href="#" onclick="onDeleteMarker(this);"><i class="fa fa-trash" aria-hidden="true"></i></a><a href="#" class="btn-visibility" onclick="onToggleVisibleMarkers(this);"><i class="fa fa-eye" aria-hidden="true"></i></a></li>';
    jq2('.marker-list').append(appendHtml);

    overlay.id = marker_id;
    let cMarker = new Tool(overlay, name, color, type);
    marker_array.push(cMarker);
    marker_id++;
}

function handleLabelOverlay(overlay, name, color, type) {
    let appendHtml = '<li><input type="hidden" class="option-id" value="' + label_id + '"><input type="hidden" class="option-color" value="' + color + '"><span class="name-span">' + name + '</span> <span class="color-span" style="background-color:' + color + '">&nbsp; &nbsp; &nbsp; &nbsp;</span><a href="#" class="btn-edit-option" onclick="onEditLabel(this);" data-toggle="modal" data-target="#set-options-modal"><i class="fa fa-edit" aria-hidden="true"></i></a><a href="#" onclick="onDeleteLabel(this);"><i class="fa fa-trash" aria-hidden="true"></i></a><a href="#" class="btn-visibility" onclick="onToggleVisibleLabels(this);"><i class="fa fa-eye" aria-hidden="true"></i></a></li>';
    jq2('.label-list').append(appendHtml);

    let cLabel = new Tool(overlay, name, color, type);
    label_array.push(cLabel);
    label_id++;
}

function isShowPoints() {
    return $showPoints.is(':checked');
}

function isShowLengths() {
    return $showLengths.is(':checked');
}

function getPolyType(id) {
    let type;
    for (let i = 0; i < measure_array.length; i++) {
        if (measure_array[i].tool.id == id) {
            type = measure_array[i].type;
            break;
        }
    }
    return type;
}

/**
 * Initialize Map
 */
function initMap(data = {}) {

    marker_array = [];
    marker_id = 0;
    shape_array = [];
    shape_id = 0;
    label_array = [];
    label_id = 0;
    measure_array = [];
    measure_id = 0;

    jq2('.shape-list').html('');
    jq2('.marker-list').html('');
    jq2('.label-list').html('');
    jq2('.measure-list').html('');

    const location = data.center || {lat: 36.2251093, lng: -94.20202171};
    const zoom = data.zoom || 10;

    map = new google.maps.Map(document.getElementById('map'), {
        zoom: zoom,
        mapTypeId: "roadmap",
        tilt: 0,
        center: location,

        scaleControl: false,
        streetViewControl: false,
        rotateControl: false,
        zoomControl: true,
        zoomControlOptions: {
            position: google.maps.ControlPosition.LEFT_CENTER
        },
    });

    if (data.overlays) {
        addOverlays(data.overlays);
    }

    drawingManager = new google.maps.drawing.DrawingManager({drawingControl: true});
    drawingManager.setMap(map);

    google.maps.event.addListener(drawingManager, 'overlaycomplete', function (event) {
        let gOverlay = event.overlay;
        let name = cur_options[cur_mode].name;
        let color = cur_options[cur_mode].color;

        if (event.type == 'marker') {
            handleMarkerOverlay(gOverlay, name, color, event.type);
        }
        if (event.type == 'circle' || event.type == 'rectangle' || event.type == 'polyline' || event.type == 'polygon') {
            handleShapeOverlay(gOverlay, name, color, event.type);
        }
    });

    /**
     *  Click event on Map for label tool
     */
    map.addListener('click', function (args) {

        if (cur_mode == LABEL_MODE) {
            let name = cur_options[cur_mode].name;
            let color = cur_options[cur_mode].color;

            let gLabel = new google.maps.Marker({
                id: label_id,
                position: args.latLng,
                label: {
                    text: name == "" ? " " : name,
                    color: color,
                    fontSize: LABEL_FONT_SIZE + "px",
                    fontWeight: "bold"
                },
                icon: createLabelSymbol(color),
                map: map
            });

            handleLabelOverlay(gLabel, name, color, 'label');
        }

        if (cur_mode == MEASURE_MODE) {
            setTimeout(function () {
                let name = cur_options[cur_mode].name;
                let color = cur_options[cur_mode].color;
                addMeasureNode(args.latLng, name, color);
            }, 300);
        }
    });

    /**
     * double click event on map when using measure tool
     */
    map.addListener('dblclick', function () {
        if (cur_mode == MEASURE_MODE && isMeasureStatus != 0) {
            isMeasureStatus = 2;
        }
    });

    /**
     * close infowindow for measure when mouse moving
     */
    map.addListener('mousemove', function () {
        if (measureInfoWnd && cur_mode == MEASURE_MODE) {
            measureInfoWnd.close();
        }
    });

    return true;
}

/**
 * update measure line values
 */
function updateMeasureLine(mLineId, paths, type) {
    calculateMeasure(mLineId, paths);
    let li = jq2(".measure-list").find('li input[value=' + mLineId + '].option-id').parent();
    if (isShowPoints()) {
        li.find('.measure-points').empty();
        for (let i = 0; i < paths.length; i++) {
            li.find('.measure-points').append("(" + paths.b[i].lat() + ", " + paths.b[i].lng() + ")");
        }
    }

    if (isShowLengths() && paths.length > 1) {
        const $lengthsList = li.find('.measure-lengths');
        let lengths = '';

        for (let i = 1; i < paths.length; i++) {
            lengths += Math.round(calculateDistance(paths.b[i-1], paths.b[i])) + '<br/>';
        }

        if (type === 'measure_polygon') {
            lengths += Math.round(calculateDistance(paths.b[0], paths.b[paths.length - 1])) + '<br/>';
        }

        $lengthsList.html(lengths);
    }
}

/**
 * Set Name & Color options for each tool
 */
function setDrawingOptions() {
    let name = jq2("#modal-name").val();
    let color = jq2("#modal-color").val();
    let type = jq2("#modal-type").val();
    let mode = jq2("#modal-mode").val();
    let id = jq2("#modal-id").val();
    if (type == SET_TYPE) {
        jq2('#menu').find('.btn-mode').removeClass('mode-selected');
        jq2('#menu').find('.btn-mode').eq(mode).addClass('mode-selected');
        cur_mode = mode;
        cur_options[cur_mode].name = name;
        cur_options[cur_mode].color = color;
        map.setOptions({disableDoubleClickZoom: false, clickableIcons: true});
        if (mode == MARKER_MODE) {
            drawingManager.setOptions({
                drawingMode: google.maps.drawing.OverlayType.MARKER,
                markerOptions: {
                    label: {
                        text: name == "" ? " " : name,
                        color: color,
                        fontSize: MARKER_LABEL_FONT_SIZE + "px",
                        fontWeight: "bold"
                    },
                    icon: createMarkerSymbol(color),
                }
            });
        }
        if (mode == MEASURE_MODE) {
            map.setOptions({disableDoubleClickZoom: true, clickableIcons: false, draggableCursor: 'crosshair'});
            drawingManager.setOptions({
                drawingMode: google.maps.drawing.OverlayType.HAND
            });
        }
        if (mode == POLYGON_MODE) {
            drawingManager.setOptions({
                drawingMode: google.maps.drawing.OverlayType.POLYGON,
                polygonOptions: {
                    fillColor: color,
                    strokeColor: color,
                    strokeWeight: 2,
                    clickable: true,
                    draggable: true,
                    editable: true,
                    zIndex: POLYGON_MODE
                }
            });
        }
        if (mode == LINE_MODE) {
            drawingManager.setOptions({
                drawingMode: google.maps.drawing.OverlayType.POLYLINE,
                polylineOptions: {
                    strokeColor: color,
                    strokeWeight: 2,
                    clickable: true,
                    editable: true,
                    zIndex: LINE_MODE
                }
            });
        }
        if (mode == RECTANGLE_MODE) {
            drawingManager.setOptions({
                drawingMode: google.maps.drawing.OverlayType.RECTANGLE,
                rectangleOptions: {
                    fillColor: color,
                    strokeColor: color,
                    strokeWeight: 2,
                    clickable: true,
                    zIndex: RECTANGLE_MODE
                }
            });
        }
        if (mode == CIRCLE_MODE) {
            drawingManager.setOptions({
                drawingMode: google.maps.drawing.OverlayType.CIRCLE,
                circleOptions: {
                    fillColor: color,
                    strokeColor: color,
                    strokeWeight: 2,
                    clickable: true,
                    zIndex: CIRCLE_MODE
                }
            });
        }
        if (mode == LABEL_MODE) {
            drawingManager.setOptions({
                drawingMode: google.maps.drawing.OverlayType.HAND
            });
        }
    } else if (type == EDIT_TYPE) {
        if (mode == MARKER_MODE) {
            let li = jq2(".marker-list").find('li input[value=' + id + '].option-id').parent();
            li.find('.name-span').text(name);
            li.find('.color-span').css('background-color', color);
            li.find('.option-color').val(color);
            marker_array.forEach(function (obj) {
                if (obj.tool.id == id) {
                    obj.tool.label.text = name;
                    obj.tool.label.color = color;
                    obj.tool.icon = createMarkerSymbol(color);
                    obj.tool.setMap(map);
                    obj.name = name;
                    obj.color = color;
                }
            });
        }
        if (mode == CIRCLE_MODE || mode == LINE_MODE || mode == POLYGON_MODE || mode == RECTANGLE_MODE) {
            let li = jq2(".shape-list").find('li input[value=' + id + '].option-id').parent();
            li.find('.name-span').text(name);
            li.find('.color-span').css('background-color', color);
            li.find('.option-color').val(color);
            shape_array.forEach(function (obj) {
                if (obj.tool.id == id) {
                    obj.tool.setMap(null);

                    let tmpOverlay;
                    if (mode == CIRCLE_MODE) {
                        tmpOverlay = new google.maps.Circle({
                            id: obj.tool.id,
                            fillColor: color,
                            strokeColor: color,
                            strokeWeight: 2,
                            clickable: false,
                            zIndex: CIRCLE_MODE,
                            map: map,
                            center: obj.tool.getCenter(),
                            radius: obj.tool.getRadius()
                        });
                    }
                    if (mode == LINE_MODE) {
                        tmpOverlay = new google.maps.Polyline({
                            id: obj.tool.id,
                            strokeColor: color,
                            strokeWeight: 2,
                            clickable: false,
                            zIndex: LINE_MODE,
                            map: map,
                            path: obj.tool.getPath()
                        });
                    }
                    if (mode == POLYGON_MODE) {
                        tmpOverlay = new google.maps.Polygon({
                            id: obj.tool.id,
                            fillColor: color,
                            strokeColor: color,
                            strokeWeight: 2,
                            clickable: true,
                            draggable: true,
                            editable: true,
                            zIndex: POLYGON_MODE,
                            map: map,
                            paths: obj.tool.getPaths()
                        });
                    }
                    if (mode == RECTANGLE_MODE) {
                        tmpOverlay = new google.maps.Rectangle({
                            id: obj.tool.id,
                            fillColor: color,
                            strokeColor: color,
                            strokeWeight: 2,
                            clickable: false,
                            zIndex: RECTANGLE_MODE,
                            map: map,
                            bounds: obj.tool.getBounds()
                        });
                    }
                    obj.name = name;
                    obj.color = color;
                    obj.tool = tmpOverlay;
                }
            });
        }
        if (mode == LABEL_MODE) {
            let li = jq2(".label-list").find('li input[value=' + id + '].option-id').parent();
            li.find('.name-span').text(name);
            li.find('.color-span').css('background-color', color);
            li.find('.option-color').val(color);
            label_array.forEach(function (obj) {
                if (obj.tool.id == id) {
                    obj.tool.label.text = name;
                    obj.tool.label.color = color;
                    obj.tool.icon = createLabelSymbol(color);
                    obj.tool.setMap(map);
                    obj.name = name;
                    obj.color = color;
                }
            });
        }
        if (mode == MEASURE_MODE) {
            let li = jq2(".measure-list").find('li input[value=' + id + '].option-id').parent();
            li.find('.name-span').text(name);
            li.find('.color-span').css('background-color', color);
            li.find('.option-color').val(color);
            measure_array.forEach(function (obj) {
                if (obj.tool.id == id) {
                    obj.tool.set('strokeColor', color);
                    obj.tool.setMap(map);
                    obj.name = name;
                    obj.color = color;
                }
            });

        }
    }
    jq2('#set-options-modal').find('.close').trigger('click');
}

/**
 * Event when you fire edit button to marker
 */
function onEditMarker(handle) {
    let name = jq2(handle).parent().find('.name-span').text();
    let color = jq2(handle).parent().find('.option-color').val();
    let id = jq2(handle).parent().find('.option-id').val();
    jq2("#modal-name").val(name);
    jq2("#modal-name").focus();
    jq2("#modal-color").val(color);
    jq2("#name-input").text(MODE_LABEL[MARKER_MODE] + " name");
    jq2("#color-input").text(MODE_LABEL[MARKER_MODE] + " color");
    jq2('#modal-type').val(EDIT_TYPE);
    jq2('#modal-mode').val(MARKER_MODE);
    jq2('#modal-id').val(id);
};

/**
 * Event when you fire delete button to marker
 */
function onDeleteMarker(handle) {
    let id = jq2(handle).parent().find('.option-id').val();
    jq2(handle).parent().remove();
    for (let i = 0; i < marker_array.length; i++) {
        if (marker_array[i].tool.id == id) {
            marker_array[i].tool.setMap(null);
            marker_array.splice(i, 1);
            break;
        }
    }
};

/**
 * Event when you fire edit button to shape
 */
function onEditShape(handle) {
    let name = jq2(handle).parent().find('.name-span').text();
    let color = jq2(handle).parent().find('.option-color').val();
    let id = jq2(handle).parent().find('.option-id').val();
    let mode = parseInt(jq2(handle).parent().find('.option-mode').val());
    jq2("#modal-name").val(name);
    jq2("#modal-name").focus();
    jq2("#modal-color").val(color);
    jq2("#name-input").text(MODE_LABEL[mode] + " name");
    jq2("#color-input").text(MODE_LABEL[mode] + " color");
    jq2('#modal-type').val(EDIT_TYPE);
    jq2('#modal-mode').val(mode);
    jq2('#modal-id').val(id);
};

/**
 * Event when you fire delete button to shape
 */
function onDeleteShape(handle) {
    let id = jq2(handle).parent().find('.option-id').val();
    jq2(handle).parent().remove();
    for (let i = 0; i < shape_array.length; i++) {
        if (shape_array[i].tool.id == id) {
            shape_array[i].tool.setMap(null);
            shape_array.splice(i, 1);
            break;
        }
    }
};

/**
 * Event when you fire edit button to label
 */
function onEditLabel(handle) {
    let name = jq2(handle).parent().find('.name-span').text();
    let color = jq2(handle).parent().find('.option-color').val();
    let id = jq2(handle).parent().find('.option-id').val();
    jq2("#modal-name").val(name);
    jq2("#modal-name").focus();
    jq2("#modal-color").val(color);
    jq2("#name-input").text(MODE_LABEL[LABEL_MODE] + " name");
    jq2("#color-input").text(MODE_LABEL[LABEL_MODE] + " color");
    jq2('#modal-type').val(EDIT_TYPE);
    jq2('#modal-mode').val(LABEL_MODE);
    jq2('#modal-id').val(id);
};

/**
 * Event when you fire delete button to label
 */
function onDeleteLabel(handle) {
    let id = jq2(handle).parent().find('.option-id').val();
    jq2(handle).parent().remove();
    for (let i = 0; i < label_array.length; i++) {
        if (label_array[i].tool.id == id) {
            label_array[i].tool.setMap(null);
            label_array.splice(i, 1);
            break;
        }
    }
};

/**
 * Event when you fire edit button to measure
 */
function onEditMeasure(handle) {
    let name = jq2(handle).parent().find('.name-span').text();
    let color = jq2(handle).parent().find('.option-color').val();
    let id = jq2(handle).parent().find('.option-id').val();
    jq2("#modal-name").val(name);
    jq2("#modal-name").focus();
    jq2("#modal-color").val(color);
    jq2("#name-input").text(MODE_LABEL[MEASURE_MODE] + " name");
    jq2("#color-input").text(MODE_LABEL[MEASURE_MODE] + " color");
    jq2('#modal-type').val(EDIT_TYPE);
    jq2('#modal-mode').val(MEASURE_MODE);
    jq2('#modal-id').val(id);
};

/**
 * Event when you fire delete button to measure
 */
function onDeleteMeasure(handle) {
    let id = jq2(handle).parent().find('.option-id').val();
    jq2(handle).parent().remove();
    for (let i = 0; i < measure_array.length; i++) {
        if (measure_array[i].tool.id == id) {
            measure_array[i].tool.setMap(null);
            if (i == measure_array.length - 1) {
                isMeasureStatus = 0;
            }
            measure_array.splice(i, 1);
            break;
        }
    }
};

function onToggleVisibleMarkers(handle, visible) {
    let $parent = jq2(handle).parent();
    let id = $parent.find('.option-id').val();
    let $btnVisibility = $parent.find('.btn-visibility');
    for (let i = 0; i < marker_array.length; i++) {
        if (marker_array[i].tool.id == id) {
            const isVisible = visible || !marker_array[i].visible;
            marker_array[i].visible = isVisible;
            marker_array[i].tool.setMap(isVisible ? map : null);
            $btnVisibility.toggleClass('inactive', !isVisible);
            break;
        }
    }
}

function onToggleVisibleShapes(handle, visible) {
    let $parent = jq2(handle).parent();
    let id = $parent.find('.option-id').val();
    let $btnVisibility = $parent.find('.btn-visibility');
    for (let i = 0; i < shape_array.length; i++) {
        if (shape_array[i].tool.id == id) {
            const isVisible = typeof visible === 'undefined' ? !shape_array[i].visible : visible;
            shape_array[i].visible = isVisible;
            shape_array[i].tool.setMap(isVisible ? map : null);
            $btnVisibility.toggleClass('inactive', !isVisible);
            break;
        }
    }
}

function onToggleVisibleLabels(handle, visible) {
    let $parent = jq2(handle).parent();
    let id = $parent.find('.option-id').val();
    let $btnVisibility = $parent.find('.btn-visibility');
    for (let i = 0; i < label_array.length; i++) {
        if (label_array[i].tool.id == id) {
            const isVisible = visible || !label_array[i].visible;
            label_array[i].visible = isVisible;
            label_array[i].tool.setMap(isVisible ? map : null);
            $btnVisibility.toggleClass('inactive', !isVisible);
            break;
        }
    }
}

function onToggleVisibleMeasurements(handle, visible) {
    let $parent = jq2(handle).parent();
    let id = $parent.find('.option-id').val();
    let $btnVisibility = $parent.find('.btn-visibility');
    for (let i = 0; i < measure_array.length; i++) {
        if (measure_array[i].tool.id == id) {
            const isVisible = visible || !measure_array[i].visible;
            measure_array[i].visible = isVisible;
            measure_array[i].tool.setMap(isVisible ? map : null);
            $btnVisibility.toggleClass('inactive', !isVisible);
            break;
        }
    }
}

function toggleDraggable(draggable) {
    shape_array.concat(marker_array).concat(label_array).concat(measure_array).forEach(function(el) {
        el.tool.setDraggable(draggable);
    });
};

function toggleResize(resizable) {
    shape_array.concat(measure_array).forEach(function(el) {
        el.tool.setEditable(resizable);
    });
}

function saveJSON(data, filename){
    if(!data) {
        return;
    }
    if(!filename) filename = 'console.json';

    if (typeof data === "object") {
        data = JSON.stringify(data, undefined, 4)
    }

    var blob = new Blob([data], {type: 'text/json'}),
      e = document.createEvent('MouseEvents'),
      a = document.createElement('a');

    a.download = filename;
    a.href = window.URL.createObjectURL(blob);
    a.dataset.downloadurl =  ['text/json', a.download, a.href].join(':');
    e.initMouseEvent('click', true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
    a.dispatchEvent(e);
};

jq2(function ($) {

    initMap();

    var autocomplete = new google.maps.places.Autocomplete(document.getElementById('autocomplete'));

    autocomplete.addListener('place_changed', onPlaceChanged);

    function onPlaceChanged() {
        var place = autocomplete.getPlace();
        map.setCenter(place.geometry.location)
    }

    $showPoints = $('.show-points');
    $showLengths = $('.show-lengths');
    $toggleButtons = $('.visibility-toggle');

    $('#panel-toggle').click(function () {
        $('#data-panel').slideToggle();
        let value = $(this).html();

        if (value == '[ hide ]') {
            $(this).html('[ show ]');
        } else {
            $(this).html('[ hide ]');
        }
    });

    /**
     * Change event when you change tool
     */
    $('.btn-mode').click(function () {
        let mode = jq2(this).parent().index();

        jq2('#menu').find('.btn-mode').removeClass('mode-selected');
        jq2('#menu').find('.btn-mode').eq(mode).addClass('mode-selected');

        if (mode == HAND_MODE) {
            map.setOptions({disableDoubleClickZoom: false, draggableCursor: null});
            drawingManager.setOptions({drawingMode: google.maps.drawing.OverlayType.HAND});

            toggleDraggable(false);
            toggleResize(false);

            return;
        }

        if (mode == MOVE_MODE) {
            map.setOptions({disableDoubleClickZoom: false, draggableCursor: null});
            drawingManager.setOptions({drawingMode: google.maps.drawing.OverlayType.HAND});

            toggleDraggable(true);
            toggleResize(false);

            return;
        }

        if (mode == RESIZE_MODE) {
            map.setOptions({disableDoubleClickZoom: false, draggableCursor: null});
            drawingManager.setOptions({drawingMode: google.maps.drawing.OverlayType.HAND});

            toggleResize(true);
            toggleDraggable(false);

            return;
        }

        $("#modal-name").val(cur_options[mode].name);
        $("#modal-name").focus();
        $("#modal-color").val(cur_options[mode].color);
        $("#name-input").text(MODE_LABEL[mode] + " name");
        $("#color-input").text(MODE_LABEL[mode] + " color");
        $('#modal-type').val(SET_TYPE);
        $('#modal-mode').val(mode);  //change mode
        $('#modal-id').val(0);
    });

    $('.save-map').click(function() {
        const mapParams = getMapParams();
        saveJSON(mapParams, 'map.json');
    });

    $('.open-map').click(function() {
        $('.file').click();
    });

    $('.file').change(function(e) {
        const files = e.target.files;

        if (files.length <= 0) {
            return false;
        }

        const fr = new FileReader();

        fr.onload = function(e) {
            const result = JSON.parse(e.target.result);
            initMap(result);
        };

        fr.readAsText(files.item(0));
    });

    $showPoints.on('change', function() {
        $('.measure-points-wrapper').toggle(isShowPoints());
    });

    $showLengths.on('change', function() {
        $('.measure-lengths-wrapper').toggle(isShowLengths());
    });

    $('.modal-form').on('submit', function(e) {
        setDrawingOptions();
        $('#set-options-modal').modal('hide');
        e.preventDefault();
    });

    $toggleButtons.on('click', function() {
        let $elements;
        switch ($(this).attr('type')) {
            case 'markers':
                $elements = $('.marker-list .btn-visibility');
                showMarkers = !showMarkers;
                $(this).toggleClass('inactive', !showMarkers);

                $elements.each(function() {
                    onToggleVisibleMarkers($(this), showMarkers);
                });

                break;
            case 'shapes':
                $elements = $('.shape-list .btn-visibility');
                showShapes = !showShapes;
                $(this).toggleClass('inactive', !showShapes);

                $elements.each(function() {
                    onToggleVisibleShapes($(this), showShapes);
                });

                break;
            case 'labels':
                $elements = $('.label-list .btn-visibility');
                showLabels = !showLabels;
                $(this).toggleClass('inactive', !showLabels);

                $elements.each(function() {
                    onToggleVisibleLabels($(this), showLabels);
                });

                break;
            case 'measurements':
                $elements = $('.measure-list .btn-visibility');
                showMeasurements = !showMeasurements;
                $(this).toggleClass('inactive', !showMeasurements);

                $elements.each(function() {
                    onToggleVisibleMeasurements($(this), showMeasurements);
                });

                break;
        }

    });

    $('.snap').on('click', function() {
        map.setOptions({
            fullscreenControl: false,
            mapTypeControl: false,
            zoomControl: false
        });

        const transform=$(".gm-style>div:first>div").css("transform");
        const comp=transform.split(",");
        const mapleft=parseFloat(comp[4]);
        const maptop=parseFloat(comp[5]);
        $(".gm-style>div:first>div").css({
            "transform":"none",
            "left":mapleft,
            "top":maptop
        });

        window.setTimeout(function() {
            html2canvas($("#map"), {
                useCORS: true,
                onrendered: function(canvas) {
                    const filename = '1px-' + calculateInchPerPx() + 'in--' + Date.now() + '.png';
                    canvas.toBlob(function(blob) {
                        saveAs(blob, filename);
                    });
                }
            });

            map.setOptions({
                fullscreenControl: true,
                mapTypeControl: true,
                zoomControl: true
            });
        }, 1000);

    })
});







